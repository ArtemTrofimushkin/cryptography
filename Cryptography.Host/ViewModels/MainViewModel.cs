﻿using System;
using System.Collections.Generic;
using System.Windows.Input;

using Cryptography.Host.Api.Abstract;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Host.Api.VisualActions;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.ViewModels
{
	public sealed class MainViewModel : ViewModelBase
	{
		#region Properties 
		public IDocumentManager DocumentManager { get; }
		public IReadOnlyCollection<VisualActionContainer> VisualActions { get; }
		#endregion

		#region Commands
		public ICommand CreateDocumentCommand { get; private set; }
		public ICommand CloseCommand { get; set; }
		public ICommand AboutCommand { get; private set; }
		#endregion

		public MainViewModel(
			IDocumentManager manager, 
			ICommandFactory commandFactory,
			IReadOnlyCollection<VisualActionContainer> visualActions)
		{
			manager.NotNull(nameof(manager));
			commandFactory.NotNull(nameof(commandFactory));
			visualActions.NotNull(nameof(visualActions));

			DocumentManager = manager;
			VisualActions = visualActions;
			CreateDocumentCommand = commandFactory.CreateCommand(this.CreateDocumentExecute);
			AboutCommand = commandFactory.CreateCommand(this.AboutExecute);

			this.AboutExecute();
		}

		#region Helpers
		private void CreateDocumentExecute(object parameter)
		{
			parameter.NotNull(nameof(parameter));

			var type = parameter as Type;
			if (type != null)
			{
				DocumentManager.CreateDocument(type, activate: true);
				return;
			}
			
			var documentKey = parameter as string;
			if (!String.IsNullOrWhiteSpace(documentKey))
			{
				DocumentManager.CreateDocument(documentKey, activate: true);
				return;
			}

			throw new InvalidOperationException(
				$"Parameter {nameof(parameter)} must be the {nameof(Type)} or {nameof(String)}");
		}
		private void AboutExecute() =>
			this.CreateDocumentExecute(typeof(AboutDocument));
		#endregion
	}
}
