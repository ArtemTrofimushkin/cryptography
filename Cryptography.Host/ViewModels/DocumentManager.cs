﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

using Autofac;

using Cryptography.Host.Api.Abstract;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.ViewModels
{
	public class DocumentManager : ViewModelBase, IDocumentManager
	{
		#region Properties
		public IDocument ActiveDocument
		{
			get { return _activeDocument; }
			set { base.UpdateProperty(value, ref _activeDocument); }
		}
		public ObservableCollection<IDocument> Documents { get; }
		public int DocumentCount
		{
			get { return _documentCount; }
			set { base.UpdateProperty(value, ref _documentCount); }
		}
		#endregion

		public DocumentManager(
			ILifetimeScope scope,
			ICommandFactory commandFactory,
			IUserInterfaceService userInterface)
		{
			scope.AssignTo(out _scope, nameof(scope));
			commandFactory.AssignTo(out _commandFactory, nameof(commandFactory));
			userInterface.AssignTo(out _userInterface, nameof(userInterface));

			Documents = new ObservableCollection<IDocument>();
			Documents.CollectionChanged += this.OnCollectionChanged;
		}

		#region Create Document
		public IDocument CreateDocument(string key, bool activate = false)
		{
			key.NotNull(nameof(key));

			return this.OnDocumentCreated(
				_scope.ResolveNamed<IDocument>(key), activate);
		}
		public IDocument CreateDocument(Type documentType, bool activate = false)
		{
			this.ValidateDocumentType(documentType);

			return this.OnDocumentCreated(
				(IDocument)_scope.Resolve(documentType), activate);
		}
		public IDocument CreateDocument(Type documentType, bool activate = false, params object[] arguments)
		{
			this.ValidateDocumentType(documentType);

			var parameters = arguments.Select(
				argument => new TypedParameter(argument.GetType(), argument));

			return this.OnDocumentCreated(
				(IDocument)_scope.Resolve(documentType, parameters), activate);
		}
		#endregion

		#region Close Document 
		public void Close(IDocument document)
		{
			document.NotNull(nameof(document));
			Documents.Remove(document);
		}
		public void Close(IEnumerable<IDocument> documents)
		{
			documents.NotNull(nameof(documents));
			Documents.RemoveRange(documents);
		}
		#endregion

		#region Helpers
		private void ValidateDocumentType(Type documentType)
		{
			documentType.NotNull(nameof(documentType));

			if (!documentType.IsAssignableTo<IDocument>())
			{
				throw new InvalidOperationException(
					$"Parameter {nameof(documentType)} must be assignable from type {typeof(IDocument)}. ");
			}
		}
		private IDocument OnDocumentCreated(IDocument document, bool activate)
		{
			document.NotNull(nameof(document));
			document.DocumentManager = this;
			document.CommandFactory = _commandFactory;
			document.UserInterface = _userInterface;
			Documents.Add(document);
			
			var configurationDocument = document as IDocumentConfiguration;
			if (configurationDocument != null)
			{
				configurationDocument.Configure();
			}
			if (activate)
			{
				ActiveDocument = document;
			}

			return document;
		}
		private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
		{
			if (args.OldItems != null && args.OldItems.Count > 0)
			{
				var documents = args.OldItems.Cast<IDocument>();
				if (documents.Contains(ActiveDocument))
				{
					ActiveDocument = Documents.FirstOrDefault();
				}
			}

			DocumentCount = Documents.Count;
		}
		#endregion

		private int _documentCount;
		private IDocument _activeDocument;
		private readonly ILifetimeScope _scope;
		private readonly ICommandFactory _commandFactory;
		private readonly IUserInterfaceService _userInterface;
	}
}
