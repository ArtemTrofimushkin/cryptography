﻿using Cryptography.Host.Api.ViewModels;

namespace Cryptography.Host.ViewModels
{
	public sealed class AboutDocument : DocumentBase
	{
		public AboutDocument()
		{
			Title = "О программе";
		}
	}
}
