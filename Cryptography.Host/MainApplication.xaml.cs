﻿using System.Windows;

using Autofac;

using Cryptography.Host.Api.Abstract;
using Cryptography.Host.ViewModels;
using Cryptography.Host.Views;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Host
{
	public sealed partial class MainApplication : Application
	{
		public MainApplication(ILifetimeScope scope)
		{
			scope.AssignTo(out _scope, nameof(scope));
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			var view = new MainWindow();
			var factory = _scope.Resolve<ICommandFactory>();
			var viewModel = _scope.Resolve<MainViewModel>();

			viewModel.CloseCommand = factory.CreateCommand(() => view.Close());
			view.DataContext = viewModel;
			view.Show();
		}

		private readonly ILifetimeScope _scope;
	}
}
