﻿using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Interactivity;
using Cryptography.Host.Properties;

namespace Cryptography.Host.Behaviors
{
	public class RichTextBoxBehavior : Behavior<RichTextBox>
	{
		protected override void OnAttached()
		{
			base.OnAttached();
			this.LoadText();
		}
		protected void LoadText()
		{
			var source = Resources.ReadMe;

			var document = new FlowDocument
			{
				TextAlignment = TextAlignment.Center
			};

			var range = new TextRange(
				document.ContentStart.DocumentStart,
				document.ContentStart.DocumentEnd);

			var memory = new MemoryStream(
				Encoding.UTF8.GetBytes(source));

			range.Load(memory, DataFormats.Text);
			AssociatedObject.Document = document;
		}
	}
}
