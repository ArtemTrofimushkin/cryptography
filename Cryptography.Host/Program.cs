﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;

using Autofac;
using Autofac.Configuration;
using Microsoft.Extensions.Configuration;

using Cryptography.Host.Api.Abstract;
using Cryptography.Host.Api.Attributes;
using Cryptography.Host.Configuration;
using Cryptography.Host.UI.Abstract;
using Cryptography.Host.ViewModels;
using Cryptography.Infrastructure.Commands;
using Cryptography.Infrastructure.Services;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Host
{
	internal sealed class Program
	{
		[STAThread]
		private static void Main(string[] args)
		{
			try
			{
				RegisterAssemblies();

				var container = ConfigureDependencies();
				using(var scope = container.Resolve<ILifetimeScope>())
				{
					var application = new MainApplication(scope);
					ConfigureApplicationResources(application, scope);
					application.Run();
				}
			}
			catch (Exception exception)
			{
				// TODO: log exception
				// Show fatal message
			}
		}

		#region Assembly configuration 
		private static void RegisterAssemblies()
		{
			var config = AssemblyConfigSection.GetExeSection();
			foreach (var assemblyElement in config.AssemblyElements)
			{
				Assembly.Load(assemblyElement.GetFullAssemblyName());
			}
		}
		#endregion

		#region Dependencies configuration
		private static IContainer ConfigureDependencies()
		{
			var container = ConfigureLocalDependencies();
			ConfigureExternalDependencies(container);
			return container;
		}
		private static IContainer ConfigureLocalDependencies()
		{
			var builder = new ContainerBuilder();

			//TODO: Configure dependencies

			builder.RegisterType<MainViewModel>()
				.AsSelf()
				.SingleInstance();

			builder.RegisterType<DocumentManager>()
				.As<IDocumentManager>()
				.SingleInstance();

			builder.RegisterType<RelayCommandFactory>()
				.As<ICommandFactory>()
				.SingleInstance();

			builder.RegisterType<UserInterfaceService>()
				.As<IUserInterfaceService>()
				.SingleInstance();

			builder.RegisterType<AboutDocument>()
				.AsSelf()
				.InstancePerDependency();

			return builder.Build();
		}
		private static void ConfigureExternalDependencies(IContainer container)
		{
			container.NotNull(nameof(container));

			var configSection = ServicesConfiguration.GetExeSection();
			var configAttribute = typeof(DependencyConfgurationAttribute);
			var assemblies = AppDomain.CurrentDomain
				.GetAssemblies()
				.Where(assembly => 
					!assembly.IsDynamic && 
					assembly.FullName.StartsWith("Cryptography") && 
					assembly.IsDefined(configAttribute));

			var configurationFiles = assemblies.Select(assembly =>
				( (DependencyConfgurationAttribute)assembly.GetCustomAttribute(configAttribute) ).ConfigurationFilename).ToArray();

			try
			{
				var builder = new ContainerBuilder();
				foreach (var file in configurationFiles)
				{
					var config = new ConfigurationBuilder();
					var path = Path.Combine(configSection.ConfgurationFolder, file);

					if (File.Exists(path))
					{
						config.AddJsonFile(path);
					}

					var module = new ConfigurationModule(config.Build());
					builder.RegisterModule(module);
				}
				builder.Update(container);
			}
			catch (Exception exception)
			{
				// TODO: Log exception
				throw;
			}
		}
		#endregion

		#region Resources configuration
		private static void ConfigureApplicationResources(MainApplication application, ILifetimeScope scope)
		{
			var providers = scope.Resolve<IReadOnlyCollection<IResourceProvider>>();
			var urls = providers.SelectMany(
					provider => provider.GetResourceDictionaries());

			foreach (var url in urls)
			{
				var dictionary = new ResourceDictionary()
				{
					Source = url
				};

				application.Resources.MergedDictionaries.Add(dictionary);
			}
		}
		#endregion
	}
}
