﻿Приложение с динамической конфигурацией, каждый элемент функционала реализован в виде отдельного подключаемого блока.
На текущий момент реализованы следующие разделы:
	1. Криптография - содержит функционал по шифрованию и дешифрованию информации, сохранению и загрузке ключей
	2. Атаки на криптосистему RSA - содержит функционал для демонстрации совершения атак на криптосистему RSA (атака Винера и атака грубым перебором)
	3. Целочисленные решетки - содержит функционал для построения целочисленной решетки и совершения LLL преобразования
	4. Дискретное логарифмирование в полях и кольцах - содержит функционал для решения задачи логарифмирования в полях Галуа и кольцах