$targetPath = @(
	"Cryptography.Security",
	"Cryptography.Math",
	"Cryptography.RC4.Core"
	"Cryptography.RSA.Core", 
	"Cryptography.RSA.UI", 
	"Cryptography.IntegralGrid.UI", 
	"Cryptography.IntegralGrid.Core", 
	"Cryptography.DiscreteLogarithm.Core"
);
$binDir = "bin\Debug";
$configDir = "bin\Debug\configuration";
$destinationLibDir = ".\bin\Debug\libs";
$destinationConfigDir = ".\bin\Debug\configuration";

Write-Host "Configuration started";
Write-Host "Targets: $targetPath.";
Write-Host "Binaries directory: $binDir.";
Write-Host "Configuration directory: $configDir";

for($i = 0
    $i -lt $targetPath.Length
    $i++)
{
    $binPath = ".\..\" + $targetPath[$i] + "\" + $binDir;
    $configPath = ".\..\" + $targetPath[$i] + "\" + $configDir;

    Write-Host "Searching libs in: $binPath";
    $binFilter = $targetPath[$i] + ".dll";
    $pdbFilter = $targetPath[$i] + ".pdb";

    $files = Get-ChildItem $binPath -File |? {$_.Name -like $binFilter -or $_.Name -like $pdbFilter};
    
    for($j = 0
        $j -lt $files.Length
        $j++)
    {
        Write-Host $files[$j].FullName;
        Copy-Item $files[$j].FullName $destinationLibDir;
    }

    Write-Host "Searching configurations in: $configPath";

    $file = Get-ChildItem $configPath -File

    if ($file -ne $null)
    {
        Write-Host "Confguration found: $file";
        Copy-Item $file.FullName $destinationConfigDir;
    }
    else 
    {
        Write-Host "Confoguration not found in $configPath";
    }
}

Write-Host "Configuration finished";