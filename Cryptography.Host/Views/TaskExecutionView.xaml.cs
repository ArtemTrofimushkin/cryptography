﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using Cryptography.Host.Api.ViewModels;

namespace Cryptography.Host.Views
{
	public partial class TaskExecutionView : UserControl
	{
		public TaskExecutionView()
		{
			InitializeComponent();
			DataContextChanged += OnDataContextChanged;
		}

		private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs args)
		{
			var execution = args.NewValue as TaskExecutionViewModel;
			if (execution == null)
			{
				return;
			}
			
			if (args.OldValue != null)
			{
				( args.OldValue as TaskExecutionViewModel ).PropertyChanged -= this.OnExecutionPropertyChanged;
			}

			execution.PropertyChanged += this.OnExecutionPropertyChanged;
		}
		private void OnExecutionPropertyChanged(object sender, PropertyChangedEventArgs arguments)
		{
			var execution = sender as TaskExecutionViewModel;
			if (execution == null)
			{
				return;
			}

			if (!String.Equals(arguments.PropertyName, nameof(TaskExecutionViewModel.IsComplited), StringComparison.Ordinal))
			{
				return;
			}

			ExecutionViewDock.Background = new SolidColorBrush(
				this.GetColorIndicator(execution));

			if (execution.IsComplited && !execution.IsSuccessed)
			{
				Tag = "Show";
			} 
			else
			{
				Tag = "Hide";
			}
		}
		private void OnAnimationComplited(object sender, EventArgs args)
		{
			Tag = "Hide";
		}
		private Color GetColorIndicator(TaskExecutionViewModel execution)
		{
			return execution.IsCancelled 
				? Colors.LightBlue 
				: execution.IsTimeout 
					? Colors.GreenYellow 
					: execution.IsFaulted 
						? Colors.Red 
						: Colors.Transparent;
		}
	}
}
