﻿using System.Configuration;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.Configuration
{
	[ConfigurationCollection(typeof(AssemblyConfigurationElement), AddItemName = "Assembly")]
	internal sealed class AssemblyConfigurationCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement() =>
			new AssemblyConfigurationElement();

		protected override object GetElementKey(ConfigurationElement element)
		{
			var libElement = element as AssemblyConfigurationElement;
			libElement.NotNull(nameof(element));

			return libElement.Identity;
		}
	}
}
