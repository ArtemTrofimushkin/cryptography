﻿using System;
using System.Configuration;
using System.Reflection;

namespace Cryptography.Host.Configuration
{
	internal sealed class AssemblyConfigurationElement : ConfigurationElement
	{
		public Guid Identity { get; } = Guid.NewGuid();

		public bool IsFullQualifiedName =>
			String.IsNullOrWhiteSpace(AssemblyFileName);

		public string GetFullAssemblyName()
		{
			if (IsFullQualifiedName)
			{
				if (String.IsNullOrWhiteSpace(AssemblyName))
				{
					throw new InvalidOperationException(
						"Assembly name is null.");
				}
				if (String.IsNullOrWhiteSpace(AssemblyVersion))
				{
					throw new InvalidOperationException(
						"Assembly version is null.");
				}

				return String.Format(
					"{0}, Version={1}, {2}{3}", AssemblyName, AssemblyVersion,
					String.IsNullOrWhiteSpace(AssemblyCulture) ? "Culture=neutral" : $"Culture={AssemblyCulture}",
					String.IsNullOrWhiteSpace(AssemblyPublicKeyToken) ? "PublicKeyToken=null" : $", PublicKeyToken={AssemblyPublicKeyToken}");
			}

			return AssemblyFileName;
		}

		public AssemblyName GetQualifiedAssemblyName()
		{
			if (!IsFullQualifiedName)
			{
				throw new InvalidOperationException(
					"Assembly name must be full qualified.");
			}

			return new AssemblyName(
				this.GetFullAssemblyName());
		}

		[ConfigurationProperty(nameof(AssemblyFileName), DefaultValue = "", IsRequired = false)]
		public string AssemblyFileName
		{
			get { return (string)base[nameof(AssemblyFileName)]; }
			set { base[nameof(AssemblyFileName)] = value; }
		}

		[ConfigurationProperty(nameof(AssemblyName), DefaultValue = "", IsRequired = false)]
		public string AssemblyName
		{
			get { return (string)base[nameof(AssemblyName)]; }
			set { base[nameof(AssemblyName)] = value; }
		}

		[ConfigurationProperty(nameof(AssemblyVersion), DefaultValue = "", IsRequired = false)]
		public string AssemblyVersion
		{
			get { return (string)base[nameof(AssemblyVersion)]; }
			set { base[nameof(AssemblyVersion)] = value; }
		}

		[ConfigurationProperty(nameof(AssemblyCulture), DefaultValue = "", IsRequired = false)]
		public string AssemblyCulture
		{
			get { return (string)base[nameof(AssemblyCulture)]; }
			set { base[nameof(AssemblyCulture)] = value; }
		}

		[ConfigurationProperty(nameof(AssemblyPublicKeyToken), DefaultValue = "", IsRequired = false)]
		public string AssemblyPublicKeyToken
		{
			get { return (string)base[nameof(AssemblyPublicKeyToken)]; }
			set { base[nameof(AssemblyPublicKeyToken)] = value; }
		}
	}
}
