﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.Configuration
{
	internal sealed class AssemblyConfigSection : ConfigurationSection
	{
		[ConfigurationProperty(nameof(Assemblies))]
		public AssemblyConfigurationCollection Assemblies
		{
			get { return (AssemblyConfigurationCollection)base[nameof(Assemblies)]; }
			set { base[nameof(AssemblyConfigurationCollection)] = value; }
		}

		public IEnumerable<AssemblyConfigurationElement> AssemblyElements =>
			Assemblies.Cast<AssemblyConfigurationElement>();

		public static AssemblyConfigSection GetSection() =>
			ConfigurationExtenssions.GetSection<AssemblyConfigSection>(nameof(AssemblyConfigSection));

		public static AssemblyConfigSection GetExeSection() =>
			ConfigurationExtenssions.GetExeSection<AssemblyConfigSection>(nameof(AssemblyConfigSection));
	}
}
