﻿using System.Configuration;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.Configuration
{
	internal sealed class ServicesConfiguration : ConfigurationSection
	{
		[ConfigurationProperty(nameof(ConfgurationFolder), IsRequired = true)]
		public string ConfgurationFolder
		{
			get { return (string)base[nameof(ConfgurationFolder)]; }
			set { base[nameof(ConfgurationFolder)] = value; }
		}

		public static ServicesConfiguration GetSection() =>
			ConfigurationExtenssions.GetSection<ServicesConfiguration>(nameof(ServicesConfiguration));

		public static ServicesConfiguration GetExeSection() =>
			ConfigurationExtenssions.GetExeSection<ServicesConfiguration>(nameof(ServicesConfiguration));
	}
}
