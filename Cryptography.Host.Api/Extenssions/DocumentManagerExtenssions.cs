﻿using Cryptography.Host.Api.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.Api.Extenssions
{
	public static class DocumentManagerExtenssions
	{
		public static TDocument CreateDocument<TDocument>(this IDocumentManager manager, bool activate = false)
			where TDocument : IDocument
		{
			manager.NotNull(nameof(manager));
			return (TDocument)manager.CreateDocument(typeof(TDocument), activate);
		}

		public static TDocument CreateDocument<TDocument>(this IDocumentManager manager, bool activate = false, params object[] arguments)
			where TDocument : IDocument
		{
			manager.NotNull(nameof(manager));
			return (TDocument)manager.CreateDocument(typeof(TDocument), activate, arguments);
		}
	}
}
