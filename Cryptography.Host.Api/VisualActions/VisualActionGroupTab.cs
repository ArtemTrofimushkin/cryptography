﻿using System.Collections.Generic;

namespace Cryptography.Host.Api.VisualActions
{
	public class VisualActionGroupTab
	{
		public bool IsContextual { get; set; }
		public string Header { get; set; }
		public string ContextualGroupName { get; set; }
		public List<VisualActionGroup> Groups { get; set; }

		public VisualActionGroupTab()
		{
			Groups = new List<VisualActionGroup>();
		}
	}
}
