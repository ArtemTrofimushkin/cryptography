﻿using System.Collections.Generic;

namespace Cryptography.Host.Api.VisualActions
{
	public class VisualActionGroup
	{
		public string Title { get; set; }
		public List<VisualAction> Actions { get; set; }

		public VisualActionGroup()
		{
			Actions = new List<VisualAction>();
		}
	}
}
