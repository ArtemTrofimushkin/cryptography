﻿namespace Cryptography.Host.Api.VisualActions
{
	public class VisualAction
	{
		#region Presentation
		public string Title { get; set; }
		public string Icon { get; set; }
		public VisalActionIconSize IconSize { get; set; }
		#endregion

		#region Interaction 
		public bool IsRootCommand { get; set; }
		public string Command { get; set; }
		public object CommandParameter { get; set; }
		#endregion

		public VisualAction()
		{
		}
	}
}
