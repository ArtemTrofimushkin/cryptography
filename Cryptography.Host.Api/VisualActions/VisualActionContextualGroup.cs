﻿using System;

namespace Cryptography.Host.Api.VisualActions
{
	public class VisualActionContextualGroup
	{
		public string Title { get; set; }
		public string Name { get; set; }
		public Type ContextualObjectType { get; set; }
		public string BackgroundAndBorder { get; set; }
	}
}
