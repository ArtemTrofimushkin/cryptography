﻿using System.Collections.Generic;

namespace Cryptography.Host.Api.VisualActions
{
	public class VisualActionContainer
	{
		public List<VisualActionContextualGroup> ContextualGroups { get; set; }
		public List<VisualActionGroupTab> GroupTabs { get; set; }

		public VisualActionContainer()
		{
			GroupTabs = new List<VisualActionGroupTab>();
			ContextualGroups = new List<VisualActionContextualGroup>();
		}
	}
}
