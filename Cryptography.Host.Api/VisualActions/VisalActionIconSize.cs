﻿namespace Cryptography.Host.Api.VisualActions
{
	public enum VisalActionIconSize
	{
		Large,
		Small
	}
}
