﻿using System.ComponentModel;

namespace Cryptography.Host.Api
{
	public enum DocumentStatus
	{
		[Description("Подготовка к работе..")]
		Initialization,

		[Description("Выполнение операции..")]
		Waiting,

		[Description("Готов")]
		Ready
	}
}
