﻿using System;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.Api.Attributes
{
	[AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
	public sealed class ComputedAttribute : Attribute
	{
		public string AffectedProperty { get; }

		public ComputedAttribute(string affectedProperty)
		{
			affectedProperty.NotNull(nameof(affectedProperty));
			AffectedProperty = affectedProperty;
		}
	}
}
