﻿using System;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.Api.Attributes
{
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = false)]
	public sealed class DependencyConfgurationAttribute : Attribute
	{
		public string ConfigurationFilename { get; set; }

		public DependencyConfgurationAttribute(string configurationFilename)
		{
			configurationFilename.NotNull(nameof(ConfigurationFilename));
			ConfigurationFilename = configurationFilename;
		}
	}
}
