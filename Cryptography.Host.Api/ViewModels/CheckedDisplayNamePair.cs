﻿namespace Cryptography.Host.Api.ViewModels
{
	public class CheckedDisplayNamePair : ViewModelBase
	{
		public string DisplayName { get; }
		public bool IsChecked
		{
			get { return _checked; }
			set { this.UpdateProperty(value, ref _checked); }
		}

		public CheckedDisplayNamePair(string displayName)
		{
			DisplayName = displayName;
		}

		private bool _checked;
	}
}
