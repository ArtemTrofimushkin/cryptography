﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

using Cryptography.Host.Api.Attributes;
using Cryptography.Utills.Extenssions;
using Cryptography.Utills.Helpers;

namespace Cryptography.Host.Api.ViewModels
{
	public abstract class ViewModelBase :
		IDataErrorInfo,
		INotifyPropertyChanged,
		INotifyPropertyChanging
	{
		#region Events
		public event PropertyChangedEventHandler PropertyChanged;
		public event PropertyChangingEventHandler PropertyChanging;
		#endregion

		#region Properties
		public bool IsValid =>
			String.IsNullOrWhiteSpace(Error);
		public virtual string Error
		{
			get
			{
				return String.Join(Environment.NewLine,
					ValidationErrors.Values);
			}
		}
		public string this[string propertyName]
		{
			get
			{
				this.EnsurePropertyExists(propertyName);
				return ValidationErrors.GetValueOrDefault(propertyName);
			}
		}
		protected IDictionary<string, string> ValidationErrors
		{
			get
			{
				return _validationErrors ??
					( _validationErrors = this.GetValidationErrors() );
			}
		}
		#endregion

		protected ViewModelBase()
		{
			_properties = this.GetPropertyNames();
			_computedProperties = this.GetComputedProperties().AsReadOnly();
		}

		#region INotifyPropertyChanged
		protected virtual void OnPropertyChanged(string propertyName)
		{
			this.EnsurePropertyExists(propertyName);

			var error = this.ValidateProperty(propertyName);
			if (!String.IsNullOrWhiteSpace(error))
			{
				ValidationErrors[propertyName] = error;
				this.RaisePropertyChanged(nameof(Error));
			}
			else if (ValidationErrors.ContainsKey(propertyName))
			{
				ValidationErrors.Remove(propertyName);
				this.RaisePropertyChanged(nameof(Error));
			}

			this.RaisePropertyChanged(propertyName);

			_computedProperties.GetValueOrDefault(propertyName)
				?.ForEach(property => this.OnPropertyChanged(property));
		}

		protected virtual void RaisePropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this,
				new PropertyChangedEventArgs(propertyName));
		}
		#endregion

		#region INotifyPropertyChanging
		protected virtual void OnPropertyChanging(string propertyName)
		{
			this.EnsurePropertyExists(propertyName);
			this.RaisePropertyChanging(propertyName);
		}

		protected virtual void RaisePropertyChanging(string propertyName)
		{
			PropertyChanging?.Invoke(this,
				new PropertyChangingEventArgs(propertyName));
		}
		#endregion

		#region PropertyHelpers
		protected virtual IReadOnlyCollection<string> GetPropertyNames()
		{
			return this.GetInstanceProperties()
				.Select(property => property.Name).ToArray();
		}

		protected virtual IDictionary<string, IReadOnlyCollection<string>> GetComputedProperties()
		{
			var computedAttribute = typeof(ComputedAttribute);

			return this.GetInstanceProperties()
				.Where(property => Attribute.IsDefined(property, computedAttribute))
				.ToDictionary(
					property => property.Name,
					property => property.GetCustomAttributes<ComputedAttribute>(true)
						.Select(attribute => attribute.AffectedProperty).ToArray() as IReadOnlyCollection<string>);
		}

		protected virtual IDictionary<string, string> GetValidationErrors()
		{
			return _properties.Select(
					propertyName => new
					{
						Name = propertyName,
						Error = this.ValidateProperty(propertyName)
					})
				.Where(x => !String.IsNullOrWhiteSpace(x.Error))
				.ToDictionary(x => x.Name, x => x.Error);
		}

		private void EnsurePropertyExists(string propertyName)
		{
#if DEBUG
			if (!_properties.Contains(propertyName))
			{
				throw new InvalidOperationException(
					$"Property with name {propertyName} does not exists.");
			}
#endif
		}

		private IReadOnlyCollection<PropertyInfo> GetInstanceProperties()
		{
			return PropertyHelper.GetProperties(
				this.GetType(), BindingFlags.Instance | BindingFlags.Public);
		}
		#endregion

		protected virtual string ValidateProperty(string propertyName)
		{
			return String.Empty;
		}
		protected virtual void UpdateProperty<T>(T value, ref T storage, [CallerMemberName]string propertyName = null)
		{
			if (!EqualityComparer<T>.Default.Equals(value, storage))
			{
				this.OnPropertyChanging(propertyName);
				storage = value;
				this.OnPropertyChanged(propertyName);
			}
		}

		protected readonly IReadOnlyCollection<string> _properties;
		protected readonly IReadOnlyDictionary<string, IReadOnlyCollection<string>> _computedProperties;
		protected IDictionary<string, string> _validationErrors;
	}
}
