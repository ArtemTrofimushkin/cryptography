﻿using System;
using System.Collections.Generic;
using System.Linq;

using Cryptography.Utills.Extenssions;
using Cryptography.Utills.Helpers;

namespace Cryptography.Host.Api.ViewModels
{
	public class EnumDescriptionViewModel : ViewModelBase, IEquatable<EnumDescriptionViewModel>
	{
		public Enum Value { get; }
		public string Description { get; }

		private EnumDescriptionViewModel(Enum value, string description)
		{
			value.NotNull(nameof(value));
			description.NotNull(nameof(description));

			Value = value;
			Description = description;
		}

		public override bool Equals(object obj)
		{
			var other = obj as EnumDescriptionViewModel;
			if (other == null)
			{
				return false;
			}

			return this.Equals(other);
		}
		public override int GetHashCode() => Value.GetHashCode();
		public bool Equals(EnumDescriptionViewModel other)
		{
			other.NotNull(nameof(other));
			return Value.Equals(other.Value);
		}

		public static EnumDescriptionViewModel Create<TEnum>(TEnum value) where TEnum: struct, IConvertible =>
			new EnumDescriptionViewModel(value as Enum, EnumHelper.GetDescription(value));
		public static IReadOnlyList<EnumDescriptionViewModel> Create<TEnum>() where TEnum : struct, IConvertible
		{
			return Enum.GetValues(typeof(TEnum))
				.Cast<TEnum>()
				.Select(Create).ToArray();
		}
	}
}
