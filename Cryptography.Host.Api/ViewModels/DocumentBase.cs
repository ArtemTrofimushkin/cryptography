﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

using Cryptography.Host.Api.Abstract;
using Cryptography.Utills.Extenssions;
using Cryptography.Utills.Helpers;

namespace Cryptography.Host.Api.ViewModels
{
	public abstract class DocumentBase : ViewModelBase, 
		IDocument, IDocumentConfiguration
	{
		#region Properties 
		public ICommandFactory CommandFactory { get; set; }
		public IDocumentManager DocumentManager { get; set; }
		public IUserInterfaceService UserInterface { get; set; }
		public TaskExecutionViewModel TaskExecution
		{
			get { return _execution; }
			set { this.UpdateProperty(value, ref _execution); }
		}
		public string Title
		{
			get { return _title; }
			set { base.UpdateProperty(value, ref _title); }
		}
		public string Status
		{
			get { return _status; }
			private set { base.UpdateProperty(value, ref _status); }
		}
		public bool IsBusy
		{
			get { return _busy; }
			private set { base.UpdateProperty(value, ref _busy); }
		}
		protected DocumentStatus DocumentStatus
		{
			get { return _documentStatus; }
			set
			{
				_documentStatus = value;
				if (_documentStatus == DocumentStatus.Waiting)
				{
					IsBusy = true;
				}
				else
				{
					IsBusy = false;
				}
				Status = _documentStatus.GetDescription();
			}
		}
		#endregion

		#region Commands
		public ICommand CloseCommand { get; private set; }
		public IAsyncCancelCommand CancelCommand { get; private set; }
		#endregion

		protected DocumentBase()
		{
			DocumentStatus = DocumentStatus.Initialization;
		}

		#region IDisposable
		void IDisposable.Dispose() =>
			this.Dispose();

		protected virtual void Dispose()
		{
		}
		#endregion

		#region IConfiguration
		void IDocumentConfiguration.Configure() =>
			this.Configure();

		protected virtual void Configure()
		{
			this.CreateCommands();
			this.OnInitialized();
		}
		protected virtual void CreateCommands()
		{
			CancelCommand = CommandFactory.CreateCancelCommand(this.CanCancelExecute);
			CloseCommand = CommandFactory.CreateCommand(
				this.CloseCommandExecute, this.CloseCommandCanExecute);
		}
		protected virtual void OnInitialized()
		{
			DocumentStatus = DocumentStatus.Ready;
		}
		#endregion

		#region Command methods
		protected void CloseCommandExecute()
		{
			this.OnClosing();
			DocumentManager.Close(this);
		}
		protected virtual void OnClosing()
		{
		}
		protected virtual bool CloseCommandCanExecute() => true;
		#endregion

		#region ExecuteAsyncOperation
		protected Task ExecuteOperationAsync(Func<Task> operation)
		{
			operation.NotNull(nameof(operation));
			return this.ExecuteOperationAsync(t => operation(), TaskHelper.EmptyResult);
		}
		protected Task ExecuteOperationAsync(Func<CancellationToken, Task> operation)
		{
			operation.NotNull(nameof(operation));
			return this.ExecuteOperationAsync((argument, token) => operation(token), TaskHelper.EmptyResult);
		}
		protected Task ExecuteOperationAsync<T>(Func<T, Task> operation, T argument)
		{
			operation.NotNull(nameof(operation));
			return this.ExecuteOperationAsync((arg, token) => operation(arg), argument);
		}
		protected async Task ExecuteOperationAsync<T>(Func<T, CancellationToken, Task> operation, T argument)
		{
			operation.NotNull(nameof(operation));

			CancelCommand.Accuire();

			var token = CancelCommand.Token;
			var task = this.CreateTask(operation, argument, token);

			TaskExecution = new TaskExecutionViewModel(task);
			if (TaskExecution.IsNotComplited)
			{
				DocumentStatus = DocumentStatus.Waiting;
			}

			if (TaskExecution.ComplitedTask != null)
			{
				await TaskExecution.ComplitedTask;
			}

			CancelCommand.Release();
			DocumentStatus = DocumentStatus.Ready;
		}
		protected virtual bool CanCancelExecute(object parameter) => true;
		private Task CreateTask<T>(Func<T, CancellationToken, Task> operation, 
			T argument, CancellationToken token)
		{
			try
			{
				return operation(argument, token);
			}
			catch(Exception exception)
			{
				return exception.FromException();
			}
		}
		#endregion

		private bool _busy;
		protected string _title;
		protected string _status;
		private DocumentStatus _documentStatus;
		private TaskExecutionViewModel _execution;
	}
}
