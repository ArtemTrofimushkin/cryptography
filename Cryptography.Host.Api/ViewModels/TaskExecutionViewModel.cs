﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.Api.ViewModels
{
	public class TaskExecutionViewModel :
		INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		#region Task properties
		public Task Task { get; private set; }
		public Task ComplitedTask { get; private set; }
		public TaskStatus TaskStatus => Task.Status;
		public bool IsComplited => Task.IsCompleted;
		public bool IsNotComplited => !Task.IsCompleted;
		public bool IsFaulted => Task.IsFaulted;
		public bool IsCancelled => Task.IsCanceled;
		public bool IsSuccessed =>
			Task.Status == TaskStatus.RanToCompletion;
		public bool IsTimeout { get; private set; }
		public string ErrorMessage { get; private set; }
		#endregion
		
		public TaskExecutionViewModel(Task task)
		{
			task.NotNull(nameof(task));

			Task = task;

			if (!task.IsCompleted)
			{
				ComplitedTask = this.ExecuteTaskAsync(task);
			}
		}

		#region Task execution handlers
		protected async Task ExecuteTaskAsync(Task task)
		{
			Exception exception = null;
			try
			{
				await task;
			}
			catch (OperationCanceledException)
			{
			}
			catch (Exception ex)
			{
				exception = ex;
			}
			finally
			{
				this.OnTaskExecuted(task, exception);
			}
		}
		protected void OnTaskExecuted(Task task, Exception exception)
		{
			var handler = PropertyChanged;
			if (handler == null)
			{
				return;
			}

			if (task.IsCanceled)
			{
				this.OnTaskCanceled(handler);
			}
			if (exception is TimeoutException)
			{
				this.OnTaskTimeout(handler);
			}
			else if (task.IsFaulted)
			{
				this.OnTaskFaulted(handler);	
			}
			if (task.IsCompleted)
			{
				this.OnTaskSuccesed(handler);
			}

			handler(this, this.Arguments(nameof(IsComplited)));
			handler(this, this.Arguments(nameof(IsNotComplited)));
			handler(this, this.Arguments(nameof(TaskStatus)));
		}
		protected virtual void OnTaskCanceled(PropertyChangedEventHandler handler)
		{
			ErrorMessage = "Операция была отменена пользователем";

			handler(this, this.Arguments(nameof(IsCancelled)));
			handler(this, this.Arguments(nameof(ErrorMessage)));
		}
		protected virtual void OnTaskTimeout(PropertyChangedEventHandler handler)
		{
			IsTimeout = true;
			ErrorMessage = "Был превышен таймаут ожидания";

			handler(this, this.Arguments(nameof(IsTimeout)));
			handler(this, this.Arguments(nameof(ErrorMessage)));
		}
		protected virtual void OnTaskFaulted(PropertyChangedEventHandler handler)
		{
			ErrorMessage = Task.Exception?.GetAllErrorMessages();

			handler(this, this.Arguments(nameof(IsFaulted)));
			handler(this, this.Arguments(nameof(ErrorMessage)));
		}
		protected virtual void OnTaskSuccesed(PropertyChangedEventHandler handler)
		{
			handler(this, this.Arguments(nameof(IsSuccessed)));
		}
		#endregion

		protected PropertyChangedEventArgs Arguments(string property) =>
			new PropertyChangedEventArgs(property);
	}
}
