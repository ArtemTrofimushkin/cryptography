﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Cryptography.Host.Api.ViewModels
{
	public class ValueViewModel<TModel> : INotifyPropertyChanged
		where TModel: struct, IEquatable<TModel>
	{
		public event PropertyChangedEventHandler PropertyChanged;
		public TModel Value
		{
			get { return _value; }
			set
			{
				if (_value.Equals(value))
				{
					return;
				}
				_value = value;
				this.RaisePropertyChanged();
			}
		}

		public ValueViewModel()
		{
		}
		public ValueViewModel(TModel value)
		{
			_value = value;
		}
		protected virtual void RaisePropertyChanged(
			[CallerMemberName] string property = default(string))
		{
			PropertyChanged?.Invoke(this,
				new PropertyChangedEventArgs(property));
		}

		private TModel _value;
	}
}
