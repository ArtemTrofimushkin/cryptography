﻿namespace Cryptography.Host.Api.Abstract
{
	public interface IUserInterfaceService
	{
		bool DisplayQuestion(string caption, string text);
		void DisplayErrorMessage(string caption, string text);
		void DisplayWarningMessage(string caption, string text);
		void DisplayInfoMessage(string caption, string text);
		string OpenFileDialog(string mask = null);
		string SaveFileDialog(string mask = null);
		string OpenFolderDialog();
	}
}
