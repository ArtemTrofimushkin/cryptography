﻿using System;
using System.Windows.Input;

namespace Cryptography.Host.Api.Abstract
{
	public interface IDocument : IDisposable
	{
		string Title { get; set; }
		string Status { get; }

		IAsyncCancelCommand CancelCommand { get; }
		IDocumentManager DocumentManager { get; set; }
		ICommandFactory CommandFactory { get; set; }
		IUserInterfaceService UserInterface { get; set; }
	}
}
