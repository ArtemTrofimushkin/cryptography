﻿using System.Threading;
using System.Windows.Input;

namespace Cryptography.Host.Api.Abstract
{
	public interface IAsyncCancelCommand : ICommand
	{
		CancellationToken Token { get; }
		void Accuire();
		void Release();
	}
}
