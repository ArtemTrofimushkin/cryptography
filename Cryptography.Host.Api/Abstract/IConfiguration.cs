﻿namespace Cryptography.Host.Api.Abstract
{
	public interface IDocumentConfiguration
	{
		void Configure();
	}
}
