﻿using System;
using System.Windows.Input;

namespace Cryptography.Host.Api.Abstract
{
	public interface ICommandFactory
	{
		ICommand CreateCommand(Action action);
		ICommand CreateCommand(Action action, Func<bool> validate);

		ICommand CreateCommand<T>(Action<T> action);
		ICommand CreateCommand<T>(Action<T> action, Func<T, bool> validate);

		ICommand CreateCommand(Action<object> action);
		ICommand CreateCommand(Action<object> action, Func<object, bool> validate);

		IAsyncCancelCommand CreateCancelCommand();
		IAsyncCancelCommand CreateCancelCommand(Func<object, bool> validate);
	}
}
