﻿using System;
using System.Collections.Generic;

namespace Cryptography.Host.Api.Abstract
{
	public interface IDocumentManager
	{
		IDocument CreateDocument(string key, bool activate = false);
		IDocument CreateDocument(Type documentType, bool activate = false);
		IDocument CreateDocument(Type documentType, bool activate = false, params object[] arguments);

		void Close(IDocument document);
		void Close(IEnumerable<IDocument> documents);
	}
}
