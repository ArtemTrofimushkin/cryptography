﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cryptography.Math.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Math.Services
{
	public class TransposDecompPermut : ITransposDecompPermut
	{
		public Transposition[] GetTranspositionsMultiplication(uint[] prot, uint[] image)
		{
			this.Validate(prot, image);

			var transposList = new List<Transposition>();
			var usedNumbList = new List<uint>();
			for (int i = 0; i < prot.Length; ++i)
			{
				if (usedNumbList.Contains(prot[i]))
				{
					continue;
				}

				uint tmp = 0;
				int k = i;
				while (tmp != prot[i])
				{
					if (!usedNumbList.Contains(prot[k]))
					{
						usedNumbList.Add(prot[k]);
					}
					if (!usedNumbList.Contains(image[k]))
					{
						transposList.Add(new Transposition(prot[i], image[k]));
						usedNumbList.Add(image[k]);
					}
					k = Array.IndexOf(prot, image[k]);
					tmp = image[k];
				}
			}
			return transposList.ToArray();
		}

		private void Validate(uint[] prot, uint[] image)
		{
			prot.NotNull(nameof(prot));
			image.NotNull(nameof(image));

			if (prot.Contains(0u) || image.Contains(0u))
			{
				throw new InvalidOperationException(
					$"Zero is not a natural number");
			}

			if (prot.Length != image.Length)
			{
				throw new InvalidOperationException(
					$"Collection must be equal size, specified: {prot.Length} and {image.Length}");
			}

			var protDistinct = prot.Distinct().ToArray();
			var imageDistinct = image.Distinct().ToArray();

			if (protDistinct.Length != prot.Length ||
				imageDistinct.Length != image.Length)
			{
				throw new InvalidOperationException(
					$"Collections must contain unique elemets");
			}

			if (!prot.All(x => imageDistinct.Contains(x)))
			{
				throw new InvalidOperationException(
					"Collection must contain same elemts");
			}
		}
	}
}
