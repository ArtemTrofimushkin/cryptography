﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Cryptography.Math.Abstract;
using Cryptography.Math.Helpers;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Math.Services
{
	public class FactorizeService : IFactorizeService
	{
		public IDictionary<ulong, int> Factorize(ulong product, CancellationToken token)
		{
			var results = new Dictionary<ulong, int>();
			this.FactorizeInternal(product, results, token);

			return results;
		}

		private void FactorizeInternal(ulong product, IDictionary<ulong, int> results, CancellationToken token)
		{
			ulong divider = 0;
			int count = 0;

			if (product == 1UL)
			{
				return;
			}

			if (IsPrime(product))
			{
				count = results.GetValueOrDefault(product);
				results[product] = ++count;
			} else if (product < 997 * 997)
			{
				divider = this.GetPrimeTrivialDivisor(product);
				count = results.GetValueOrDefault(divider);

				results[divider] = ++count;

				this.FactorizeInternal(product / divider, results, token);
			} else
			{
				divider = this.FactorizePollardRho(product);

				if (divider == 1)
				{
					divider = this.FactorizePollardBent(product);
				}
				if (divider == 1)
				{
					divider = this.FactorizeFerma(product, token);
				}

				this.FactorizeInternal(divider, results, token);
				this.FactorizeInternal(product / divider, results, token);
			}
		}

		private ulong FactorizeFerma(ulong product, CancellationToken token)
		{
			int iteration = 0;
			ulong x = AlgebraicHelper.Sqrt(product),
				y = 0,
				r = x * x - y * y - product;

			while (true)
			{
				if (++iteration % CheckIteration == 0)
				{
					token.ThrowIfCancellationRequested();
				}

				if (r == 0)
				{
					return x != y ?
						x - y : x + y;
				} else if (r > 0)
				{
					r -= y + y + 1;
					++y;
				} else
				{
					r += x + x + 1;
					++x;
				}
			}
		}
		private ulong FactorizePollardBent(ulong product)
		{
			ulong first = (ulong)_random.Next() % product,
				second = first,
				a = 0,
				g = 0;

			second = AlgebraicHelper.ModuleMultiply(second, first + 2L, product);
			a = second;

			for (int i = 0, seriesLength = 1; i < ProbabilityTestTriesCount; i++, seriesLength *= 2)
			{
				g = AlgebraicHelper.EuclideAlgorithm(second - first, product);

				for (int len = 0; len < seriesLength && ( g == 1 && g == product ); len++)
				{
					second = AlgebraicHelper.ModuleMultiply(second, first + 2, product);

					var difference = ( (long)second - (long)first ).Abs();
					g = AlgebraicHelper.EuclideAlgorithm(difference, product);
				}
				first = a;
				a = second;
				if (g != 1 && g != product)
				{
					return g;
				}
			}
			return 1;
		}
		private ulong FactorizePollardRho(ulong product)
		{
			ulong first = (ulong)_random.Next() % product,
				second = first,
				g = 0;

			second = AlgebraicHelper.ModuleMultiply(second, second, product);

			if (++second == product)
			{
				second = 0;
			}
			g = AlgebraicHelper.EuclideAlgorithm(( (long)second - (long)first ).Abs(), product);

			for (int i = 0; i < ProbabilityTestTriesCount && ( g == 1 || g == product ); i++)
			{
				first = AlgebraicHelper.ModuleMultiply(first, first, product);
				if (++first == product)
				{
					first = 0;
				}
				second = AlgebraicHelper.ModuleMultiply(second, second, product);
				++second;

				second = AlgebraicHelper.ModuleMultiply(second, second, product);
				if (++second == product)
				{
					second = 0;
				}

				g = AlgebraicHelper.EuclideAlgorithm(( (long)second - (long)first ).Abs(), product);
			}

			return g;
		}
		private ulong GetPrimeTrivialDivisor(ulong product)
		{
			if (product == 2 || product == 3)
			{
				return 1;
			}
			if (product < 2)
			{
				return 0UL;
			}
			if (AlgebraicHelper.IsEven(product))
			{
				return 2UL;
			}

			ulong divider = 0UL;
			this.EnsurePrimeNumbersEnough(MaxTrivialDivisor);

			for (int i = 0; i != _primes.Count; ++i)
			{
				divider = _primes[i];
				if (divider * divider > product)
				{
					break;
				} else if (product % divider == 0)
				{
					return divider;
				}
			}

			return product < MaxTrivialDivisor * MaxTrivialDivisor ? 1UL : 0UL;
		}
		private void EnsurePrimeNumbersEnough(ulong number)
		{
			if (_primes == null)
			{
				_primes = new List<ulong>() { 2UL };
			}

			ulong start = _primes.Count == 1 ?
				3UL : _primes.Last();

			for (ulong current = start; current <= number; current += 2)
			{
				bool prime = true;

				for (int i = 0; i != _primes.Count; ++i)
				{
					var divisor = _primes[i];
					if (divisor * divisor > current)
					{
						break;
					}
					if (current % divisor == 0)
					{
						prime = false;
						break;
					}
				}
				if (prime)
				{
					_primes.Add(current);
				}
			}
		}
		private bool MuillerRabinCheck(ulong number, ulong basis)
		{
			for (ulong g = 0; ( g = AlgebraicHelper.EuclideAlgorithm(number, basis) ) != 1; ++basis)
			{
				if (number > g)
				{
					return false;
				}
			}

			ulong unity = number - 1,
				p = 0,
				q = 0;

			AlgebraicHelper.TransformToExponential(number, ref p, ref q);

			ulong reminder = AlgebraicHelper.ModulePower(basis, q, number);
			if (reminder == 1 || reminder == unity)
			{
				return true;
			}

			for (ulong i = 1; i < p; i++)
			{
				reminder = AlgebraicHelper.ModuleMultiply(reminder, reminder, number);
				if (reminder == unity)
				{
					return true;
				}
			}
			return false;
		}
		private bool IsPrime(ulong number)
		{
			ulong trivial = this.GetPrimeTrivialDivisor(number);

			if (trivial == 1)
			{
				return true;
			}
			if (trivial > 1)
			{
				return false;
			}

			// TODO: Исправить
			//var result = true;
			//for (int i = 0; i != MuillerRabinTriesCount; i++)
			//{
			//	result &= this.MuillerRabinCheck(number, (ulong)_random.Next());
			//}
			return false;
		}

		private List<ulong> _primes;
		private readonly Random _random = new Random(Guid.NewGuid().GetHashCode());

		#region Constants
		private const int CheckIteration = 10000;
		private const int MuillerRabinTriesCount = 100;
		private const int ProbabilityTestTriesCount = 1000;
		private const ulong MaxTrivialDivisor = 1260157;
		#endregion
	}
}
