﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cryptography.Math.Abstract;
using Cryptography.Math.Helpers;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Math.Services
{
	public class ChineseReminderService : IChineseReminderService
	{
		public ulong FindReminder(IDictionary<ulong, ulong> pairs)
		{
			pairs.NotNull(nameof(pairs));

			var sum = 0UL;
			var product = pairs.Keys.Aggregate(1UL, (x, y) => x * y);

			foreach(var pair in pairs)
			{
				var division = product / pair.Key;
				sum += pair.Value * AlgebraicHelper.MultiplicativeInverse(division, pair.Key) * division;
			}

			return sum % product;
		}
	}
}
