﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Math
{
	using Math = System.Math;

	public class SquaredMatrix :
		IEnumerable<double>,
		IEquatable<SquaredMatrix>,
		ICloneable
	{
		#region Properties
		public int Size { get; }
		public double this[int i, int j]
		{
			get { return _values[i * Size + j]; }
			set { _values[i * Size + j] = value; }
		}
		public bool HasDeterminant =>
			Math.Abs(this.Determinant()) > Epsilon;
		#endregion

		public SquaredMatrix(int size)
		{
			_values = this.AllocateValues(size);
			Size = size;
		}
		public SquaredMatrix(int size, params double[] values)
		{
			values.NotNull(nameof(values));
			_values = this.AllocateValues(size, values);
			Size = size;
		}
		public SquaredMatrix(int size, IEnumerable<double> values)
		{
			values.NotNull(nameof(values));
			_values = this.AllocateValues(size, values.ToArray());
			Size = size;
		}

		#region Operations
		public SquaredMatrix Addition(SquaredMatrix other)
		{
			this.EnsureEqualSize(other);

			for (int i = 0; i != Size * Size; ++i)
			{
				_values[i] += other._values[i];
			}

			return this;
		}
		public SquaredMatrix Substract(SquaredMatrix other)
		{
			this.EnsureEqualSize(other);

			for (int i = 0; i != Size * Size; ++i)
			{
				_values[i] -= other._values[i];
			}

			return this;
		}
		public SquaredMatrix Multiply(SquaredMatrix other)
		{
			this.EnsureEqualSize(other);

			var result = new SquaredMatrix(Size);
			for (int i = 0; i != Size; ++i)
			{
				for (int j = 0; j != Size; ++j)
				{
					for (int k = 0; k != Size; k++)
					{
						result[i, j] += this[i, k] * other[k, j];
					}
				}
			}

			return result;
		}
		public SquaredMatrix Multiply(double number)
		{
			var result = new SquaredMatrix(Size);
			for (int i = 0; i != Size; ++i)
			{
				for (int j = 0; j != Size; ++j)
				{
					result[i, j] = this[i, j] * number;
				}
			}

			return result;
		}
		public SquaredMatrix Divide(SquaredMatrix other) =>
			this.Multiply(other.ToInversed());
		public SquaredMatrix Divide(double number) =>
			this.Multiply(1.0 / number);
		public SquaredMatrix ToTransposed()
		{
			var result = new SquaredMatrix(Size);
			for (int i = 0; i != Size; ++i)
			{
				for (int j = 0; j != Size; ++j)
				{
					result[i, j] = this[j, i];
				}
			}
			return result;
		}
		public SquaredMatrix ToInversed()
		{
			SquaredMatrix result = this.Clone(),
				identity = SquaredMatrix.Identity(Size);

			for (int i = 0, leader = 0; i != Size; ++i)
			{
				leader = GetLeaderPosition(_values, i, leader, Size);

				if (Math.Abs(this[leader, i]) > Epsilon)
				{
					SwapRow(result._values, i, leader, Size);
					SwapRow(identity._values, i, leader, Size);
				}

				for (int j = 0; j < Size; j++)
				{
					if (i == j)
					{
						continue;
					}

					var multiplier = result[j, i] / result[i, i];
					for (int k = 0; k < Size; k++)
					{
						result[j, k] -= result[i , k] * multiplier;
						identity[j, k] -= identity[i, k] * multiplier;
					}
				}
			}

			for (int i = 0; i < Size; i++)
			{
				var divisor = result[i, i];
				if (Math.Abs(divisor) <= Double.Epsilon)
				{
					throw new InvalidOperationException(
						"Determinator is equal to 0");
				}
				for (int k = 0; k < Size; k++)
				{
					identity[i, k] /= divisor;
				}
			}

			return identity;
		}
		public double Determinant()
		{
			var clone = this.Clone();
			var determinant = _values.All(x => Math.Abs(x) > Epsilon)
				? 1.0
				: 0.0;

			for (int i = 0, leader = 0; i < Size; ++i)
			{
				leader = GetLeaderPosition(_values, i, leader, Size);

				if (Math.Abs(this[leader, i]) < Epsilon)
				{
					break;
				}
				SwapRow(clone._values, i, leader, Size);

				if (i != leader)
				{
					determinant *= -1.0;
				}
				determinant *= clone[i, i];

				for (int j = i + 1; j < Size; ++j)
				{
					clone[i, j] /= clone[i, i];
				}
				for (int j = 0; j < Size; ++j)
				{
					if (j != i && 
						Math.Abs(clone[j, i]) > Epsilon)
					{
						for (int k = i + 1; k < Size; ++k)
						{
							clone[j, k] -= clone[i, k] * clone[j, i];
						}
					}
				}
			}

			return determinant;
		}
		#endregion

		#region Static Operations
		public static SquaredMatrix Identity(int size)
		{
			var matrix = new SquaredMatrix(size);
			for(int i = 0; i != size; ++i)
			{
				matrix._values[i * size + i] = 1.0;
			}

			return matrix;
		}
		public static SquaredMatrix Zero(int size) =>
			new SquaredMatrix(size);
		public static SquaredMatrix Transposed(SquaredMatrix matrix)
		{
			matrix.NotNull(nameof(matrix));
			return matrix.ToTransposed();
		}
		public static SquaredMatrix Inversed(SquaredMatrix matrix)
		{
			matrix.NotNull(nameof(matrix));
			return matrix.ToInversed();
		}
		public static SquaredMatrix Addition(SquaredMatrix left, SquaredMatrix right)
		{
			left.NotNull(nameof(left));

			return left.Clone()
				.Addition(right);
		}
		public static SquaredMatrix Substract(SquaredMatrix left, SquaredMatrix right)
		{
			left.NotNull(nameof(left));

			return left.Clone()
				.Substract(right);
		}
		public static SquaredMatrix Multiply(SquaredMatrix left, SquaredMatrix right)
		{
			left.NotNull(nameof(left));
			return left.Multiply(right);
		}
		public static SquaredMatrix Divide(SquaredMatrix left, SquaredMatrix right)
		{
			left.NotNull(nameof(left));
			return left.Divide(right);
		}
		public SquaredMatrix Multiply(SquaredMatrix left, double right)
		{
			left.NotNull(nameof(left));
			return left.Multiply(right);
		}
		public SquaredMatrix Divide(SquaredMatrix left, double right)
		{
			left.NotNull(nameof(left));
			return left.Divide(right);
		}
		#endregion

		#region Operators
		public static SquaredMatrix operator +(SquaredMatrix left, SquaredMatrix right) =>
			SquaredMatrix.Addition(left, right);
		public static SquaredMatrix operator -(SquaredMatrix left, SquaredMatrix right) =>
			SquaredMatrix.Substract(left, right);
		public static SquaredMatrix operator *(SquaredMatrix left, SquaredMatrix right) =>
			SquaredMatrix.Multiply(left, right);
		public static SquaredMatrix operator /(SquaredMatrix left, SquaredMatrix right) =>
			SquaredMatrix.Divide(left, right);
		public static explicit operator double[] (SquaredMatrix matrix)
		{
			matrix.NotNull(nameof(matrix));
			return (double[])matrix._values.Clone();
		}
		#endregion

		#region Equtable
		public override int GetHashCode() => 
			_values.GetHashCode();
		public override bool Equals(object obj)
		{
			var other = obj as SquaredMatrix;
			if (other == null)
			{
				return false;
			}
			return this.Equals(other);
		}
		public bool Equals(SquaredMatrix other)
		{
			other.NotNull(nameof(other));

			if (Size != other.Size)
			{
				return false;
			}
			for (int i = 0; i != Size * Size; ++i)
			{
				if (Math.Abs(_values[i] - other._values[i]) > Epsilon)
				{
					return false;
				}
			}
			return true;
		}
		#endregion

		#region Clonable
		object ICloneable.Clone() =>
			new SquaredMatrix(Size, _values);
		public SquaredMatrix Clone()
		{
			return (SquaredMatrix)( (ICloneable)this ).Clone();
		}
		#endregion

		#region IEnumerable
		public IEnumerator<double> GetEnumerator() =>
			_values.Cast<double>().GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => 
			this.GetEnumerator();
		#endregion

		public override string ToString()
		{
			var builder = new StringBuilder();
			for(int i = 0; i != Size; ++i)
			{
				builder.Append("( ");
				for(int j = 0; j != Size; ++j)
				{
					builder.Append($"{this[i, j]} ");
				}
				builder.Append($"){Environment.NewLine}");
			}
			return builder.ToString();
		}
		
		#region Helpers
		private double[] AllocateValues(int size)
		{
			if (size < 0)
			{
				throw new ArgumentException(
					"Size must be pisitive", nameof(size));
			}

			return new double[size * size];
		}
		private double[] AllocateValues(int size, IReadOnlyList<double> values)
		{
			var array = this.AllocateValues(size);

			for(int k = 0; k != values.Count; ++k)
			{
				array[k] = values[k];
			}

			return array;
		}
		private void EnsureEqualSize(SquaredMatrix other)
		{
			other.NotNull(nameof(other));
			if (Size != other.Size)
			{
				throw new ArgumentException("Must be equal size", nameof(other));
			}
		}
		private static void SwapRow(double[] values, int i, int k, int size)
		{
			for (int j = 0; j < size; j++)
			{
				values.Swap(i * size + j, k * size + j);
			}
		}
		private static int GetLeaderPosition(double[] values, int row, int position, int size)
		{
			var leader = position;

			for (int j = row; j != size; ++j)
			{
				if (Math.Abs(values[row * size + j]) > 
					Math.Abs(values[row * size + position]))
				{
					leader = j;
				}
			}

			return leader;
		}
		#endregion

		private readonly double[] _values;
		private const double Epsilon = 0.00000000000001;
	}
}
