﻿
namespace Cryptography.Math
{
	public struct Transposition
	{
		public uint First { get; }
		public uint Second { get; }

		public Transposition(uint first, uint second)
		{
			First = first;
			Second = second;
		}
		public override string ToString() =>
			$"({First} {Second})";
	}
}
