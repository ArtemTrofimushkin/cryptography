﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cryptography.Math.Helpers;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Math
{
	using Math = System.Math;
	public class PolynomGF :
		IReadOnlyList<ulong>,
		IEquatable<PolynomGF>,
		ICloneable
	{
		#region Properties
		public ulong[] Elements { get; }
		public ulong Character { get; private set; }
		public ulong MaxDegree
		{
			get
			{
				var index = Elements.Any(x => x != 0U)
					? Elements.LastIndexOf(x => x != 0U)
					: -1;

				return index != -1 ? (ulong)index : 0U;
			}
		}
		public bool IsZero =>
			Count == 1 && Elements[0] == 0UL;
		public bool IsConstant =>
			Count == 1;
		#endregion

		#region IReadOnlyList
		public int Count => Elements.Length;
		public ulong this[int index]
		{
			get
			{
				return index <= Count - 1
					? Elements[index]
					: 0U;
			}
		}
		#endregion

		#region Constructors
		public PolynomGF(ulong character) :
			this(0UL, character)
		{
		}
		public PolynomGF(ulong length, ulong character)
		{
			Elements = new ulong[length];
			Character = character;
		}
		public PolynomGF(ulong[] elements, ulong character)
		{
			elements.NotNull(nameof(elements));

			Elements = (ulong[])elements.Clone();
			Character = character;
		}
		public PolynomGF(PolynomGF other)
		{
			other.NotNull(nameof(other));

			Character = other.Character;
			Elements = other.Elements
				.Copy(other.MaxDegree + 1);
		}
		#endregion

		#region Operations
		public PolynomGF Increment()
		{
			var result = this.Clone();

			result.Elements[0] = ( result.Elements[0] + 1UL ) % result.Character;

			if (result.Elements[0] == 0UL)
			{
				var increaseNext = true;
				for (int i = 1; i <= (int)MaxDegree; ++i)
				{
					if (increaseNext)
					{
						result.Elements[i] = ( Elements[i] + 1UL ) % result.Character;
					} else
					{
						result.Elements[i] = Elements[i];
					}

					increaseNext = result.Elements[i] == 0UL;
				}
				if (result.Elements[MaxDegree] == 0UL)
				{
					var newMaxDegree = MaxDegree + 1;
					result = new PolynomGF(newMaxDegree + 1, result.Character);
					result.Elements[newMaxDegree] = 1UL;
				}
			}
			return result;
		}
		public PolynomGF Add(PolynomGF other)
		{
			var degree = this.MaxDegree >= other.MaxDegree
				? this.MaxDegree + 1u
				: other.MaxDegree + 1u;

			var result = new PolynomGF(degree, Character);
			for (var i = 0; i < result.Elements.Length; ++i)
			{
				result.Elements[i] = ( this[i] + other[i] ) % result.Character;
			}
			return result.Clone();
		}
		public PolynomGF Add(PolynomGF other, PolynomGF module)
		{
			return Add(other).LongDivision(module);
		}
		public PolynomGF Multiply(ulong coef)
		{
			var result = this.Clone();
			for (int i = 0; i < result.Elements.Length; ++i)
			{
				result.Elements[i] = ( coef * result.Elements[i] ) % result.Character;
			}
			return result;
		}
		public PolynomGF Multiply(ulong coef, ulong degree)
		{
			var index = this.MaxDegree + 1;
			var result = new PolynomGF(index + degree, Character);

			for (var i = 0; i < this.Elements.Length; ++i)
			{
				result.Elements[i + (int)degree] = ( coef * this[i] ) % result.Character;
			}
			return result;
		}
		public PolynomGF Multiply(ulong coef, ulong degree, PolynomGF module)
		{
			return Multiply(coef, degree).LongDivision(module);
		}
		public PolynomGF Multiply(PolynomGF multiplier)
		{
			multiplier.NotNull(nameof(multiplier));

			var result = new PolynomGF(Character);
			var index = multiplier.MaxDegree + 1;

			for (int i = 0; (ulong)i < index; ++i)
			{
				var tmp = this.Multiply(multiplier[i], (ulong)i);
				result = result.Add(tmp);
			}
			return result;
		}
		public PolynomGF Multiply(PolynomGF multiplier, PolynomGF module)
		{
			return Multiply(multiplier).LongDivision(module);
		}
		public PolynomGF LongDivision(PolynomGF divisor)
		{
			divisor.NotNull(nameof(divisor));

			var result = this.Clone();
			var difference = Count - divisor.Count;
			if (difference < 0)
			{
				return result;
			}

			var coefficient = divisor.Elements
				.Last(el => el != 0u);

			while (difference >= 0 && !result.IsZero)
			{
				var resultCoeff = result.Equation(coefficient);
				var temp = divisor.Multiply(resultCoeff, (ulong)difference);

				result = result.Add(temp);
				difference = (int)( result.MaxDegree - divisor.MaxDegree );
			}

			return result;
		}
		#endregion

		#region Static Operations
		public static bool TryParse(string input, ulong character, out PolynomGF polynom)
		{
			input.NotNull(nameof(input));

			polynom = null;

			var splited = input.Split(new[] { '+', ' ' }, StringSplitOptions.RemoveEmptyEntries);
			if (splited.Length == 0)
			{
				return false;
			}

			var source = new SortedDictionary<int, ulong>();

			for (int i = 0; i < splited.Length; i++)
			{
				var part = splited[i];

				if (part.Contains("x"))
				{
					var cidx = part.IndexOf("*");
					var coeff = cidx > 0
						? ulong.Parse(part.Substring(0, cidx))
						: 1UL;
					var pidx = part.IndexOf("^");
					var power = pidx > 0
						? int.Parse(part.Substring(pidx + 1))
						: 1;

					source[power] = coeff;
				} else
				{
					source[0] = ulong.Parse(part);
				}
			}

			var maxPower = source.Last().Key + 1;
			var result = new ulong[maxPower];
			
			for(int i = 0; i != result.Length; i++)
			{
				if (source.ContainsKey(i))
				{
					result[i] = source[i];
				}
			}

			polynom = new PolynomGF(result, character);

			return true;
		}
		public static PolynomGF Parse(string input, ulong character)
		{
			input.NotNull(nameof(input));

			var result = default(PolynomGF);
			if (!TryParse(input, character, out result))
			{
				throw new InvalidOperationException(
					"Invalid polynom format");
			}

			return result;
		}
		public static PolynomGF Addition(PolynomGF first, PolynomGF second)
		{
			first.NotNull(nameof(first));

			return first.Clone()
				.Add(second);
		}
		public static PolynomGF Addition(PolynomGF first, PolynomGF second, PolynomGF module)
		{
			return Addition(first, second).LongDivision(module);
		}
		public static PolynomGF Multiplication(PolynomGF first, PolynomGF second)
		{
			first.NotNull(nameof(first));

			return first.Clone()
				.Multiply(second);
		}
		public static PolynomGF Multiplication(PolynomGF first, PolynomGF second, PolynomGF module)
		{
			return Multiplication(first, second).LongDivision(module);
		}
		public static PolynomGF Division(PolynomGF first, PolynomGF second)
		{
			first.NotNull(nameof(first));

			return first.LongDivision(second);
		}
		public static PolynomGF One(ulong element, ulong character) =>
			new PolynomGF(element.OneToArray(), character);
		public static PolynomGF FromNumber(ulong number, ulong character)
		{
			var list = new List<ulong>();

			while (number > 0UL)
			{
				list.Add(number % character);
				number /= character;
			}
			return new PolynomGF(list.ToArray(), character);
		}
		#endregion

		#region Clonable
		object ICloneable.Clone() =>
			new PolynomGF(this);
		public PolynomGF Clone()
		{
			return (PolynomGF)( (ICloneable)this ).Clone();
		}
		#endregion

		#region IEqutable
		public bool Equals(PolynomGF other)
		{
			other.NotNull(nameof(other));

			if (Count != other.Count)
			{
				return false;
			}
			return Elements.SequenceEqual(other.Elements);
		}
		#endregion

		#region IEnumerable
		public IEnumerator<ulong> GetEnumerator() =>
			( (IEnumerable<ulong>)Elements ).GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() =>
			Elements.GetEnumerator();
		#endregion

		#region Overrides
		public override bool Equals(object obj)
		{
			var other = obj as PolynomGF;
			return other != null && this.Equals(other);
		}
		public override int GetHashCode() =>
			Elements.GetHashCode();
		public override string ToString()
		{
			var builder = new StringBuilder();

			for (int i = 0; i < Count; i++)
			{
				if (Elements[i] == 0UL)
				{
					continue;
				}
				if (i == 0)
				{
					builder.Append(Elements[i]);
				}
				else
				{
					var pattern = Elements[i] == 1UL
						? $"x"
						: $"{Elements[i]}*x";

					if (i != 1)
					{
						pattern += $"^{i}";
					}
					builder.Append(pattern);
				}
				if (i != Count - 1)
				{
					builder.Append(" + ");
				}
			}
			return builder.ToString();
		}
		#endregion

		#region Helpers
		public ulong ToNumber()
		{
			var exponent = 1UL;
			var number = 0UL;

			for (int i = 0; i < Count; i++)
			{
				var coefficient = this[i];
				if (coefficient != 0UL)
				{
					number += exponent * coefficient;
				}
				exponent *= Character;
			}
			return number;
		}
		private ulong Equation(ulong coefficient)
		{
			var index = Elements.LastOrDefault(x => x != 0U);
			return GaloisFieldHelper.EquationByModulRoot(index, coefficient, Character);
		}
		#endregion
	}
}
