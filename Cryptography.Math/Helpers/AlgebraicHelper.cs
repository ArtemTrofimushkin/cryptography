﻿using System;
using System.Runtime.CompilerServices;

namespace Cryptography.Math.Helpers
{
	using ValType = System.UInt64;
	using Math = System.Math;

	public static class AlgebraicHelper
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsEven(this int number) =>
			( number & 1 ) == 0;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsEven(this ValType number) =>
			( number & 1UL ) == 0;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsOdd(this ValType number) =>
			( number & 1UL ) == 1;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ValType Sqrt(ValType number) =>
			(ValType)Math.Floor(Math.Sqrt(number));

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ulong Abs(this long value) =>
			(ulong)( value > 0L ? value : -value );

		public static int MostSignifantBit(ValType number)
		{
			if (number < 0)
			{
				throw new ArgumentException("Number must be positive", nameof(number));
			}

			const ulong deBruijn = 0x03f79d71b4cb0a89;

			number |= number >> 1;
			number |= number >> 2;
			number |= number >> 4;
			number |= number >> 8;
			number |= number >> 16;
			number |= number >> 32;

			return IndexTable[( number * deBruijn ) >> 58];
		}
		public static ValType IntegerSqrt(ValType number)
		{
			if (number < 0UL)
			{
				throw new ArgumentException("Number must be positive", nameof(number));
			}
			if (number == 0UL)
			{
				return 0UL;
			}

			var bitLength = MostSignifantBit(number) + 1;
			var integer = bitLength / 2;
			var reminder = bitLength % 2;
			ulong x = (ulong)( Math.Pow(2, ( integer + reminder )) );

			while (true)
			{
				var y = ( x + number / x ) / 2;

				if (y >= x)
				{
					return x;
				}

				x = y;
			}
		}
		public static ValType PerfectSquare(ValType number)
		{
			var last = number & 0xF;

			if (last > 9)
			{
				return 0UL;
			}

			if (last == 2 ||
				last == 3 ||
				last == 5 ||
				last == 6 ||
				last == 7 ||
				last == 8)
			{
				return 0UL;
			}

			var sqrt = IntegerSqrt(number);
			return sqrt * sqrt == number ? sqrt : 0UL;
		}

		public static ValType ModuleMultiply(this ValType left, ValType right, ValType module)
		{
			left %= module;
			right %= module;

			ValType result = 0UL;
			while (right > 0UL)
			{
				if (!IsEven(right))
				{
					result = ( result + left ) % module;
					--right;
				} else
				{
					left = ( left << 1 ) % module;
					right >>= 1;
				}
			}

			return result;
		}
		public static ValType ModulePower(this ValType valueBase, ValType exponent, ValType module)
		{
			valueBase %= module;
			exponent %= module;

			ValType result = 1UL;
			while (exponent > 0UL)
			{
				if (exponent.IsOdd())
				{
					result = result.ModuleMultiply(valueBase, module);
				}

				exponent >>= 1;
				valueBase = valueBase.ModuleMultiply(valueBase, module);
			}

			return result;
		}
		public static void TransformToExponential(this ValType number, ref ValType p, ref ValType q)
		{
			ValType result = 0UL;

			while (IsEven(number))
			{
				++result;
				number >>= 1;
			}
			p = result;
			q = number;
		}

		public static ValType EuclideAlgorithm(ValType a, ValType b) =>
			( a == 0UL ) ? b : EuclideAlgorithm(b % a, a);
		public static ValType ExtendedEuclideAlgorithm(ValType a, ValType b, ref ValType s, ref ValType t)
		{
			if (a == 0)
			{
				s = 0;
				t = 1;
				return b;
			}

			ValType temp_s = 0, temp_t = 0;
			ValType d = ExtendedEuclideAlgorithm(b % a, a, ref temp_s, ref temp_t);

			s = temp_t - ( b / a ) * temp_s;
			t = temp_s;

			return d;
		}

		// Khuth Algorithm to avoid negative result
		public static ValType MultiplicativeInverse(ValType u, ValType v)
		{
			ValType inv, u1, u3, v1, v3, t1, t3, q;
			int iter;
			/* Step X1. Initialise */
			u1 = 1;
			u3 = u;
			v1 = 0;
			v3 = v;
			/* Remember odd/even iterations */
			iter = 1;
			/* Step X2. Loop while v3 != 0 */
			while (v3 != 0)
			{
				/* Step X3. Divide and "Subtract" */
				q = u3 / v3;
				t3 = u3 % v3;
				t1 = u1 + q * v1;

				/* Swap */
				u1 = v1;
				v1 = t1;
				u3 = v3;
				v3 = t3;

				iter = -iter;
			}
			/* Make sure u3 = gcd(u,v) == 1 */
			if (u3 != 1)
			{
				return 0;
			}
			if (iter < 0)
			{
				inv = v - u1;
			} else
			{
				inv = u1;
			}
			return inv;
		}

		private static readonly int[] IndexTable = new int[]
		{
			0, 47,  1, 56, 48, 27,  2, 60,
			57, 49, 41, 37, 28, 16,  3, 61,
			54, 58, 35, 52, 50, 42, 21, 44,
			38, 32, 29, 23, 17, 11,  4, 62,
			46, 55, 26, 59, 40, 36, 15, 53,
			34, 51, 20, 43, 31, 22, 10, 45,
			25, 39, 14, 33, 19, 30,  9, 24,
			13, 18,  8, 12,  7,  6,  5, 63
		};
	}
}
