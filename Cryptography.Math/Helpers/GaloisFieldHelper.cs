﻿using System;

namespace Cryptography.Math.Helpers
{
	public static class GaloisFieldHelper
	{
		public static PolynomGF ToPolynom(this ulong number, ulong character) =>
			PolynomGF.FromNumber(number, character);
		public static ulong EquationByModulRoot(ulong a, ulong b, ulong c)
		{
			for (ulong i = 1; i < c; ++i)
			{
				if (( a + b * i ) % c == 0)
					return i;
			}

			throw new InvalidOperationException(
				"Диофантово уравнение в конечном поле не имеет решения.");
		}
		public static PolynomGF ModulePower(this PolynomGF valueBase, ulong exponent, PolynomGF module)
		{
			PolynomGF localResult = PolynomGF.One(1, module.Character);

			while (exponent != 0UL)
			{
				if (exponent.IsOdd())
				{
					localResult = localResult.Multiply(valueBase, module);
				}

				exponent >>= 1;
				valueBase = valueBase.Multiply(valueBase, module);
			}
			return localResult;
		}
	}
}
