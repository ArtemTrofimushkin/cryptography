﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.Math.Abstract
{
	public interface IChineseReminderService<T>
	{
		T FindReminder(IDictionary<ulong, ulong> pairs);
	}

	public interface IChineseReminderService : IChineseReminderService<ulong>
	{
	}
}
