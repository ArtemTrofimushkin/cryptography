﻿
namespace Cryptography.Math.Abstract
{
    public interface ITransposDecompPermut
    {
        Transposition[] GetTranspositionsMultiplication(uint[] prot, uint[] image);
    }
}
