﻿using System.Collections.Generic;
using System.Threading;

namespace Cryptography.Math.Abstract
{
	public interface IFactorizeService<T>
	{
		IDictionary<T, int> Factorize(T product, CancellationToken token);
	}

	public interface IFactorizeService : IFactorizeService<ulong>
	{
	}
}
