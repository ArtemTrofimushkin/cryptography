﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Math
{
	using Math = System.Math;

	public class Vector :
		IEnumerable<double>,
		IEquatable<Vector>,
		ICloneable
	{
		#region Properties
		public double Length =>
			Math.Sqrt(LengthSquared);
		public double LengthSquared =>
			this.Multiply(this);
		public int Size
		{
			get { return _values.Length; }
		}
		public double this[int i]
		{
			get { return _values[i]; }
			set { _values[i] = value; }
		}
		#endregion

		public Vector(int size)
		{
			_values = new double[size];
		}
		public Vector(params double[] values)
		{
			values.NotNull(nameof(values));

			if (!values.Any())
			{
				throw new ArgumentException("Empty values collection", nameof(values));
			}

			_values = (double[])values.Clone();
		}
		public Vector(IEnumerable<double> values)
		{
			values.NotNull(nameof(values));

			var collection = values.ToArray();
			if (!collection.Any())
			{
				throw new ArgumentException("Empty values collection", nameof(values));
			}

			_values = (double[])collection.Clone();
		}

		#region Operations
		public Vector ToNormalized()
		{
			return this.Clone()
				.Normalize();
		}
		public Vector Normalize()
		{
			var length = Length;
			for (int i = 0; i != Size; ++i)
			{
				_values[i] /= length;
			}
			return this;
		}
		public Vector Addition(Vector other)
		{
			this.EnsureEqualSize(other);

			for (int i = 0; i != Size; ++i)
			{
				_values[i] += other[i];
			}

			return this;
		}
		public Vector Substract(Vector other)
		{
			this.EnsureEqualSize(other);

			for (int i = 0; i != Size; ++i)
			{
				_values[i] -= other[i];
			}

			return this;
		}
		public Vector Multiply(double number)
		{
			for (int i = 0; i != Size; ++i)
			{
				_values[i] *= number;
			}
			return this;
		}
		public Vector Divide(double number) =>
			this.Multiply(1.0 / number);
		public Vector Project(Vector other) =>
			Vector.Multiply(other, this.ProjectCoefficient(other));
		public double ProjectCoefficient(Vector other)
		{
			this.EnsureEqualSize(other);
			return this.Multiply(other) / other.LengthSquared;
		}
		public double Multiply(Vector other)
		{
			this.EnsureEqualSize(other);

			var result = 0.0;
			for (int i = 0; i != Size; ++i)
			{
				result += _values[i] * other[i];
			}

			return result;
		}
		#endregion

		#region Static operations
		public static Vector Zero(int size)
		{
			return new Vector(size);
		}
		public static Vector One(int size)
		{
			return new Vector(
				Enumerable.Range(1, size).Select(x => x * 1.0));
		}
		public static Vector Addition(Vector left, Vector right)
		{
			left.NotNull(nameof(left));
			right.NotNull(nameof(right));

			return left.Clone()
				.Addition(right);
		}
		public static Vector Substract(Vector left, Vector right)
		{
			left.NotNull(nameof(left));
			right.NotNull(nameof(right));

			return left.Clone()
				.Substract(right);
		}
		public static Vector Multiply(Vector left, double right)
		{
			left.NotNull(nameof(left));

			return left.Clone()
				.Multiply(right);
		}
		public static Vector Divide(Vector left, double right)
		{
			left.NotNull(nameof(left));

			return left.Clone()
				.Divide(right);
		}
		public static double Multiply(Vector left, Vector right)
		{
			left.NotNull(nameof(left));
			right.NotNull(nameof(right));

			return left.Multiply(right);
		}
		public static Vector Project(Vector left, Vector right)
		{
			left.NotNull(nameof(left));
			right.NotNull(nameof(right));

			return left.Project(right);
		}
		public static IList<Vector> Ortogonalize(IEnumerable<Vector> source)
		{
			source.NotNull(nameof(source));

			var collection = source.ToArray();
			Vector.EnsureEqualSize(collection);

			var result = new List<Vector>(collection.Length);

			for (int i = 0; i != collection.Length; ++i)
			{
				result.Add(collection[i].Clone());
				for (int j = i + 1; j != collection.Length; ++j)
				{
					var product = Vector.Multiply(collection[i], collection[j]);
					if (Math.Abs(product * product - collection[i].LengthSquared * collection[j].LengthSquared) < Double.Epsilon)
					{
						throw new InvalidOperationException(
							$"Vectors: {collection[i]} and {collection[j]} are lineary dependent.");
					}
				}
				for (int k = 0; k < i; ++k)
				{
					result[i] -= collection[i].Project(result[k]);
				}
			}

			return result;
		}
		#endregion

		#region Operators 
		public static Vector operator +(Vector left, Vector right) =>
			Vector.Addition(left, right);
		public static Vector operator -(Vector left, Vector right) =>
			Vector.Substract(left, right);
		public static Vector operator *(Vector left, double right) =>
			Vector.Multiply(left, right);
		public static Vector operator /(Vector left, double right) =>
			Vector.Divide(left, right);
		public static explicit operator double[] (Vector vector)
		{
			vector.NotNull(nameof(vector));
			return (double[])vector._values.Clone();
		}
		#endregion

		#region Equtable
		public override int GetHashCode() =>
			_values.GetHashCode();
		public override bool Equals(object obj)
		{
			var other = obj as Vector;
			if (other == null)
			{
				return false;
			}
			return this.Equals(other);
		}
		public bool Equals(Vector other)
		{
			other.NotNull(nameof(other));

			if (Size != other.Size)
			{
				return false;
			}
			for (int i = 0; i != Size; ++i)
			{
				if (Math.Abs(_values[i] - other[i]) > Double.Epsilon)
				{
					return false;
				}
			}
			return true;
		}
		#endregion

		#region Clonable
		object ICloneable.Clone() =>
			new Vector(_values);
		public Vector Clone()
		{
			return (Vector)( (ICloneable)this ).Clone();
		}
		#endregion

		#region IEnumerable
		public IEnumerator<double> GetEnumerator() =>
			_values.Cast<double>().GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() =>
			this.GetEnumerator();
		#endregion

		public override string ToString() =>
			$"[ {String.Join("; ", _values)} ]";

		#region Helpers
		private void EnsureEqualSize(Vector other)
		{
			other.NotNull(nameof(other));
			if (Size != other.Size)
			{
				throw new ArgumentException("Must be equal size", nameof(other));
			}
		}
		private static void EnsureEqualSize(IReadOnlyList<Vector> source)
		{
			int min = Int32.MaxValue,
				max = 0;

			for (int i = 0; i != source.Count; ++i)
			{
				if (source[i].Size < min)
				{
					min = source[i].Size;
				}
				if (source[i].Size > max)
				{
					max = source[i].Size;
				}
			}

			if (min != max)
			{
				throw new InvalidOperationException(
					$"Vectors must be equal size. Minimum size of vector collection: {min}, maximum size of vector collection: {min}");
			}

			if (min < source.Count)
			{
				throw new InvalidOperationException(
					$"Vectors system must be linearly independent.");
			}
		}
		#endregion

		private readonly double[] _values;
	}
}
