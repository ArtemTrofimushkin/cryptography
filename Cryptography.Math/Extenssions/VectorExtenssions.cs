﻿using System.Collections.Generic;
using System.Linq;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Math.Extenssions
{
	public static class VectorExtenssions
	{
		public static bool IsOrtogonational(this IEnumerable<Vector> system, 
			double criteria = 0.000000000000000000001)
		{
			system.NotNull(nameof(system));

			return system.OrtogonalizeResults()
				.All(x => x < criteria);
		}
		public static IEnumerable<double> OrtogonalizeResults(this IEnumerable<Vector> system)
		{
			system.NotNull(nameof(system));

			var collection = system.ToArray();
			for (int i = 0; i != collection.Length; ++i)
			{
				var vector = collection[i];
				for (int j = i + 1; j < collection.Length; ++j)
				{
					yield return vector.Multiply(collection[j]);
				}
			}
		}
		public static IList<Vector> ToOrtogonationalSystem(this IEnumerable<Vector> system)
		{
			system.NotNull(nameof(system));

			return system.IsOrtogonational() 
				? system.ToList()
				: Vector.Ortogonalize(system);
		}
	}
}
