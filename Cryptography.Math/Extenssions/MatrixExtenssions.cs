﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Math.Extenssions
{
	public static class MatrixExtenssions
	{
		public static SquaredMatrix ToSquaredMatrix(this IEnumerable<Vector> system)
		{
			system.NotNull(nameof(system));

			var collection = system.ToArray();
			if (collection.Any(vector => vector.Size != collection.Length))
			{
				throw new InvalidOperationException(
					"Vector's system must be equal size");
			}

			var source = collection.SelectMany(vector => vector);
			return new SquaredMatrix(collection.Length, source)
				.ToTransposed();
		}
	}
}
