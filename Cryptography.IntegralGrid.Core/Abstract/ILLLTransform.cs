﻿using System.Collections.Generic;
using Cryptography.Math;

namespace Cryptography.IntegralGrid.Core.Abstract
{
	public interface ILLLTransform
	{
		double ReduceCoefficient { get; }

		bool IsReduced(IEnumerable<Vector> source);
		IList<Vector> ApplyReduceTransform(IEnumerable<Vector> source);
	}
}
