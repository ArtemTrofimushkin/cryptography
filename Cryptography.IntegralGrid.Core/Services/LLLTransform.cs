﻿using System;
using System.Collections.Generic;
using System.Linq;

using Cryptography.IntegralGrid.Core.Abstract;
using Cryptography.Math;
using Cryptography.Utills.Extenssions;

namespace Cryptography.IntegralGrid.Core.Services
{
	using Math = System.Math;

	public class LLLTransform : ILLLTransform
	{
		public double ReduceCoefficient { get; }

		public LLLTransform() : 
			this(FourThird)
		{
		}
		public LLLTransform(double reduceCoefficient)
		{
			if (reduceCoefficient > FourThird ||
				reduceCoefficient <= 0.0)
			{
				throw new ArgumentException(
					$"Reduction coeffitient must be positive, and less then {FourThird}");
			}

			ReduceCoefficient = reduceCoefficient;
		}

		public bool IsReduced(IEnumerable<Vector> source)
		{
			source.NotNull(nameof(source));

			var pairs = source.Zip(source.Skip(1), 
				(x, y) => new { Prev = x, Next = y });

			return pairs.All(x => this.IsReduced(x.Prev, x.Next));
		}
		public IList<Vector> ApplyReduceTransform(IEnumerable<Vector> source)
		{
			source.NotNull(nameof(source));

			var collection = source.ToArray();
			var coefficients = new Vector[collection.Length];
			var orthogonalized = this.Orthogonalize(collection, coefficients);

			for (int i = 0; i < collection.Length - 1; )
			{
				if (this.IsReduced(collection[i], collection[i + 1]))
				{
					++i;
				}
				else
				{
					orthogonalized[i + 1] += orthogonalized[i] * coefficients[i + 1][i];
					coefficients[i][i] = collection[i].ProjectCoefficient(orthogonalized[i + 1]);

					this.CorrectCoefficients(i, coefficients);

					orthogonalized[i] -= orthogonalized[i + 1] * coefficients[i][i];

					this.Swap(i, collection, coefficients, orthogonalized);
					this.ProjectCoefficients(i, collection, coefficients, orthogonalized);

					if (Math.Abs(coefficients[i + 1][i]) > Half)
					{
						this.Reduce(i + 1, collection, collection);
					}

					i = Math.Max(i - 1, 0);
				}
			}

			return new List<Vector>(collection);
		}

		private Vector[] Orthogonalize(
			Vector[] source, 
			Vector[] coefficients)
		{
			var orthogonalized = new Vector[source.Length];

			for (int i = 0; i < source.Length; i++)
			{
				coefficients[i] = Vector.Zero(source[i].Size);
				coefficients[i][i] = 1;
				orthogonalized[i] = source[i];

				for (int j = 0; j <= i - 1; j++)
				{
					coefficients[i][j] = source[i].ProjectCoefficient(orthogonalized[j]);
					orthogonalized[i] -= orthogonalized[j] * coefficients[i][j];
				}

				this.Reduce(i, source, coefficients);	
			}

			return orthogonalized;
		}
		private void Reduce(int index,
			Vector[] source,
			Vector[] coefficients)
		{
			for (var j = index - 1; j >= 0; )
			{
				source[index] -= source[j] * Math.Round(coefficients[index][j]);
				coefficients[index] -= coefficients[j] * Math.Round(coefficients[index][j]);
				--j;
			}
		}
		private bool IsReduced(Vector first, Vector second) =>
			first.LengthSquared <= ReduceCoefficient * second.LengthSquared;
		private void CorrectCoefficients(int index,
			Vector[] coefficients)
		{
			coefficients[index][index + 1] = 1;
			coefficients[index + 1][index] = 1;
			coefficients[index + 1][index + 1] = 0;
		}
		private void Swap(int index,
			Vector[] source,
			Vector[] coefficients,
			Vector[] orthogonalized)
		{
			coefficients.Swap(index, index + 1);
			orthogonalized.Swap(index, index + 1);
			source.Swap(index, index + 1);
		}
		private void ProjectCoefficients(int index,
			Vector[] source,
			Vector[] coefficients,
			Vector[] orthogonalized)
		{
			for (int k = index + 2; k < source.Length; k++)
			{
				coefficients[k][index] = source[k].ProjectCoefficient(orthogonalized[index]);
				coefficients[k][index + 1] = source[k].ProjectCoefficient(orthogonalized[index + 1]);
			}
		}

		private const double Half = 1.0 / 2.0;
		private const double FourThird = 4.0 / 3.0;
	}
}
