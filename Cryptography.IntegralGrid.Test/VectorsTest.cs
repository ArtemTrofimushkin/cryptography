﻿using System;
using Cryptography.Math;
using Cryptography.Math.Extenssions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.IntegralGrid.Test
{
	using Math = System.Math;

	[TestClass]
	public class VectorsTest
	{
		[TestMethod]
		public void VectorOperations()
		{
			var vector1 = new Vector(1, 2, 3, 4);
			var product = Vector.Multiply(vector1, vector1);
			Assert.AreEqual(product, vector1.LengthSquared);
		}

		[TestMethod]
		public void VectorOrtogonalization()
		{
			var array = new Vector[3]
			{
				new Vector(3, -1, -1, 0),
				new Vector(2, -1, 0, 1),
				new Vector(1, 1, -2, 3)
			};

			var ortogonalizeArray = Vector.Ortogonalize(array);

			Assert.IsTrue(Math.Abs(Vector.Multiply(ortogonalizeArray[0], ortogonalizeArray[1])) < 0.000000001, "1");
			Assert.IsTrue(Math.Abs(Vector.Multiply(ortogonalizeArray[0], ortogonalizeArray[2])) < 0.000000001, "2");
			Assert.IsTrue(Math.Abs(Vector.Multiply(ortogonalizeArray[1], ortogonalizeArray[2])) < 0.000000001, "3");
		}

		[TestMethod]
		public void VectorMultiply()
		{
			var vector1 = new Vector(new double[] { 1, 2, 3, 4 });
			var result = vector1 + vector1;

			Assert.AreEqual(result, vector1 * 2.0);
		}

		[TestMethod]
		public void VectorSystemToMatrix()
		{
			var system = new[]
			{
				new Vector(1, 2, 3),
				new Vector(4, 5, 6),
				new Vector(7, 8, 9)
			};

			var matrix = system.ToSquaredMatrix();

			Assert.AreEqual(matrix, new SquaredMatrix(3, 1, 4, 7, 2, 5, 8, 3, 6, 9));
		}
	}
}
