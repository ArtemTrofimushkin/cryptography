﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Cryptography.IntegralGrid.Core.Services;
using Cryptography.Math;

namespace Cryptography.IntegralGrid.Test
{
	[TestClass]
	public class LLLTransformTest
	{
		[TestMethod]
		public void LLLTransformTest_ShouldReturn_Basis()
		{
			var service = new LLLTransform();
			var source = new Vector[3]
			{
				new Vector(3, -1, -1, 0),
				new Vector(2, -1, 0, 1),
				new Vector(1, 1, -2, 3)
			};

			var result = service.ApplyReduceTransform(source);

			Assert.IsFalse(service.IsReduced(source), 
				$"Input source not {service.ReduceCoefficient}-reducted");

			Assert.IsTrue(service.IsReduced(result), 
				$"Result source is {service.ReduceCoefficient}-reducted");
		}
	}
}
