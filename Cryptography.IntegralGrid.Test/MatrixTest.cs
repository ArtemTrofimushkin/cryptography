﻿using System;

using Cryptography.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.IntegralGrid.Test
{
	using Math = System.Math;

	[TestClass]
	public class MatrixTest
	{
		[TestMethod]
		public void MatrixAddition()
		{
			var matrix = new SquaredMatrix(3, 1, 2, 3, 4, 5, 6, 7, 8, 9);
			var result = matrix + matrix;

			Assert.AreEqual(result, new SquaredMatrix(3, 2, 4, 6, 8, 10, 12, 14, 16, 18));
		}

		[TestMethod]
		public void MatrixMultiply()
		{
			var matrix = new SquaredMatrix(3, 1, 2, 3, 4, 5, 6, 7, 8, 9);
			var result = matrix * matrix;

			Assert.AreEqual(result, new SquaredMatrix(3, 30, 36, 42, 66, 81, 96, 102, 126, 150));
		}

		[TestMethod]
		public void MatrixTranspose()
		{
			var matrix = new SquaredMatrix(3, 1, 2, 3, 4, 5, 6, 7, 8, 9);
			var result = matrix.ToTransposed();

			Assert.AreEqual(result, new SquaredMatrix(3, 1, 4, 7, 2, 5, 8, 3, 6, 9));
		}

		[TestMethod]
		public void MatrixDeterminantZero()
		{
			var matrix = new SquaredMatrix(3, 1, 2, 3, 4, 5, 6, 7, 8, 9);
			var result = matrix.Determinant();

			Assert.IsTrue(Math.Abs(result) < 0.00000000000001);
		}

		[TestMethod]
		public void MatrixDeterminantNotZero()
		{
			var matrix = new SquaredMatrix(3, 2, 4, 9, 5, 7, 8, 1, 10, 12);
			var result = matrix.Determinant();

			Assert.AreEqual(result, 187);
		}

		[TestMethod]
		public void MatrixInversed()
		{
			var matrix = new SquaredMatrix(3, 1, 2, 5, 2, 3, 6, 7, 4, 0);
			var result = matrix.ToInversed();

			Assert.AreEqual(result, new SquaredMatrix(3, 4.8, -4, 0.6, -8.4, 7, -0.8, 2.6, -2, 0.2));
		}

		[TestMethod]
		public void MatrixInversedZeroDeterminant()
		{
			var catched = false;
			var matrix = new SquaredMatrix(3, 1, 2, 3, 4, 5, 6, 7, 8, 9);

			try
			{
				var result = matrix.ToInversed();
			}
			catch(Exception ex)
			{
				catched = true;
				var message = ex.Message;

				Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
				Assert.AreEqual(message, "Determinator is equal to 0");
			}

			if (!catched)
			{
				Assert.Fail("Not exception");
			}
		}

		[TestMethod]
		public void MatrixIdentity()
		{
			var matrix = SquaredMatrix.Identity(4);
			Assert.AreEqual(matrix, new SquaredMatrix(4, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1));
		}
	}
}
