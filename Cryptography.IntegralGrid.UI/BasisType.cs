﻿using System.ComponentModel;

namespace Cryptography.IntegralGrid.UI
{
	public enum BasisType
	{
		[Description("Двухмерный")]
		Basis2D,

		[Description("Трехмерный")]
		Basis3D
	}
}
