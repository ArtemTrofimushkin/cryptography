﻿using System.Collections;

namespace Cryptography.IntegralGrid.UI.Abstract
{
	public interface IIntegralGridGenerator
	{
		bool IsBasisCorrect();
		IEnumerable GenerateIntegralGrid(int step);
		void AppendIntegralGrid(IEnumerable source);
		void ClearIntegralGrid();
	}
}
