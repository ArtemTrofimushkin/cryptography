﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Cryptography.Host.Api.ViewModels;
using Cryptography.IntegralGrid.Core.Abstract;
using Cryptography.IntegralGrid.UI.ViewModels;
using Cryptography.Math;
using Cryptography.Math.Extenssions;
using Cryptography.Utills.Extenssions;
using Cryptography.Utills.Helpers;

namespace Cryptography.IntegralGrid.UI.Documents
{
	public class TransformDocument : DocumentBase
	{
		#region Commands
		public ICommand ApplyTransformCommand { get; private set; }
		public ICommand EditVectorCommand { get; private set; }
		#endregion

		#region Properties
		public int VectorCount
		{
			get { return _vectorCount; }
			set
			{
				this.UpdateProperty(value, ref _vectorCount);
				this.UpdateVectorSystem();
			}
		}
		public VectorEditViewModel VectorEdit
		{
			get { return _edit; }
			set { this.UpdateProperty(value, ref _edit); }
		}
		public ObservableCollection<Vector> SourceVectors
		{
			get { return _sourceVectors; }
			set { this.UpdateProperty(value, ref _sourceVectors); }
		}
		public IList<Vector> ReducedVectors
		{
			get { return _reducedVectors; }
			set { this.UpdateProperty(value, ref _reducedVectors); }
		}
		#endregion

		public TransformDocument(ILLLTransform transform)
		{
			transform.AssignTo(out _transform, nameof(transform));

			Title = "Применение LLL преобразования";
		}

		#region Overrides
		protected override void CreateCommands()
		{
			base.CreateCommands();

			ApplyTransformCommand = CommandFactory.CreateCommand(
				this.ApplyTransformExecute,
				this.ApplyTransformCanExecute);
			EditVectorCommand = CommandFactory.CreateCommand<Vector>(
				this.EditVectorExecute);
		}
		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals(propertyName, nameof(VectorCount), StringComparison.Ordinal))
			{
				return VectorCount <= 1
					? "Количество векторов должно быть более одного"
					: String.Empty;
			}

			return base.ValidateProperty(propertyName);
		}
		#endregion

		#region Command Methods
		private async void ApplyTransformExecute()
		{
			await this.ExecuteOperationAsync(this.ApplyTransformAsync);
		}
		private void EditVectorExecute(Vector source)
		{
			if (source == null)
			{
				return;
			}

			VectorEdit = this.CreateEditModel(source);
		}
		private bool ApplyTransformCanExecute()
		{
			return _vectorCount > 1 &&
				_sourceVectors != null &&
				this.IsBasisCorrect(SourceVectors) &&
				_transform.IsReduced(_sourceVectors);
		}
		#endregion

		#region Helpers
		private VectorEditViewModel CreateEditModel(Vector source)
		{
			var model = new VectorEditViewModel(source)
			{
				CommandFactory = CommandFactory
			};

			model.Completed += this.OnEditCompteted;

			return model;
		}
		private void OnEditCompteted(Vector source, IEnumerable<double> original)
		{
			var values = original.ToArray();

			for (int i = 0; i < source.Size; i++)
			{
				source[i] = values[i];
			}

			// TODO: Переписать
			SourceVectors = new ObservableCollection<Vector>(SourceVectors.Select(x => x));
		}
		private void UpdateVectorSystem()
		{
			if (VectorCount < 2)
			{
				return;
			}

			var range = Enumerable.Range(0, VectorCount)
				.Select(i => Vector.Zero(VectorCount));

			SourceVectors = new ObservableCollection<Vector>(range);
			ReducedVectors = range.ToList();
		}
		private bool IsBasisCorrect (IList<Vector> vectors) =>
			vectors.ToSquaredMatrix().HasDeterminant;
		private async Task ApplyTransformAsync(CancellationToken token)
		{
			var reduced = await Task.Run(
					() => _transform.ApplyReduceTransform(SourceVectors))
				.WithCancellation(token);

			if (!token.IsCancellationRequested)
			{
				ReducedVectors = reduced;
			}
		}
		#endregion

		private int _vectorCount;
		private VectorEditViewModel _edit;
		private ObservableCollection<Vector> _sourceVectors;
		private IList<Vector> _reducedVectors;
		private readonly ILLLTransform _transform;
	}
}
