﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

using Cryptography.Host.Api.Attributes;
using Cryptography.Host.Api.ViewModels;
using Cryptography.IntegralGrid.UI.Abstract;
using Cryptography.IntegralGrid.UI.ViewModels;
using Cryptography.Utills.Helpers;

namespace Cryptography.IntegralGrid.UI.Documents
{
	public class IntergralGridVisualizationDocument : DocumentBase
	{
		public ICommand GenerateIntegralGridCommand { get; private set; }
		public ICommand ClearIntegralGridCommand { get; private set; }

		public IReadOnlyCollection<EnumDescriptionViewModel> AviableBasises => _basises.Keys.ToArray();

		[Computed(nameof(SelectedIntegralBasis))]
		public EnumDescriptionViewModel SelectedBasis
		{
			get { return _selectedBasis; }
			set { this.UpdateProperty(value, ref _selectedBasis); }
		}
		public IIntegralGridGenerator SelectedIntegralBasis => _basises[SelectedBasis];

		public int Dimenssion
		{
			get { return _dimenssion; }
			set { this.UpdateProperty(value, ref _dimenssion); }
		}

		public IntergralGridVisualizationDocument()
		{
			Title = "Визуализация целочисленных решеток";

			_basises[EnumDescriptionViewModel.Create(BasisType.Basis2D)] = new BasisViewModel2D(0, Dimenssion);
			_basises[EnumDescriptionViewModel.Create(BasisType.Basis3D)] = new BasisViewModel3D();

			SelectedBasis = _basises.Keys.First();
		}

		protected override void CreateCommands()
		{
			base.CreateCommands();

			GenerateIntegralGridCommand = CommandFactory.CreateCommand(
				this.GenerateIntegralBasisExecute, 
				this.GenerateIntegralBasisCanExecute);
			ClearIntegralGridCommand = CommandFactory.CreateCommand(
				this.ClearIntegralGridExecute,
				this.ClearIntegralGridCanExecute);
		}

		private async void GenerateIntegralBasisExecute()
		{
			await this.ExecuteOperationAsync(
				this.GenerateIntegralBasisAsync);
		}
		private void ClearIntegralGridExecute() =>
			SelectedIntegralBasis.ClearIntegralGrid();

		private bool GenerateIntegralBasisCanExecute() => 
			SelectedIntegralBasis.IsBasisCorrect();
		private bool ClearIntegralGridCanExecute() =>
			SelectedIntegralBasis.IsBasisCorrect();

		private async Task GenerateIntegralBasisAsync(CancellationToken token)
		{
			var models = await Task.Run(() => 
					SelectedIntegralBasis.GenerateIntegralGrid(20))
				.WithCancellation(token);

			SelectedIntegralBasis.AppendIntegralGrid(models);
		}

		private int _dimenssion = 500;
		private EnumDescriptionViewModel _selectedBasis;
		private readonly IDictionary<EnumDescriptionViewModel, IIntegralGridGenerator> _basises =
			new Dictionary<EnumDescriptionViewModel, IIntegralGridGenerator>();
	}
}
