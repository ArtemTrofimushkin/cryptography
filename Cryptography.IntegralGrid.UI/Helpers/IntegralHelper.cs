﻿using System;

namespace Cryptography.IntegralGrid.UI.Helpers
{
	using Math = System.Math;

	public static class IntegralHelper
	{
		public static bool IsIntegral(this double number)
		{
			return Math.Abs(number % 1.0) < Double.Epsilon;
		}
	}
}
