﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

using Cryptography.Host.Api.ViewModels;
using Cryptography.IntegralGrid.UI.Abstract;
using Cryptography.IntegralGrid.UI.Helpers;
using Cryptography.IntegralGrid.UI.ViewModels.Geometry;
using Cryptography.Utills.Extenssions;

namespace Cryptography.IntegralGrid.UI.ViewModels
{
	using Math = System.Math;

	public class BasisViewModel2D : ViewModelBase, IIntegralGridGenerator
	{
		public ObservableCollection<ShapeModel> Shapes { get; } = 
			new ObservableCollection<ShapeModel>();
		public IReadOnlyList<Vector2DViewModel> Vectors { get; }
		private Vector2DViewModel First => Vectors[0];
		private Vector2DViewModel Second => Vectors[1];

		public BasisViewModel2D(int min, int max)
		{
			#region Validate
			if (min >= max)
			{
				throw new ArgumentOutOfRangeException(nameof(min),
					"Minimum must be less than Maximum");
			}
			#endregion

			_min = min;
			_max = max;

			Vectors = new[]
			{
				new Vector2DViewModel(7, 4),
				new Vector2DViewModel(3, 1)
			};

			this.GenerateCoordinatesGrid();
		}
		public bool IsBasisCorrect()
		{
			if (!First.IsValid || !Second.IsValid)
			{
				return false;
			}

			return Math.Abs(Second.X * First.Y - First.X * Second.Y) > Double.Epsilon;
		}
		public IEnumerable GenerateIntegralGrid(int step)
		{
			this.ClearIntegralGrid();

			var vectorI = First.ToVector();
			var vectorJ = Second.ToVector();
			var xCoeff = (int) Vectors.Select(v => Math.Abs(v.X)).Max();
			var yCoeff = (int) Vectors.Select(v => Math.Abs(v.Y)).Max();

			for(int x = - xCoeff * _max; x < xCoeff * _max; x += step)
			{
				for(int y = - yCoeff * _max; y < yCoeff * _max; y += step)
				{
					var vector = Vector.Multiply(x, vectorI) +
						Vector.Multiply(y, vectorJ);

					if (!this.CoordinatesInBound(vector.X, vector.Y) ||
						!this.CoordinatesIntegral(vector.X / step, vector.Y / step))
					{
						continue;
					}

					yield return new PointModel(vector.X, _max - vector.Y);
				}
			}
		}
		public void AppendIntegralGrid(IEnumerable source)
		{
			source.NotNull(nameof(source));

			Shapes.AddRange(
				source.OfType<PointModel>());
		}
		public void ClearIntegralGrid()
		{
			Shapes.RemoveRange(
				Shapes.OfType<PointModel>());
		}

		private bool CoordinatesInBound(double x, double y)
		{
			return x >= _min && x < _max &&
				y >= _min && y < _max;
		}
		private bool CoordinatesIntegral(double x, double y)
		{
			return x.IsIntegral() && y.IsIntegral();
		}
		private void GenerateCoordinatesGrid()
		{
			for (int y = _min; y <= _max; y += CoordinatesGridStep)
			{
				Shapes.Add(
					this.VerticalLine(y));
			}

			for (int x = _min; x <= _max; x += CoordinatesGridStep)
			{
				Shapes.Add(
					this.HorizontalLine(x));
			}
			Shapes.Add(
				this.VerticalArrow());
			Shapes.Add(
				this.HorizontalArrow());
		}
		private LineModel VerticalLine(int y)
		{
			return new LineModel
			{
				X = _min,
				Y = y,
				EndX = _max,
				EndY = y
			};
		}
		private LineModel HorizontalLine(int x)
		{
			return new LineModel
			{
				X = x,
				Y = _min,
				EndX = x,
				EndY = _max
			};
		}
		private ArrowModel VerticalArrow()
		{
			return new ArrowModel(3, 7)
			{
				ArrowDirection = ArrowDirection.Vertical,
				X = _min,
				Y = _max,
				EndX = _min,
				EndY = _min - CoordinatesGridStep / 2
			};
		}
		private ShapeModel HorizontalArrow()
		{
			return new ArrowModel(3, 7)
			{
				ArrowDirection = ArrowDirection.Horizontal,
				X = _min,
				Y = _max,
				EndX = _max + CoordinatesGridStep / 2,
				EndY = _max
			};
		}

		private readonly int _min;
		private readonly int _max;
		const int CoordinatesGridStep = 20;
	}
}
