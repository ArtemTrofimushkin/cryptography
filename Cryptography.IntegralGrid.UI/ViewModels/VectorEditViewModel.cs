﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Host.Api.Abstract;
using Cryptography.Math;

namespace Cryptography.IntegralGrid.UI.ViewModels
{
	using Double = ValueViewModel<double>;

	public class VectorEditViewModel
	{
		public event Action<Vector, IEnumerable<double>> Completed;

		public ICommandFactory CommandFactory
		{
			get { return _factory; }
			set
			{
				_factory = value;
				EditCompleted = _factory.CreateCommand(this.OnEditCompleted);
			}
		}
		public ICommand EditCompleted { get; private set; }
		public IList<Double> Line { get; }

		public VectorEditViewModel(Vector source)
		{
			Line = source.Select(x => new Double(x))
				.ToList();

			_original = source;
		}

		private void OnEditCompleted() =>
			Completed?.Invoke(_original, Line.Select(x => x.Value));

		private ICommandFactory _factory;
		private readonly Vector _original;
	}
}
