﻿using System;
using System.Windows.Media.Media3D;
using Cryptography.Host.Api.ViewModels;

namespace Cryptography.IntegralGrid.UI.ViewModels
{
	public class Vector3DViewModel : ViewModelBase
	{
		public double X
		{
			get { return _x; }
			set { this.UpdateProperty(value, ref _x); }
		}
		public double Y
		{
			get { return _y; }
			set { this.UpdateProperty(value, ref _y); }
		}
		public double Z
		{
			get { return _z; }
			set { this.UpdateProperty(value, ref _z); }
		}

		public Vector3D ToVector3D() =>
			new Vector3D(X, Y, Z);

		public override string Error
		{
			get
			{
				if (X <= Double.Epsilon && 
					Y <= Double.Epsilon && 
					Z <= Double.Epsilon)
				{
					return "Укажите вектор";
				}

				return base.Error;
			}
		}

		private double _x;
		private double _y;
		private double _z;
	}
}
