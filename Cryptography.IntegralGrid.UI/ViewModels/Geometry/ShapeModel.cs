﻿using System;

namespace Cryptography.IntegralGrid.UI.ViewModels.Geometry
{
	public class ShapeModel
	{
		public double X { get; set; }
		public double Y { get; set; }
	}
}
