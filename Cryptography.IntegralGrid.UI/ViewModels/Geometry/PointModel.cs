﻿namespace Cryptography.IntegralGrid.UI.ViewModels.Geometry
{
	public class PointModel : ShapeModel
	{
		public int Size { get; set; }
		public PointModel(double x, double y)
		{
			Size = 4;
			X = x;
			Y = y - Size;
		}
	}
}
