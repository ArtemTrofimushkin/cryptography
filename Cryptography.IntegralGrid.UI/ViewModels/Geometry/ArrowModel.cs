﻿using System;

namespace Cryptography.IntegralGrid.UI.ViewModels.Geometry
{
	public class ArrowModel : LineModel
	{
		public ArrowDirection ArrowDirection { get; set; }
		public double LeftArrowX
		{
			get
			{
				return ArrowDirection == ArrowDirection.Vertical ?
					EndX - ArrowRange : EndX - ArrowHeight;
			}
		}
		public double LeftArrowY
		{
			get
			{
				return ArrowDirection == ArrowDirection.Vertical ?
					EndY + ArrowHeight : EndY - ArrowRange;
			}
		}
		public double RightArrowX
		{
			get
			{
				return ArrowDirection == ArrowDirection.Vertical ?
					EndX + ArrowRange : EndX - ArrowHeight;
			}
		}
		public double RightArrowY
		{
			get
			{
				return ArrowDirection == ArrowDirection.Vertical ?
					EndY + ArrowHeight : EndY + ArrowRange;
			}
		}
		public int ArrowRange { get; set; }
		public int ArrowHeight { get; set; }

		public ArrowModel(int range, int height)
		{
			if (range < 0)
			{
				throw new ArgumentException("Range must be positive", nameof(range));
			}
			if (height < 0)
			{
				throw new ArgumentException("Height must be positive", nameof(height));
			}

			ArrowRange = range;
			ArrowHeight = height;
		}
	}
}
