﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.IntegralGrid.UI.ViewModels.Geometry
{
	public class LineModel : ShapeModel
	{
		public int EndX { get; set; }
		public int EndY { get; set; }
	}
}
