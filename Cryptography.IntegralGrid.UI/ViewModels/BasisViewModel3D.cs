﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Media.Media3D;

using Cryptography.Host.Api.ViewModels;
using Cryptography.IntegralGrid.UI.Abstract;

namespace Cryptography.IntegralGrid.UI.ViewModels
{
	using Math = System.Math;

	public class BasisViewModel3D : ViewModelBase, IIntegralGridGenerator
	{
		public IList<Point3D> Points { get; private set; }
		public IList<Vector3DViewModel> Vectors { get; }
		public Vector3DViewModel First => Vectors[0];
		public Vector3DViewModel Second => Vectors[1];
		public Vector3DViewModel Third => Vectors[2];

		public BasisViewModel3D()
		{
			Vectors = new Vector3DViewModel[]
			{
				new Vector3DViewModel(),
				new Vector3DViewModel(),
				new Vector3DViewModel()
			};
		}

		public bool IsBasisCorrect()
		{
			if (!First.IsValid || 
				!Second.IsValid || 
				!Third.IsValid)
			{
				return false;
			}

			var determinant =
				( First.X * Second.Y * Third.Z + First.Y * Second.Z * Third.X + Second.X * Third.Y * First.Z ) -
				( First.Z * Second.Y * Third.X + First.Y * Second.X * Third.Z + Second.Z * Third.Y * First.Z );

			return Math.Abs(determinant) > Double.Epsilon;
		}
		public IEnumerable GenerateIntegralGrid(int step)
		{
			throw new NotImplementedException();
		}
		public void AppendIntegralGrid(IEnumerable source)
		{
			throw new NotImplementedException();
		}
		public void ClearIntegralGrid()
		{
			throw new NotImplementedException();
		}
	}
}
