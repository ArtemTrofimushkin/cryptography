﻿using System;
using System.Windows;
using Cryptography.Host.Api.ViewModels;

namespace Cryptography.IntegralGrid.UI.ViewModels
{
	public class Vector2DViewModel : ViewModelBase
	{
		public double X
		{
			get { return _x; }
			set { this.UpdateProperty(value, ref _x); }
		}
		public double Y
		{
			get { return _y; }
			set { this.UpdateProperty(value, ref _y); }
		}

		public Vector2DViewModel()
		{
		}
		public Vector2DViewModel(double x, double y)
		{
			X = x;
			Y = y;
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals("X", propertyName, StringComparison.Ordinal) ||
				String.Equals("Y", propertyName, StringComparison.Ordinal))
			{
				return X <= Double.Epsilon && Y <= Double.Epsilon ?
					"Укажите компоненты вектора" : String.Empty;
			}

			return base.ValidateProperty(propertyName);
		}

		public Vector ToVector() =>
			new Vector(X, Y);

		private double _x;
		private double _y;
	}
}
