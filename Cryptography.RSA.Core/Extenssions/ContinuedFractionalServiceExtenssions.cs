﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cryptography.RSA.Core.Abstract;
using Cryptography.RSA.Core.Attacks;
using Cryptography.Utills.Extenssions;

namespace Cryptography.RSA.Core.Extenssions
{
	public static class ContinuedFractionalServiceExtenssions
	{
		public static ICollection<T> GetContinuedRepresentation<T>(
			this IContinuedFractionalService<T> service, T numerator, T denumerator) where T: IEquatable<T>
		{
			service.NotNull(nameof(service));

			var fraction = new Fraction<T>
			{
				Numerator = numerator,
				Denumerator = denumerator
			};

			return service.GetContinuedRepresentation(fraction);
		}
	}
}
