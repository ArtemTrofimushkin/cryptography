﻿using System;
using System.IO;
using System.Threading;

using Cryptography.Security.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.RSA.Core
{
	using Math = System.Math;

	internal sealed class Decoder : EncoderBase, IDecoder
	{
		public byte[] DecodeKey { get; }

		public Decoder(byte[] decodeKey)
		{
			if (decodeKey.Length != 2 * sizeof(UInt64))
			{
				throw new ArgumentException(
					$"{nameof(decodeKey)} size must be equal to 2 * sizeof({nameof(UInt64)}). ");
			}

			_modulle = BitConverter.ToUInt64(decodeKey, 0);
			_exponent = BitConverter.ToUInt64(decodeKey, 8);
			_length = this.GetModuleLength(_modulle);

			DecodeKey = decodeKey;
		}
		public MemoryStream DecodeStream(Stream source, CancellationToken token)
		{
			source.NotNull(nameof(source));

			if (!source.CanRead)
			{
				throw new InvalidOperationException(
					$"{nameof(source)} must be readable");
			}

			var capacity = source.CanSeek ?
				(int)source.Length : DefaultCapacity;

			return this.DecodeStreamCore(source, token, new MemoryStream(capacity));
		}

		private MemoryStream DecodeStreamCore(Stream source, CancellationToken token, MemoryStream memory)
		{
			int readed = 0,
				iteration = 0,
				readChunkLength = _length + 2;

			var chunk = new byte[readChunkLength];

			while ((readed = source.Read(chunk, 0, readChunkLength) ) != 0)
			{
				if (( ++iteration % DefaultCheckIteration ) == 0)
				{
					token.ThrowIfCancellationRequested();
				}

				var encodedChunkLength = Math.Min(chunk.Length, chunk[_length]);
				var readedChunkLength = Math.Min(chunk.Length, chunk[_length + 1]);

				var number = this.EncodeNumber(
					this.ConvertToNumber(chunk, encodedChunkLength));

				var count = this.FillBufferFromNumber(chunk, readed, number);

				memory.Write(chunk, 0, readedChunkLength);
			}

			return memory;
		}
	}
}
