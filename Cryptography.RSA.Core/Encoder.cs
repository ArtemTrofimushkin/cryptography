﻿using System;
using System.Linq;
using System.IO;
using System.Threading;

using Cryptography.Math.Helpers;
using Cryptography.Utills.Extenssions;
using Cryptography.Security.Abstract;

namespace Cryptography.RSA.Core
{
	using Math = System.Math;

	internal sealed class Encoder : EncoderBase, IEncoder
	{
		public byte[] EncodeKey { get; }

		public Encoder(ulong p, ulong q, bool strong = true)
		{
			EncodeKey = this.GenerateKey(p, q, strong);
		}

		public MemoryStream EncodeStream(Stream source, CancellationToken token)
		{
			source.NotNull(nameof(source));

			if (!source.CanRead)
			{
				throw new InvalidOperationException(
					$"{nameof(source)} must be readable");
			}

			var capacity = source.CanSeek ? 
				(int)source.Length : DefaultCapacity;

			return this.EncodeStreamCore(source, token, new MemoryStream(capacity));
		}
		private MemoryStream EncodeStreamCore(Stream source, CancellationToken token, MemoryStream memory)
		{
			int readed = 0,
				iteration = 0,
				readChunkLength = _length - 1,
				writeChunkLength = _length + 2;

			var chunk = new byte[writeChunkLength];

			while (( readed = source.Read(chunk, 0, readChunkLength) ) != 0)
			{
				if (( ++iteration % DefaultCheckIteration ) == 0)
				{
					token.ThrowIfCancellationRequested();
				}

				var number = this.EncodeNumber(
					this.ConvertToNumber(chunk, Math.Min(readed, readChunkLength)));

				var count = this.FillBufferFromNumber(chunk, readed, number);

				chunk[_length] = (byte)count;
				chunk[_length + 1] = (byte)readed;

				memory.Write(chunk, 0, writeChunkLength);
			}

			return memory;
		}

		private byte[] GenerateKey(ulong p, ulong q, bool strong)
		{
			_modulle = p * q;
			_length = this.GetModuleLength(_modulle);

			ulong euller = ( p - 1UL ) * ( q - 1UL ),
				decryptExp = 0UL,
				temp = 1UL;

			var random = new Random();
			var buffer = new byte[_length];

			bool relative, generated = false;
			while (!generated)
			{
				relative = false;
				while (!relative)
				{
					random.NextBytes(buffer);
					_exponent = this.ConvertToNumber(buffer, _length);

					relative = (_exponent < _modulle) &&
						AlgebraicHelper.EuclideAlgorithm(_exponent, euller) == 1UL;
				}

				AlgebraicHelper.ExtendedEuclideAlgorithm(_exponent, euller, ref decryptExp, ref temp);

				generated = decryptExp != 1UL &&
					_exponent.ModuleMultiply(decryptExp, euller) == 1UL && 
					strong ^ decryptExp < AlgebraicHelper.Sqrt(AlgebraicHelper.Sqrt(_modulle));
				
				// Comment:
				// If Strong Key and Decrypt Exp is small => false
				// If Strong Key and Decrypt Exp is large => true
				// If Not Strong Key and Decrypt Exp is small => true
				// If Not Strong Key and Decrypt Exp is large => false
			}

			return BitConverter.GetBytes(_exponent)
				.Concat(BitConverter.GetBytes(_modulle))
				.Concat(BitConverter.GetBytes(decryptExp))
				.ToArray();
		}
	}
}
