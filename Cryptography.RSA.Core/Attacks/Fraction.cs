﻿using System;

namespace Cryptography.RSA.Core.Attacks
{
	public struct Fraction<T> : IEquatable<Fraction<T>> 
		where T: IEquatable<T>
	{
		public T Numerator { get; set; }
		public T Denumerator { get; set; }

		public Fraction(T numerator, T denumerator)
		{
			Numerator = numerator;
			Denumerator = denumerator;
		}

		public bool Equals(Fraction<T> other)
		{
			return Numerator.Equals(other.Numerator) &&
				Denumerator.Equals(other.Denumerator);
		}
		public override bool Equals(object obj) =>
			obj is Fraction<T> && this.Equals((Fraction<T>)obj);

		public override int GetHashCode()
		{
			return Numerator.GetHashCode() ^ 
				Denumerator.GetHashCode();
		}

		public override string ToString() =>
			$"{Numerator} / {Denumerator}";
	}
}
