﻿using System.Collections;
using System.Collections.Generic;
using Cryptography.RSA.Core.Abstract;

namespace Cryptography.RSA.Core.Attacks
{
	public class VinnerAttackResult : IAttackResult
	{
		public ulong SecretExponent { get; set; }
		public bool Success { get; set; }
		public Fraction<ulong> Fraction { get; set; }
		public ICollection<Fraction<ulong>> Representation { get; set; }
	}
}
