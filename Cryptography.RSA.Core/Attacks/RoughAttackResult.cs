﻿using Cryptography.RSA.Core.Abstract;

namespace Cryptography.RSA.Core.Attacks
{
	public class RoughAttackResult : IAttackResult
	{
		public ulong P { get; set; }
		public ulong Q { get; set; }
		public ulong SecretExponent { get; set; }
		public bool Success { get; set; }
	}
}
