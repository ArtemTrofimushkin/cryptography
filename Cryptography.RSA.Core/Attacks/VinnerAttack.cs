﻿using System.Threading;

using Cryptography.Math.Helpers;
using Cryptography.RSA.Core.Abstract;
using Cryptography.RSA.Core.Extenssions;
using Cryptography.Utills.Extenssions;

namespace Cryptography.RSA.Core.Attacks
{
	public class VinnerAttack : IRSAProviderAttack
	{
		public VinnerAttack(IContinuedFractionalService service)
		{
			service.AssignTo(out _service, nameof(service));
		}

		public IAttackResult Attack(IRSAEncoder encoder, CancellationToken token)
		{
			encoder.NotNull(nameof(encoder));

			var convergent = _service.GetConvergentFromContinuedRepresentation(
				_service.GetContinuedRepresentation(encoder.Exponent, encoder.Module));

			foreach(var fraction in convergent)
			{
				token.ThrowIfCancellationRequested();

				var result = this.CheckSecretKey(fraction.Numerator, fraction.Denumerator, encoder.Exponent, encoder.Module);

				if (result > 0)
				{
					return new VinnerAttackResult
					{
						SecretExponent = result,
						Success = true,
						Fraction = fraction,
						Representation = convergent
					};
				}
			}

			return new VinnerAttackResult
			{
				Success = false
			};
		}

		private ulong CheckSecretKey(ulong numerator, ulong denumerator, ulong exponent, ulong module)
		{
			if (numerator != 0 && (exponent * denumerator - 1UL) % numerator == 0)
			{
				var phi = ( exponent * denumerator - 1UL ) / numerator;
				var s = module - phi + 1UL;
				var discriminant = s * s - 4UL * module;

				if (discriminant >= 0)
				{
					var sqrt = AlgebraicHelper.PerfectSquare(discriminant);

					if (sqrt != 0 && 
						(s + sqrt) % 2 == 0UL)
					{
						return denumerator;
					}
				}
			}

			return 0UL;
		}

		private readonly IContinuedFractionalService _service;
	}
}
