﻿using System.Linq;
using System.Threading;

using Cryptography.Math.Abstract;
using Cryptography.Math.Helpers;
using Cryptography.RSA.Core.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.RSA.Core.Attacks
{
	public class RoughAttack : IRSAProviderAttack
	{
		public RoughAttack(IFactorizeService factorize)
		{
			factorize.AssignTo(out _factorize, nameof(factorize));
		}

		public IAttackResult Attack(IRSAEncoder encoder, CancellationToken token)
		{
			encoder.NotNull(nameof(encoder));

			var module = encoder.Module;
			var results = _factorize.Factorize(module, token);

			if (results.Count != 2 || 
				results.Any(pair => pair.Value != 1))
			{
				return new RoughAttackResult
				{
					Success = false
				};
			}

			ulong p = results.ElementAt(0).Key,
				q = results.ElementAt(1).Key,
				euller = ( p - 1UL ) * ( q - 1UL ), reminder = 1UL, decrypt = 0UL;

			AlgebraicHelper.ExtendedEuclideAlgorithm(encoder.Exponent, euller, ref decrypt, ref reminder);

			return new RoughAttackResult
			{
				P = p,
				Q = q,
				SecretExponent = decrypt,
				Success = encoder.Exponent.ModuleMultiply(decrypt, euller) == 1UL
			};
		}

		private readonly IFactorizeService _factorize;
	}
}
