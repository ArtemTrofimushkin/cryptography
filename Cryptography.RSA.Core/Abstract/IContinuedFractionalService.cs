﻿using System;
using System.Collections.Generic;

using Cryptography.RSA.Core.Attacks;

namespace Cryptography.RSA.Core.Abstract
{
	public interface IContinuedFractionalService<T> 
		where T: IEquatable<T>
	{
		Fraction<T> GetFractionRepresentation(IEnumerable<T> continued);
		ICollection<T> GetContinuedRepresentation(Fraction<T> fraction);
		ICollection<Fraction<T>> GetConvergentFromContinuedRepresentation(IEnumerable<T> continued);
	}

	public interface IContinuedFractionalService : IContinuedFractionalService<ulong>
	{
	}
}
