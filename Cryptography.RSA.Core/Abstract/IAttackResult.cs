﻿namespace Cryptography.RSA.Core.Abstract
{
	public interface IAttackResult
	{
		bool Success { get; }
		ulong SecretExponent { get; }
	}
}
