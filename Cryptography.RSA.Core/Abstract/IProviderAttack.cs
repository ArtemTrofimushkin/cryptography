﻿using System.Threading;
using Cryptography.RSA.Core.Attacks;

namespace Cryptography.RSA.Core.Abstract
{
	public interface IRSAProviderAttack
	{
		IAttackResult Attack(IRSAEncoder encoder, CancellationToken token);
	}
}
