﻿using Cryptography.Security.Abstract;

namespace Cryptography.RSA.Core.Abstract
{
	public interface IRSAWeakAlgorithmProvider : IAlgorithmProvider
	{
		IEncoder CreateWeakEncoder(string passPhrase);
	}
}
