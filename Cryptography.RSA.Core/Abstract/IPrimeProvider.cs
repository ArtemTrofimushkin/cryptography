﻿using System.Collections.Generic;

namespace Cryptography.RSA.Core.Abstract
{
	public interface IPrimeProvider
	{
		IReadOnlyList<ulong> GetPrimeNumbers();
	}
}
