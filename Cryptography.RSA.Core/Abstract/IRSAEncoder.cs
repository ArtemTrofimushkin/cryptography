﻿namespace Cryptography.RSA.Core.Abstract
{
	public interface IRSAEncoder
	{
		ulong Module { get; }
		ulong Exponent { get; }
	}
}
