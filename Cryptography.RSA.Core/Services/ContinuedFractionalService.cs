﻿using System.Collections.Generic;
using System.Linq;

using Cryptography.RSA.Core.Abstract;
using Cryptography.RSA.Core.Attacks;
using Cryptography.Utills.Extenssions;

namespace Cryptography.RSA.Core.Services
{
	public class ContinuedFractionalService : IContinuedFractionalService
	{
		public ICollection<ulong> GetContinuedRepresentation(Fraction<ulong> fraction)
		{
			var list = new List<ulong>(
				this.GetContinuedRepresentationCore(fraction));

			return list;
		}
		public Fraction<ulong> GetFractionRepresentation(IEnumerable<ulong> continued)
		{
			continued.NotNull(nameof(continued));
			return this.GetFractionRepresentationCore(continued, continued.Count());
		}
		public ICollection<Fraction<ulong>> GetConvergentFromContinuedRepresentation(IEnumerable<ulong> continued)
		{
			continued.NotNull(nameof(continued));

			var collection = continued.ToArray();
			var result = new List<Fraction<ulong>>();
			for (int i = 0; i != collection.Length; ++i)
			{
				var source = continued.Take(i);

				result.Add(
					this.GetFractionRepresentationCore(source, i));
			}

			return result;
		}

		private IEnumerable<ulong> GetContinuedRepresentationCore(Fraction<ulong> fraction)
		{
			var divider = fraction.Numerator / fraction.Denumerator;
			var result = new[] { divider };

			if (divider * fraction.Denumerator == fraction.Numerator)
			{
				return result;
			}

			var frac = new Fraction<ulong>
			{
				Denumerator = fraction.Numerator - divider * fraction.Denumerator,
				Numerator = fraction.Denumerator
			};

			return result.Concat(
				this.GetContinuedRepresentationCore(frac));
		}
		private Fraction<ulong> GetFractionRepresentationCore(IEnumerable<ulong> continued, int length)
		{
			if (length == 0)
			{
				return new Fraction<ulong>(0UL, 1UL);
			}

			var first = continued.ElementAt(0);

			if (length == 1)
			{
				return new Fraction<ulong>(first, 1UL);
			}

			var reminder = continued.Skip(1);
			var frac = this.GetFractionRepresentationCore(reminder, length - 1);

			return new Fraction<ulong>(first * frac.Numerator + frac.Denumerator, frac.Numerator);
		}
	}
}
