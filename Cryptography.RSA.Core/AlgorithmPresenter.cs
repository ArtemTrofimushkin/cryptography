﻿using System.Collections.Generic;

using Cryptography.RSA.Core.Abstract;
using Cryptography.Security;
using Cryptography.Security.Abstract;
using Cryptography.Security.Servises;
using Cryptography.Utills.Extenssions;

namespace Cryptography.RSA.Core
{
	public sealed class AlgorithmPresenter : AlgorithmPresenterBase
	{
		public AlgorithmPresenter(IEnumerable<IPrimeProvider> primeProviders)
		{
			primeProviders.AssignTo(out _primeProviders, nameof(primeProviders));

			AlgorithmType = AlgorithmType.Assymetric | AlgorithmType.Block;
			Title = "Ассиметричный алгоритм шифрования RSA";
			ImageKey = $"RSASecurityAlgorithmImage";

			#region Description
			Description = @"RSA (аббревиатура от фамилий Rivest, Shamir и Adleman) — криптографический алгоритм с открытым ключом, основывающийся на вычислительной сложности задачи факторизации больших целых чисел.

Криптосистема RSA стала первой системой, пригодной и для шифрования, и для цифровой подписи. Алгоритм используется в большом числе криптографических приложений, включая PGP, S/MIME, TLS/SSL, IPSEC/IKE и других.";
			#endregion
		}

		protected override IAlgorithmProvider CreateProvider()
		{
			return _provider ??
				( _provider = new AlgorithmProvider(_primeProviders) );
		}

		private readonly IEnumerable<IPrimeProvider> _primeProviders;
	}
}
