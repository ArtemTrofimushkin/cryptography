﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

using Cryptography.Utills.Extenssions;

namespace Cryptography.RSA.Core.Configuration
{
	internal sealed class RSAConfigSection : ConfigurationSection
	{
		[ConfigurationProperty(nameof(GeneratedPrimesFolderPath), DefaultValue = "", IsRequired = false)]
		public string GeneratedPrimesFolderPath
		{
			get { return (string)base[nameof(GeneratedPrimesFolderPath)]; }
			set { base[nameof(GeneratedPrimesFolderPath)] = value; }
		}

		[ConfigurationProperty(nameof(GeneratedPrimesFiles), DefaultValue = "", IsRequired = false)]
		public string GeneratedPrimesFiles
		{
			get { return (string)base[nameof(GeneratedPrimesFiles)]; }
			set { base[nameof(GeneratedPrimesFiles)] = value; }
		}

		[ConfigurationProperty(nameof(UseWeakPrimes), DefaultValue = true, IsRequired = false)]
		public bool UseWeakPrimes
		{
			get { return (bool)base[nameof(UseWeakPrimes)]; }
			set { base[nameof(UseWeakPrimes)] = value; }
		}

		public IReadOnlyCollection<string> GetGeneratedPrimeFilePaths()
		{
			if (String.IsNullOrWhiteSpace(GeneratedPrimesFolderPath))
			{
				throw new InvalidOperationException(
					"Generated prime folder not presented.");
			}

			if (!Directory.Exists(GeneratedPrimesFolderPath))
			{
				throw new InvalidOperationException(
					"Generated prime folder is invalid.");
			}

			if (String.IsNullOrWhiteSpace(GeneratedPrimesFiles))
			{
				return Directory.EnumerateFiles(GeneratedPrimesFolderPath).ToArray();
			}

			return GeneratedPrimesFiles.Split(Separators, StringSplitOptions.RemoveEmptyEntries)
				.Select(name => Path.Combine(GeneratedPrimesFolderPath, name)).ToArray();
		}

		public static RSAConfigSection GetSection() =>
			ConfigurationExtenssions.GetSectionOrDefault(nameof(RSAConfigSection), () => new RSAConfigSection());

		public static RSAConfigSection GetExeSection() =>
			ConfigurationExtenssions.GetExeSectionOrDefault(nameof(RSAConfigSection), () => new RSAConfigSection());

		private static readonly char[] Separators = { ';', ',' };
	}
}
