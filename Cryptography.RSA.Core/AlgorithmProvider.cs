﻿using System;
using System.Collections.Generic;
using System.Linq;

using Cryptography.Utills.Extenssions;
using Cryptography.RSA.Core.Abstract;
using Cryptography.Security.Abstract;

namespace Cryptography.RSA.Core
{
	using Math = System.Math;

	public sealed class AlgorithmProvider : 
		IAlgorithmProvider, 
		IRSAWeakAlgorithmProvider
	{
		public AlgorithmProvider(IEnumerable<IPrimeProvider> providers)
		{
			providers.NotNull(nameof(providers));

			var primes = providers.SelectMany(p => p.GetPrimeNumbers())
				.Distinct()
				.ToArray();

			if (primes.Count() < 2)
			{
				throw new ArgumentException(
					"Providers returned empty or less than two prime numbers. ");
			}

			_primes = primes;
		}

		public IDecoder CreateDecoder(byte[] key)
		{
			key.NotNull(nameof(key));

			if (key.Length != 3 * sizeof(ulong))
			{
				var message = $"{nameof(key)} length must be equal to 3 * sizeof({nameof(UInt64)}). {Environment.NewLine}" +
					$"Actual {nameof(key)} size is: {key.Length}. ";

				throw new ArgumentException(message, nameof(key));
			}

			return new Decoder(
				key.Skip(sizeof(ulong)).ToArray());
		}
		public IEncoder CreateEncoder(string passPhrase) =>
			this.CreateEncoderCore(passPhrase, true);
		public IEncoder CreateWeakEncoder(string passPhrase) =>
			this.CreateEncoderCore(passPhrase, false);

		private IEncoder CreateEncoderCore(string passPhrase, bool strong)
		{
			passPhrase.NotNull(nameof(passPhrase));

			var random = new Random(Guid.NewGuid().GetHashCode());
			var hash = Math.Abs(passPhrase.GetHashCode());
			var p = this.GenerateNextPrimeNumber(random, hash);

			return new Encoder(
				p, this.GenerateNextPrimeNumber(random, hash, p), strong);
		}
		private ulong GenerateNextPrimeNumber(Random random, int hash, ulong p = 0UL)
		{
			var q = 0UL;
			while(q == 0UL || q == p)
			{
				q = _primes.ElementAt(( random.Next(int.MaxValue - hash) + hash ) % _primes.Count);
			}
			return q;
		}

		private readonly IReadOnlyCollection<ulong> _primes;
	}
}
