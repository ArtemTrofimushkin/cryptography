﻿using System;

using Cryptography.RSA.Core.Abstract;
using Cryptography.Math.Helpers;

namespace Cryptography.RSA.Core
{
	internal class EncoderBase : IRSAEncoder
	{
		public ulong Module => _modulle;
		public ulong Exponent => _exponent;

		protected ulong EncodeNumber(ulong number)
		{
			if (number == 0UL)
			{
				return 0UL;
			}

			return AlgebraicHelper.ModulePower(number, _exponent, _modulle);
		}
		protected ulong ConvertToNumber(byte[] buffer, int count)
		{
			ulong number = 0UL;

			for(int i = 0; i != count; ++i)
			{
				number |= ((ulong)buffer[i]) << ( i * 8 );
			}

			return number;
		}
		protected int FillBufferFromNumber(byte[] buffer, int count, ulong number)
		{
			Array.Clear(buffer, 0, buffer.Length);

			int index = 0;

			while (number > 0)
			{
				buffer[index++] = (byte)( number & 0xFF );
				number >>= 8;
			}

			return index == 0 ? count : index;
		}
		protected int GetModuleLength(ulong module)
		{
			var length = 0;

			while(module > 0)
			{
				length++;
				module >>= 8;
			}

			return length;
		}

		protected ulong _modulle;
		protected ulong _exponent;
		protected int _length;
		protected const int DefaultCapacity = 128 * 1024;
		protected const int DefaultCheckIteration = 100;
	}
}
