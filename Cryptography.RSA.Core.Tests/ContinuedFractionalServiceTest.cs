﻿using System;
using Cryptography.RSA.Core.Abstract;
using Cryptography.RSA.Core.Attacks;
using Cryptography.RSA.Core.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.RSA.Core.Tests
{
	[TestClass]
	public class ContinuedFractionalServiceTest
	{
		[TestInitialize]
		public void Initialize()
		{
			_service = new ContinuedFractionalService();
		}

		[TestMethod]
		public void FractionsContinuedRepresentation()
		{
			var fractions = new[]
			{
				new Fraction<ulong>{ Numerator = 1UL, Denumerator = 1UL },
				new Fraction<ulong>{ Numerator = 1UL, Denumerator = 2UL },
				new Fraction<ulong>{ Numerator = 5UL, Denumerator = 15UL },
				new Fraction<ulong>{ Numerator = 27UL, Denumerator = 73UL },
				new Fraction<ulong>{ Numerator = 73UL, Denumerator = 27UL }
			};

			foreach(var fraction in fractions)
			{
				var list = _service.GetContinuedRepresentation(fraction);
				var frac = _service.GetFractionRepresentation(list);

				Assert.IsTrue(
					fraction.Numerator % frac.Numerator == 0 &&
					fraction.Denumerator % frac.Denumerator == 0);
			}
		}

		[TestMethod]
		public void FractionsConvergent()
		{
			var fractions = new Fraction<ulong>[]
			{
				new Fraction<ulong>{ Numerator = 1UL, Denumerator = 1UL },
				new Fraction<ulong>{ Numerator = 1UL, Denumerator = 2UL },
				new Fraction<ulong>{ Numerator = 5UL, Denumerator = 15UL },
				new Fraction<ulong>{ Numerator = 27UL, Denumerator = 73UL },
				new Fraction<ulong>{ Numerator = 73UL, Denumerator = 27UL }
			};

			foreach (var fraction in fractions)
			{
				var list = _service.GetContinuedRepresentation(fraction);
				var convergent = _service.GetConvergentFromContinuedRepresentation(list);

				var str = String.Join(Environment.NewLine, convergent);

				Assert.IsNotNull(convergent);
			}
		}

		private IContinuedFractionalService<ulong> _service;
	}
}
