﻿using System;
using System.Linq;
using System.Threading;

using Cryptography.Math.Abstract;
using Cryptography.Math.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.RSA.Core.Tests
{
	using Math = System.Math;

	[TestClass]
	public class FactorizationTest
	{
		[TestInitialize]
		public void Initialization()
		{
			_service = new FactorizeService();
		}

		[TestMethod]
		public void FactorizePrimeNumber()
		{
			var primes = new ulong[]
			{
				102781873, 123167789, 153699743, 182392117, 213517531, 249962389,
				250014097, 339002861, 339027419, 448088087, 448077319, 485224171,
				297095983, 279985469, 313542419, 404820617, 452292109, 490149889,
				550846441, 500037229, 580490047, 591526549, 550844669, 617111881,
				613650599, 613667771, 694867111, 725537873, 725547517, 749995199,
				750122951, 776868997, 783059227, 836865661, 893616371, 882547301,
				915820397, 915831529, 930390509, 951955399, 956120899, 999965179
			};

			var results = primes.Select(p => _service.Factorize(p, CancellationToken.None))
				.Select(map => map.Count == 1)
				.ToArray();

			Assert.AreEqual(results.Length, primes.Length);
		}

		[TestMethod]
		public void FactorizeNonPrimeNumber()
		{
			var number = 1283746517631UL;
			var map = _service.Factorize(number, CancellationToken.None);

			Assert.AreEqual(map.Count, 3);
			Assert.AreEqual(number, map.Aggregate(1UL, (acc, pair) => acc *= (ulong)Math.Pow(pair.Key, pair.Value)));
		}

		[TestMethod]
		public void FactorizeNumberPerfomance()
		{
			const int count = 10000;

			var random = new Random(Guid.NewGuid().GetHashCode());
			for(int i = 0; i != count; ++i)
			{
				var number = (ulong)random.Next();
				_service.Factorize(number, CancellationToken.None);
			}
		}

		private IFactorizeService<ulong> _service;
	}
}
