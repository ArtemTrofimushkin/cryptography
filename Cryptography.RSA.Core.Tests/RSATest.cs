﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Cryptography.Security.Abstract;
using Cryptography.Security.Extenssions;

namespace Cryptography.RSA.Core.Tests
{
	[TestClass]
	public class RSATest
	{
		public TestContext TestContext { get; set; }

		[TestInitialize]
		public void RSATestInit()
		{
			_algorithm = new AlgorithmProvider(new[] { new PrimeProvider() });
		}

		[TestMethod]
		public void RSAEncryption()
		{
			this.EncodeDecode(
				this.GenerateSomeData());
		}

		[TestMethod]
		public void RSAPerfomanceEncryption()
		{
			var list = new List<AssertFailedException>();
			const int count = 20;

			for(int i = 0; i != count; ++i)
			{
				try
				{
					this.EncodeDecode(
						this.GenerateSomeData(1, 20000));
				}
				catch(AssertFailedException exception)
				{
					TestContext.WriteLine(exception.Message);
					list.Add(exception);
				}
			}

			if (list.Any())
			{
				var message = $"Total Count: {list.Count}{Environment.NewLine}" +
					String.Join(Environment.NewLine, list.Select(ex => ex.Message));

				throw new AssertFailedException(message);
			}
		}

		private void EncodeDecode(byte[] source)
		{
			var encoder = _algorithm.CreateEncoder("Password");
			var decoder = _algorithm.CreateDecoder(encoder.EncodeKey);

			var encrypted = encoder.EncodeBytes(source);
			var decrypted = decoder.DecodeBytes(encrypted);
			var i = 0;

			try
			{
				Assert.AreEqual(source.Length, decrypted.Length);

				for (; i != source.Length; ++i)
				{
					TestContext.WriteLine("{0} {1}", source[i], decrypted[i]);
					Assert.AreEqual(source[i], decrypted[i]);
				}
			}
			catch (AssertFailedException exception)
			{
				var message = $"Source length: {source.Length}. Encoded length: {encrypted.Length}. Decoded length: {decrypted.Length}. " +
					$"{Environment.NewLine}Source data: {string.Join(" ", source)}." +
					$"{Environment.NewLine}Encrypted data: {string.Join(" ", encrypted)}." +
					$"{Environment.NewLine}Decrypted data: {string.Join(" ", decrypted)}." +
					$"{Environment.NewLine}EncodeKey: {string.Join(" ", encoder.EncodeKey)}" +
					$"{Environment.NewLine}ErrorIndex: {i}.";

				throw new AssertFailedException(message, exception);
			}
		}
		private byte[] GenerateSomeData(int min = 1, int max = 20000)
		{
			var random = new Random();
			var length = random.Next(min, max);
			var buffer = new byte[length];

			random.NextBytes(buffer);

			return buffer;
		}

		private IAlgorithmProvider _algorithm;
	}
}
