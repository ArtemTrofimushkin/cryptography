﻿using System;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Cryptography.RSA.Core.Abstract;
using Cryptography.RSA.Core.Attacks;
using Cryptography.RSA.Core.Services;
using Cryptography.Math.Services;

namespace Cryptography.RSA.Core.Tests
{
	[TestClass]
	public class RSATestAttack
	{
		[TestInitialize]
		public void Initialize()
		{
			_algorithm = new AlgorithmProvider(new[] { new PrimeProvider() });
		}

		[TestMethod]
		public void VinnerAttack()
		{
			var encoder = _algorithm.CreateWeakEncoder("Password");
			var decoder = _algorithm.CreateDecoder(encoder.EncodeKey);

			var rsaEncoder = encoder as IRSAEncoder;
			var rsaDecoder = decoder as IRSAEncoder;

			var vinnerAttack = new VinnerAttack(new ContinuedFractionalService());

			var result = vinnerAttack.Attack(rsaEncoder, CancellationToken.None);

			if (result.Success)
			{
				Assert.AreEqual(result.SecretExponent, rsaDecoder.Exponent);
			}
			else
			{
				throw new AssertFailedException();
			}
		}

		[TestMethod]
		public void RoughAttack()
		{
			var encoder = _algorithm.CreateEncoder("Password");
			var decoder = _algorithm.CreateDecoder(encoder.EncodeKey);

			var rsaEncoder = encoder as IRSAEncoder;
			var rsaDecoder = decoder as IRSAEncoder;

			var roughAttack = new RoughAttack(new FactorizeService());

			var result = roughAttack.Attack(rsaEncoder, CancellationToken.None);

			if (result.Success)
			{
				Assert.AreEqual(result.SecretExponent, rsaDecoder.Exponent);
			} 
			else
			{
				throw new AssertFailedException();
			}
		}

		private IRSAWeakAlgorithmProvider _algorithm;
	}
}
