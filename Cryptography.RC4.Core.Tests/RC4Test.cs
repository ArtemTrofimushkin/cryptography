﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Cryptography.Security.Abstract;
using Cryptography.Security.Extenssions;

namespace Cryptography.RC4.Core.Tests
{
	[TestClass]
	public class RC4Test
	{
		public TestContext TestContext { get; set; }

		[TestInitialize]
		public void RC4TestInit()
		{
			_algorithm = new AlgorithmProvider();
		}

		[TestMethod]
		public void RC4Encryption()
		{
			this.EncodeDecode(
				this.GenerateSomeData(100000, 1000000));
		}

		[TestMethod]
		public void RC4PerfomanceEncryption()
		{
			var list = new List<AssertFailedException>();
			const int count = 1000;

			for (int i = 0; i != count; ++i)
			{
				try
				{
					this.EncodeDecode(
						this.GenerateSomeData(100000, 1000000));
				}
				catch (AssertFailedException exception)
				{
					TestContext.WriteLine(exception.Message);
					list.Add(exception);
				}
			}

			if (list.Any())
			{
				var message = String.Join(Environment.NewLine, 
					list.Select(ex => ex.Message));

				message += $"{Environment.NewLine}Total Count: {list.Count}";

				throw new AssertFailedException(message);
			}
		}

		private void EncodeDecode(byte[] source)
		{
			var encoder = _algorithm.CreateEncoder("Password");
			var decoder = _algorithm.CreateDecoder(encoder.EncodeKey);

			var encrypted = encoder.EncodeBytes(source);
			var decripted = decoder.DecodeBytes(encrypted);

			Assert.AreEqual(source.Length, decripted.Length);

			for(int i = 0; i != source.Length; ++i)
			{
				Assert.AreEqual(source[i], decripted[i]);
			}
		}

		private byte[] GenerateSomeData(int min = 1, int max = 20000)
		{
			var random = new Random();
			var length = random.Next(min, max);
			var buffer = new byte[length];

			random.NextBytes(buffer);

			return buffer;
		}

		private IAlgorithmProvider _algorithm;
	}
}
