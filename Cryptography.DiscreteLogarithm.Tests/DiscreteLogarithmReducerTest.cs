﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Cryptography.Math;
using Cryptography.Math.Helpers;
using Cryptography.Math.Services;
using Cryptography.DiscreteLogarithm.Core.Services;
using Cryptography.DiscreteLogarithm.Core.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.DiscreteLogarithm.Tests
{
	[TestClass]
	public class DiscreteLogarithmReducerTest
	{
		[TestMethod]
		public void ShanksDiscreteLogarithmReduser()
		{
			var reduser = new ShanksReducer();

			this.AssertReduser(reduser);
		}

		[TestMethod]
		public void ChineseReminderTest()
		{
			var pairs = new Dictionary<ulong, ulong>()
			{
				[3] = 2,
				[5] = 3,
				[7] = 2,
				[8] = 3,
				[11] = 1,
				[15] = 12
			};
			var service = new ChineseReminderService();
			var first = service.FindReminder(pairs.Take(3).ToDictionary(x => x.Key, x => x.Value));
			var second = service.FindReminder(pairs.Skip(3).ToDictionary(x => x.Key, x => x.Value));

			Assert.AreEqual(first, 23UL);
			Assert.AreEqual(second, 507UL);
		}

		[TestMethod]
		public void PohligHellmanDiscreteLogarithmReduser()
		{
			var reduser = new PohligHellmanReducer(
				new FactorizeService(),
				new ChineseReminderService());

			this.AssertReduser(reduser);
		}

		[TestMethod]
		public void PollardRhoReducer()
		{
			var reducer = new PollardRhoReducer(101UL);

			this.AssertReduser(reducer);
		}

		[TestMethod]
		public void PollardLambdaReducer()
		{
			var reducer = new PollardLambdaReducer(60, 80, 101);

			this.AssertReduser(reducer);
		}

		[TestMethod]
		public void GaloisFieldReducerTest()
		{
			var basis = new PolynomGF(new ulong[] { 1, 0, 1 }, 13);
			var module = new PolynomGF(new ulong[] { 1, 0, 1, 6, 2 }, 13);
			var result = GaloisFieldHelper.ModulePower(basis, 5UL, module);

			var reduser = new GaloisFieldReducer(13UL);
			var redusedResult = reduser.ReduceDicreteLogarithm(module, basis, result, CancellationToken.None);

			Assert.IsTrue(redusedResult.Successed);
			Assert.AreEqual(redusedResult.Exponent, 5UL);
			Assert.AreEqual(redusedResult.Check, result);
		}

		[TestMethod]
		public void GaloisFieldReducerTest2()
		{
			var basis = new PolynomGF(new ulong[] { 1, 1, 1 }, 2UL);
			var module = new PolynomGF(new ulong[] { 1, 1, 0, 1 }, 2UL);
			var result = GaloisFieldHelper.ModulePower(basis, 3UL, module);

			var reduser = new GaloisFieldReducer(2UL);
			var redusedResult = reduser.ReduceDicreteLogarithm(module, basis, result, CancellationToken.None);

			Assert.IsTrue(redusedResult.Successed);
			Assert.AreEqual(redusedResult.Exponent, 3UL);
			Assert.AreEqual(redusedResult.Check, result);
		}

		[TestMethod]
		public void GaloisFieldReducerTest3()
		{
			var basis = new PolynomGF(new ulong[] { 1, 0, 1 }, 13);
			var module = new PolynomGF(new ulong[] { 6, 1, 0, 1 }, 13);
			var result = basis.ModulePower(5, module);

			var reduser = new GaloisFieldReducer(13UL);
			var redusedResult = reduser.ReduceDicreteLogarithm(module, basis, result, CancellationToken.None);

			Assert.IsTrue(redusedResult.Successed);
			Assert.AreEqual(redusedResult.Exponent, 5UL);
			Assert.AreEqual(redusedResult.Check, result);
		}

		[TestMethod]
		public void IrreducibleGenerationTest()
		{
			var generator = new IrreducibleGenerator();
			var poly = generator.GenerateIrreduciblePolynom(13UL, 5UL, CancellationToken.None);

			Assert.IsNotNull(poly);
		}

		private void AssertReduser(INumberDiscreteLogarithmReducer reduser)
		{
			var module = 607UL;
			var basis = 64UL;
			var result = 122UL;

			var reducedResult = reduser.ReduceDicreteLogarithm(module, basis, result, CancellationToken.None);

			Assert.IsTrue(reducedResult.Successed);
			Assert.AreEqual(AlgebraicHelper.ModulePower(basis, reducedResult.Exponent, module), result);
		}
	}
}
