﻿using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.DiscreteLogarithm.Core.Services;
using Cryptography.Math;

namespace Cryptography.DiscreteLogarithm.Tests
{
	[TestClass]
	public class IrreducibleGeneratorTests
	{
		[TestInitialize]
		public void Cobfigure()
		{
			_generator = new IrreducibleGenerator();
		}

		[TestMethod]
		public void IncreaseTest_1()
		{
			var p1 = new PolynomGF(new[] { 0UL }, 13);
			var p2 = new PolynomGF(new[] { 11UL, 3UL }, 13);

			for (int i = 0; i < 50; ++i)
			{
				p1 = p1.Increment();
			}

			Assert.IsTrue(p1.Equals(p2));
		}

		[TestMethod]
		public void IncreaseTest_2()
		{
			var p1 = new PolynomGF(new[] { 1UL, 0UL, 2UL }, 3);
			var p2 = new PolynomGF(new[] { 0UL, 0UL, 0UL, 1UL }, 3);

			for (int i = 0; i < 8; ++i)
			{
				p1 = p1.Increment();
			}

			Assert.IsTrue(p1.Equals(p2));
		}

		[TestMethod]
		public void BuildGaloisField_1()
		{
			const ulong character = 13UL;
			const ulong degree = 5UL;

			var source = _generator.GenerateField(character, degree)
				.ToArray();

			var condition = source.Last()
				.All(el => el == character - 1);

			Assert.IsTrue(condition);
		}

		[TestMethod]
		public void BuildGaloisField_2()
		{
			const ulong character = 5UL;
			const ulong degree = 5UL;
			var source = _generator.GenerateField(character, degree);

			var condition = source
				.Last()
				.All(x => x == character - 1);

			Assert.IsTrue(condition);
		}
		
		[TestMethod]
		public void OptimusPrimeTest_1()
		{
			const ulong character = 7UL;
			const ulong degree = 5UL;

			var prime = new PolynomGF(new[] { 4UL, 1UL, 0UL, 0UL, 0UL, 1UL }, character);
			var list = _generator.GenerateIrreduciblePolynomials(character, degree, CancellationToken.None)
				.ToArray();

			Assert.IsTrue(list.Contains(prime));
		}

		[TestMethod]
		public void OptimusPrimeTest_2()
		{
			const ulong character = 7UL;
			const ulong degree = 5UL;

			var prime = new PolynomGF(new[] { 2UL, 1UL, 0UL, 0UL, 0UL, 1UL }, character);
			var list = _generator.GenerateIrreduciblePolynomials(character, degree, CancellationToken.None)
				.ToArray();

			Assert.IsFalse(list.Contains(prime));
		}

		private IIrreducibleGenerator _generator;
	}
}
