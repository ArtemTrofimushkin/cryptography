﻿using System;
using System.Collections.Generic;
using Cryptography.Host.UI.Abstract;

namespace Cryptography.Security.Resources
{
	internal sealed class ResourceProvider : IResourceProvider
	{
		public IReadOnlyCollection<Uri> GetResourceDictionaries()
		{
			var assemblyName = this.GetType().Assembly.GetName().Name;
			var baseLocation = $"pack://application:,,,/{assemblyName};component/Resources/";

			return new Uri[]
			{
				new Uri($"{baseLocation}DataTemplates.xaml"),
				new Uri($"{baseLocation}GenericResources.xaml"),
				new Uri($"{baseLocation}Converters.xaml"),
				new Uri($"{baseLocation}Styles.xaml")
			};
		}
	}
}
