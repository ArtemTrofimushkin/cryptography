﻿namespace Cryptography.Security.Abstract
{
	public interface IPasswordStorage : 
		IKeyedStorage<string, byte[]>
	{
	}
}
