﻿using System.Threading;
using System.Threading.Tasks;

namespace Cryptography.Security.Abstract
{
	public interface IEncryptionService
	{
		bool CanExecute(object parameter);
		Task DecryptAync(IDecoder decoder, CancellationToken token);
		Task EncryptAync(IEncoder encoder, CancellationToken token);
	}
}