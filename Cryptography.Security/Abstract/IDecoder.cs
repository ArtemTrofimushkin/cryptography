﻿using System.IO;
using System.Threading;

namespace Cryptography.Security.Abstract
{
	public interface IDecoder
	{
		byte[] DecodeKey { get; }

		MemoryStream DecodeStream(Stream source, CancellationToken token);
	}
}
