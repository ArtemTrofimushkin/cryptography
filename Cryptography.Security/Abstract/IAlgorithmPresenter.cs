﻿namespace Cryptography.Security.Abstract
{
	public interface IAlgorithmPresenter
	{
		string Title { get; }
		string Description { get; }
		string ProviderKey { get; }
		string ImageKey { get; }
		AlgorithmType AlgorithmType { get; }

		IAlgorithmProvider CreateProvider();
	}
}
