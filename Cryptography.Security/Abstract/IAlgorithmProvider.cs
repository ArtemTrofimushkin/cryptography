﻿namespace Cryptography.Security.Abstract
{
	public interface IAlgorithmProvider
    {
		IEncoder CreateEncoder(string passPhrase);

		IDecoder CreateDecoder(byte[] key);
	}
}