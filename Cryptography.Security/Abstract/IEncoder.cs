﻿using System.IO;
using System.Threading;

namespace Cryptography.Security.Abstract
{
	public interface IEncoder
	{
		byte[] EncodeKey { get; }

		MemoryStream EncodeStream(Stream source, CancellationToken token);
	}
}
