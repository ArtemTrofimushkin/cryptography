﻿namespace Cryptography.Security.Abstract
{
	public interface IKeyedStorage<TKey, TValue>
	{
		TValue Get(TKey key);
		void Create(TKey key, TValue value);
		void Update(TKey key, TValue value);
		void Remove(TKey key);
	}
}
