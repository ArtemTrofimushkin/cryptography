﻿using System.ComponentModel;

namespace Cryptography.Security
{
	public enum AlgorithmType
	{
		[Description("Ассиметричный")]
		Assymetric = 0x01,

		[Description("Симметричный")]
		Symmetric = 0x02,

		[Description("Блочный")]
		Block = 0x10,

		[Description("Поточный")]
		Stream = 0x20
	}
}
