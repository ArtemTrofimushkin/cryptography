﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

using Cryptography.Host.Api.Abstract;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Security.Abstract;
using Cryptography.Security.Extenssions;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Security.ViewModels.Encrypting
{
	public sealed class LocalFileTargetViewModel : ViewModelBase, IEncryptionService
	{
		public ICommand OpenFile { get; }
		public ICommand SaveFile { get; }

		public string InputFilename
		{
			get { return _inputFilename; }
			set { this.UpdateProperty(value, ref _inputFilename); }
		}
		public string OutputFilename
		{
			get { return _outputFilename; }
			set { this.UpdateProperty(value, ref _outputFilename); }
		}

		public LocalFileTargetViewModel(
			ICommandFactory factory,
			IUserInterfaceService userInterface)
		{
			userInterface.AssignTo(out _userInterface, nameof(userInterface));

			OpenFile = factory.CreateCommand(this.OpenFileExecute);
			SaveFile = factory.CreateCommand(this.SaveFileExecute);
		}
		public bool CanExecute(object parameter) => IsValid;
		public Task EncryptAync(IEncoder encoder, CancellationToken token)
		{
			if (!File.Exists(InputFilename))
			{
				throw new FileNotFoundException("File not found", InputFilename);
			}

			return encoder.EncryptFileAsync(token, InputFilename, OutputFilename);
		}
		public Task DecryptAync(IDecoder decoder, CancellationToken token)
		{
			if (!File.Exists(InputFilename))
			{
				throw new FileNotFoundException("File not found", InputFilename);
			}

			return decoder.DecryptFileAsync(token, InputFilename, OutputFilename);
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals(nameof(InputFilename), propertyName, StringComparison.Ordinal))
			{
				if (String.IsNullOrWhiteSpace(InputFilename))
				{
					return "Укажите входной файл";
				} 
				else if (!File.Exists(InputFilename))
				{
					return "Укажите абсолютный путь к существующему файлу";
				}
			}
			if (String.Equals(nameof(OutputFilename), propertyName, StringComparison.Ordinal))
			{
				if (String.IsNullOrWhiteSpace(OutputFilename))
				{
					return "Укажите выходной файл";
				}

				var path = Path.GetFullPath(OutputFilename);
				var name = Path.GetFileNameWithoutExtension(OutputFilename);

				if (Path.GetInvalidPathChars().Any(c => path.Contains(c)) &&
					Path.GetInvalidFileNameChars().Any(c => name.Contains(c)))
				{
					return "Неверный путь или имя файла";
				}
			}

			return base.ValidateProperty(propertyName);
		}

		private void OpenFileExecute()
		{
			var filename = _userInterface.OpenFileDialog();

			if (!String.IsNullOrWhiteSpace(filename))
			{
				InputFilename = filename;
			}
		}
		private void SaveFileExecute()
		{
			var filename = _userInterface.SaveFileDialog();

			if (!String.IsNullOrWhiteSpace(filename))
			{
				OutputFilename = filename;
			}
		}

		private string _inputFilename;
		private string _outputFilename;
		private readonly IUserInterfaceService _userInterface;
	}
}
