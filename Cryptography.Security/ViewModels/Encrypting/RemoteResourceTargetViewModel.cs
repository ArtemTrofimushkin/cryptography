﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

using Cryptography.Host.Api.Abstract;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Security.Abstract;
using Cryptography.Security.Extenssions;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Security.ViewModels.Encrypting
{
	public sealed class RemoteResourceTargetViewModel : ViewModelBase, IEncryptionService
	{
		public ICommand SaveFile { get; }

		public string Filename
		{
			get { return _filename; ; }
			set { this.UpdateProperty(value, ref _filename); }
		}
		public string RemoteUrl
		{
			get { return _remoteUrl; }
			set { this.UpdateProperty(value, ref _remoteUrl); }
		}

		public RemoteResourceTargetViewModel(
			ICommandFactory factory,
			IUserInterfaceService userInterface)
		{
			userInterface.AssignTo(out _userInterface, nameof(userInterface));

			SaveFile = factory.CreateCommand(this.OpenFileExecute);
		}

		public bool CanExecute(object parameter)
		{
			var actionName = parameter as string;

			if (String.Equals(actionName, "Encrypt", StringComparison.Ordinal))
			{
				return IsValid;
			}

			return false;
		}
		public Task DecryptAync(IDecoder decoder, CancellationToken token)
		{
			throw new NotSupportedException();
		}
		public async Task EncryptAync(IEncoder encoder, CancellationToken token)
		{
			using (var client = new HttpClient(new HttpClientHandler(), true))
			{
				var message = await client.GetAsync(RemoteUrl, token)
					.ConfigureAwait(false);

				using (message)
				{
					message.EnsureSuccessStatusCode();

					var source = await message.Content
						.ReadAsStreamAsync()
						.ConfigureAwait(false);

					using (source)
					{
						await encoder.EncodeStreamToFileAsync(token, source, Filename)
							.ConfigureAwait(false);
					}
				}
			}
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals(nameof(Filename), propertyName, StringComparison.Ordinal))
			{
				return this.ValidateFilename();
			}
			if (String.Equals(nameof(RemoteUrl), propertyName, StringComparison.Ordinal))
			{
				return this.ValidateRemoteUrl();
			}

			return base.ValidateProperty(propertyName);
		}

		private void OpenFileExecute(object parameter)
		{
			var filename = _userInterface.SaveFileDialog();

			if (!String.IsNullOrWhiteSpace(filename))
			{
				Filename = filename;
			}
		}
		private string ValidateFilename()
		{
			if (String.IsNullOrWhiteSpace(Filename))
			{
				return "Укажите входной файл";
			}

			return String.Empty;
		}
		private string ValidateRemoteUrl()
		{
			if (String.IsNullOrWhiteSpace(RemoteUrl))
			{
				return "Укажите удаленный ресурс";
			}

			Uri uri;
			if (!Uri.TryCreate(RemoteUrl, UriKind.Absolute, out uri))
			{
				return "Неверно задано имя удаленного ресурса.";
			}

			if (uri.Scheme != Uri.UriSchemeHttp &&
				uri.Scheme != Uri.UriSchemeHttps)
			{
				return "Удаленный ресурс должен быть доступен по протоколу http или https.";
			}

			return String.Empty;
		}
		
		private string _filename;
		private string _remoteUrl;
		private readonly IUserInterfaceService _userInterface;
	}
}
