﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Cryptography.Host.Api.ViewModels;
using Cryptography.Security.Abstract;
using Cryptography.Security.Extenssions;

namespace Cryptography.Security.ViewModels.Encrypting
{
	public sealed class PlainTextTargetViewModel : ViewModelBase, IEncryptionService
	{
		public string PlainText
		{
			get { return _input; }
			set { this.UpdateProperty(value, ref _input); }
		}
		public string SecretText
		{
			get { return _output; }
			set { this.UpdateProperty(value, ref _output); }
		}

		public bool CanExecute(object parameter)
		{
			var actionName = parameter as string;

			if (String.Equals(actionName, "Encrypt", StringComparison.Ordinal) && 
				!String.IsNullOrWhiteSpace(PlainText))
			{
				return true;
			}
			if (String.Equals(actionName, "Decrypt", StringComparison.Ordinal) &&
				!String.IsNullOrWhiteSpace(SecretText))
			{
				return true;
			}

			return false;
		}
		public async Task EncryptAync(IEncoder encoder, CancellationToken token)
		{
			var source = Encoding.UTF8
				.GetBytes(PlainText);

			var encoded = await encoder.EncodeBytesAsync(source, token);
			SecretText = String.Join(Separators[0], encoded);
		}
		public async Task DecryptAync(IDecoder decoder, CancellationToken token)
		{
			var source = SecretText.Split(Separators, StringSplitOptions.RemoveEmptyEntries)
				.Select(this.ConvertToByte)
				.ToArray();

			var decoded = await decoder.DecodeBytesAsync(source, token);

			PlainText = Encoding.UTF8
				.GetString(decoded);
		}

		private byte ConvertToByte(string source)
		{
			byte result;
			return Byte.TryParse(source, out result) ? result : default(byte);
		}

		private string _input;
		private string _output;
		private static readonly string[] Separators = new[] { "-" };
	}
}
