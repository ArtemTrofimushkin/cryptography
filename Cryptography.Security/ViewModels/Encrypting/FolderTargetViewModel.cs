﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

using Cryptography.Host.Api.Abstract;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Security.Abstract;
using Cryptography.Security.Extenssions;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Security.ViewModels.Encrypting
{
	public sealed class FolderTargetViewModel : ViewModelBase, IEncryptionService
	{
		public ICommand OpenFolder { get; }
		public string Folder
		{
			get { return _folder; }
			set
			{
				this.UpdateProperty(value, ref _folder);
				this.UpdateFileExtenssions();
			}
		}
		public ObservableCollection<CheckedDisplayNamePair> FileExtenssions { get; } =
			new ObservableCollection<CheckedDisplayNamePair>();

		public FolderTargetViewModel(
			ICommandFactory factory,
			IUserInterfaceService userInterface)
		{
			userInterface.AssignTo(out _userInterface, nameof(userInterface));
			OpenFolder = factory.CreateCommand(this.OpenFolderExecute);
		}

		public bool CanExecute(object parameter) => IsValid;
		public Task DecryptAync(IDecoder decoder, CancellationToken token)
		{
			var proccesedFolder = this.EnsureProcessedFolder("Decrypted");

			return this.ProcessDirectoryAsync(
				file => decoder.DecryptFileAsync(token, file, this.CreateProcessedFilename(file, proccesedFolder)));
		}
		public Task EncryptAync(IEncoder encoder, CancellationToken token)
		{
			var proccesedFolder = this.EnsureProcessedFolder("Encrypted");

			return this.ProcessDirectoryAsync(
				file => encoder.EncryptFileAsync(token, file, this.CreateProcessedFilename(file, proccesedFolder)));
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals(nameof(Folder), propertyName, StringComparison.Ordinal))
			{
				if (String.IsNullOrWhiteSpace(Folder))
				{
					return "Укажите папку";
				} 
				else if (!Directory.Exists(Folder))
				{
					return "Укажите абсолютный путь к папке";
				}
			}
			return base.ValidateProperty(propertyName);
		}

		private void OpenFolderExecute()
		{
			var folder = _userInterface.OpenFolderDialog();

			if (!String.IsNullOrWhiteSpace(folder))
			{
				Folder = folder;
			}
		}
		private void UpdateFileExtenssions()
		{
			if (!this.IsValid)
			{
				return;
			}

			var source = Directory.EnumerateFiles(Folder)
				.Select(filepath => $"*{Path.GetExtension(filepath)}")
				.Distinct()
				.Select(x => new CheckedDisplayNamePair(x));

			FileExtenssions.Clear();
			FileExtenssions.AddRange(source);
		}
		private string EnsureProcessedFolder(string suffix)
		{
			var foldername = $"{Folder}_{suffix}";

			if (Directory.Exists(foldername))
			{
				var selectedFolder = _userInterface.OpenFolderDialog();

				if (string.IsNullOrWhiteSpace(selectedFolder))
				{
					throw new InvalidOperationException(
						$"Path: {foldername} already exists, but empty foldername is specified.");
				}

				foldername = selectedFolder;
			}
			else
			{
				Directory.CreateDirectory(foldername);
			}

			return foldername;
		}
		private string CreateProcessedFilename(string filepath, string folder) =>
			Path.Combine(folder, Path.GetFileName(filepath));
		private Task ProcessDirectoryAsync(Func<string, Task> action)
		{
			var extenssions = FileExtenssions.Where(x => x.IsChecked)
				.Select(ext => ext.DisplayName.Substring(1))
				.ToSet();

			var tasks = Directory.EnumerateFiles(_folder)
				.Where(file => extenssions.Contains(Path.GetExtension(file)))
				.Select(file => action(file));

			return Task.WhenAll(tasks);
		}

		private string _folder;
		private readonly IUserInterfaceService _userInterface;
	}
}
