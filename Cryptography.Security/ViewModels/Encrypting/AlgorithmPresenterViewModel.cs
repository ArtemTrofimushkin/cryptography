﻿using Cryptography.Security.Abstract;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Security.ViewModels.Encrypting
{
	public class AlgorithmPresenterViewModel : ViewModelBase
	{
		public string Title => _presenter.Title;
		public string Description => _presenter.Description;
		public string ImageKey => _presenter.ImageKey;
		public string Key => _presenter.ProviderKey;
		
		public AlgorithmPresenterViewModel(IAlgorithmPresenter model)
		{
			model.AssignTo(out _presenter, nameof(model));
		}

		public IAlgorithmProvider GetAlgorithmProvider() =>
			_presenter.CreateProvider();

		private readonly IAlgorithmPresenter _presenter;
	}
}
