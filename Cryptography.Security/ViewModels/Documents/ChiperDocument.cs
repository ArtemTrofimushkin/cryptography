﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Cryptography.Host.Api.Abstract;
using Cryptography.Host.Api.Attributes;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Security.Abstract;
using Cryptography.Security.ViewModels.Encrypting;
using Cryptography.Utills.Extenssions;
using Cryptography.Utills.Helpers;

namespace Cryptography.Security.ViewModels.Documents
{
	using System.Windows.Input;
	using DocumentStatus = Host.Api.DocumentStatus;

	public sealed class ChiperDocument : DocumentBase
	{
		public ICommand EncryptCommand { get; private set; }
		public ICommand DecryptCommand { get; private set; }

		public IReadOnlyCollection<AlgorithmPresenterViewModel> Presenters { get; }
		public IReadOnlyCollection<EnumDescriptionViewModel> EncryptionTargets => _services.Keys.ToArray();
		public AlgorithmPresenterViewModel SelectedAlgorithm
		{
			get { return _selectedPresenter; }
			set { this.UpdateProperty(value, ref _selectedPresenter); }
		}

		[Computed(nameof(EncryptionService))]
		public EnumDescriptionViewModel SelectedEncryptionTarget
		{
			get { return _selectedEcnryptionTarget; }
			set { this.UpdateProperty(value, ref _selectedEcnryptionTarget); }
		}
		public IEncryptionService EncryptionService => 
			_services[SelectedEncryptionTarget];

		public string Passphrase
		{
			get { return _passphrase; }
			set { this.UpdateProperty(value, ref _passphrase); }
		}
		public int Timeout
		{
			get { return _timeout; }
			set { this.UpdateProperty(value, ref _timeout); }
		}

		public ChiperDocument(
			IReadOnlyCollection<IAlgorithmPresenter> presenters, 
			IPasswordStorage storage)
		{
			presenters.NotNull(nameof(presenters));
			storage.AssignTo(out _storage, nameof(storage));

			Title = "Шифрование - дешифрование файлов и папок";
			Timeout = 120;

			Presenters = presenters.Select(presenter => new AlgorithmPresenterViewModel(presenter)).ToArray();
			_services = new Dictionary<EnumDescriptionViewModel, IEncryptionService>();
		}

		protected override void OnInitialized()
		{
			_services[EnumDescriptionViewModel.Create(EncryptionTarget.Text)] = new PlainTextTargetViewModel();
			_services[EnumDescriptionViewModel.Create(EncryptionTarget.File)] = new LocalFileTargetViewModel(CommandFactory, UserInterface);
			_services[EnumDescriptionViewModel.Create(EncryptionTarget.Folder)] = new FolderTargetViewModel(CommandFactory, UserInterface);
			_services[EnumDescriptionViewModel.Create(EncryptionTarget.ExternalResource)] = new RemoteResourceTargetViewModel(CommandFactory, UserInterface);

			SelectedEncryptionTarget = _services.Keys.First();
			SelectedAlgorithm = Presenters.First();

			base.OnInitialized();
		}
		protected override void CreateCommands()
		{
			base.CreateCommands();

			EncryptCommand = CommandFactory.CreateCommand(o => this.Execute(true), this.CanExecute);
			DecryptCommand = CommandFactory.CreateCommand(o => this.Execute(false), this.CanExecute);
		}
		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals(propertyName, "Passphrase", StringComparison.Ordinal))
			{
				if (String.IsNullOrWhiteSpace(Passphrase))
				{
					return "Укажите ключевое слово";
				}
				else if (Passphrase.Length < 8)
				{
					return "Длина ключевой фразы не может быть меньше 8 символов";
				}
			}
			if (String.Equals(propertyName, "Timeout", StringComparison.Ordinal))
			{
				return Timeout <= 0 ? "Укажите целое положительное значение, в секундах" : String.Empty;
			}

			return base.ValidateProperty(propertyName);
		}

		private bool CanExecute(object parameter)
		{
			return IsValid && !IsBusy &&
				EncryptionService.CanExecute(parameter);
		}
		private async void Execute(bool encrypt)
		{
			var handler = encrypt
				? new Func<CancellationToken, Task>(this.EncryptExecute)
				: new Func<CancellationToken, Task>(this.DecryptExecute);

			await this.ExecuteOperationAsync(handler);
		}
		private Task EncryptExecute(CancellationToken token)
		{
			var key = _storage.Get(this.GenerateKey());
			if (key != null)
			{
				var message = @"Ключевая фраза уже была использована. 
Ключ шифрования, ассоциированный с этой фразой будет утерян, и все документы, зашифрованные ранее будет невозможно восстановить. 
Продолжить операцию?";

				if (!UserInterface.DisplayQuestion("Внимание!", message))
				{
					return TaskHelper.FromCancellation();
				}
			}

			var encoder = SelectedAlgorithm.GetAlgorithmProvider()
				.CreateEncoder(Passphrase);

			_storage.Update(this.GenerateKey(), encoder.EncodeKey);

			return EncryptionService.EncryptAync(encoder, token)
				.WithTimeout(Timeout * 1000);
		}
		private Task DecryptExecute(CancellationToken token)
		{
			var provider = SelectedAlgorithm.GetAlgorithmProvider();
			var key = _storage.Get(this.GenerateKey());
			if (key == null)
			{
				return new InvalidOperationException(
					"Ключ алгоритма шифрования не найден").FromException();
			}

			var decoder = provider.CreateDecoder(key);

			return EncryptionService.DecryptAync(decoder, token)
				.WithTimeout(Timeout * 1000);
		}
		private string GenerateKey() =>
			$"{SelectedAlgorithm.Key}_{Passphrase}";

		private int _timeout;
		private string _passphrase;
		private AlgorithmPresenterViewModel _selectedPresenter;
		private EnumDescriptionViewModel _selectedEcnryptionTarget;
		private readonly IPasswordStorage _storage;
		private readonly IDictionary<EnumDescriptionViewModel, IEncryptionService> _services;
	}
}
