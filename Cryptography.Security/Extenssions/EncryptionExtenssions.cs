﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Cryptography.Security.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Security.Extenssions
{
	public static class FileEncryptionService
	{
		public static async Task EncryptFileAsync(this IEncoder encoder, CancellationToken token,
			string inputFilename, string outputFilename)
		{
			encoder.NotNull(nameof(encoder));

			using (var input = File.OpenRead(inputFilename))
			{
				var memory = await encoder.EncodeStreamAsync(input, token)
					.ConfigureAwait(false);

				using (memory)
				{
					await WriteDataAsync(memory.ToArray(), outputFilename, token)
						.ConfigureAwait(false);
				}
			}
		}
		public static async Task DecryptFileAsync(this IDecoder decoder, CancellationToken token,
			string inputFilename, string outputFilename)
		{
			decoder.NotNull(nameof(decoder));

			using (var input = File.OpenRead(inputFilename))
			{
				var memory = await decoder.DecodeStreamAsync(input, token)
					.ConfigureAwait(false);

				using (memory)
				{
					await WriteDataAsync(memory.ToArray(), outputFilename, token)
						.ConfigureAwait(false);
				}
			}
		}
		public static async Task EncodeStreamToFileAsync(this IEncoder encoder, CancellationToken token,
			Stream source, string filename)
		{
			encoder.NotNull(nameof(encoder));
			source.NotNull(nameof(source));

			var memory = await encoder.EncodeStreamAsync(source, token)
				.ConfigureAwait(false);

			using (memory)
			{
				await WriteDataAsync(memory.ToArray(), filename, token);
			}
		}

		private static async Task WriteDataAsync(byte[] data, string filename, CancellationToken token)
		{
			using (var file = CreateAsyncFile(filename))
			{
				await file.WriteAsync(data, 0, data.Length, token)
					.ConfigureAwait(false);
			}
		}
		private static FileStream CreateAsyncFile(string filename) =>
			new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None, BufferSize, FileOptions.Asynchronous);

		private const int BufferSize = 32 * 1024;
	}
}
