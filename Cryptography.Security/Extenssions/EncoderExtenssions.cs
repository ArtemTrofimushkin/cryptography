﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Cryptography.Security.Abstract;
using Cryptography.Utills.Extenssions;
using Cryptography.Utills.Helpers;

namespace Cryptography.Security.Extenssions
{
	public static class EncoderExtenssions
	{
		#region Encode bytes async
		public static Task<byte[]> EncodeBytesAsync(this IEncoder encoder, byte[] source) =>
			EncodeBytesAsync(encoder, source, CancellationToken.None);
		public static Task<byte[]> EncodeBytesAsync(this IEncoder encoder, byte[] source, int timeout) =>
			EncodeBytesAsync(encoder, source, timeout, CancellationToken.None);
		public static Task<byte[]> EncodeBytesAsync(this IEncoder encoder, byte[] source, int timeout, CancellationToken token) =>
			EncodeBytesAsync(encoder, source, token).WithTimeout(timeout);
		public static Task<byte[]> EncodeBytesAsync(this IEncoder encoder, byte[] source, CancellationToken token)
		{
			encoder.NotNull(nameof(encoder));
			source.NotNull(nameof(source));
			
			return Task.Run(() => encoder.EncodeBytes(source, token), token);
		}
		#endregion

		#region Encode stream async
		public static Task<MemoryStream> EncodeStreamAsync(this IEncoder encoder, Stream source) =>
			EncodeStreamAsync(encoder, source, CancellationToken.None);
		public static Task<MemoryStream> EncodeStreamAsync(this IEncoder encoder, Stream source, int timeout) =>
			EncodeStreamAsync(encoder, source, timeout, CancellationToken.None);
		public static Task<MemoryStream> EncodeStreamAsync(this IEncoder encoder, Stream source, int timeout, CancellationToken token) =>
			EncodeStreamAsync(encoder, source, token).WithTimeout(timeout);
		public static Task<MemoryStream> EncodeStreamAsync(this IEncoder encoder, Stream source, CancellationToken token)
		{
			encoder.NotNull(nameof(encoder));
			source.NotNull(nameof(source));

			return Task.Run(() => encoder.EncodeStream(source, token), token);
		}
		#endregion

		#region Encode bytes
		public static byte[] EncodeBytes(this IEncoder encoder, byte[] source) =>
			EncodeBytes(encoder, source, CancellationToken.None);
		public static byte[] EncodeBytes(this IEncoder encoder, byte[] source, CancellationToken token)
		{
			encoder.NotNull(nameof(encoder));
			source.NotNull(nameof(source));

			using (var memory = new MemoryStream(source))
			{
				return encoder.EncodeStream(memory, token).ToArray();
			}
		}
		#endregion

		#region Encode stream
		public static MemoryStream EncodeStream(this IEncoder encoder, Stream source)
		{
			encoder.NotNull(nameof(encoder));

			return encoder.EncodeStream(source, CancellationToken.None);
		}
		#endregion
	}
}
