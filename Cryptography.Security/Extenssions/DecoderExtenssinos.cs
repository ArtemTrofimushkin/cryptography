﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Cryptography.Security.Abstract;
using Cryptography.Utills.Extenssions;
using Cryptography.Utills.Helpers;

namespace Cryptography.Security.Extenssions
{
	public static class DecoderExtenssinos
	{
		#region Decode bytes async
		public static Task<byte[]> DecodeBytesAsync(this IDecoder decoder, byte[] source) =>
			DecodeBytesAsync(decoder, source, CancellationToken.None);
		public static Task<byte[]> DecodeBytesAsync(this IDecoder decoder, byte[] source, int timeout) =>
			DecodeBytesAsync(decoder, source, timeout, CancellationToken.None);
		public static Task<byte[]> DecodeBytesAsync(this IDecoder decoder, byte[] source, int timeout, CancellationToken token) =>
			DecodeBytesAsync(decoder, source).WithTimeout(timeout);
		public static Task<byte[]> DecodeBytesAsync(this IDecoder decoder, byte[] source, CancellationToken token)
		{
			decoder.NotNull(nameof(decoder));
			source.NotNull(nameof(source));

			return Task.Run(() => decoder.DecodeBytes(source, token), token);
		}
		#endregion

		#region Decode stream async
		public static Task<MemoryStream> DecodeStreamAsync(this IDecoder decoder, Stream source) =>
			DecodeStreamAsync(decoder, source, CancellationToken.None);
		public static Task<MemoryStream> DecodeStreamAsync(this IDecoder decoder, Stream source, int timeout) =>
			DecodeStreamAsync(decoder, source, timeout, CancellationToken.None);
		public static Task<MemoryStream> DecodeStreamAsync(this IDecoder decoder, Stream source, int timeout, CancellationToken token) =>
			DecodeStreamAsync(decoder, source, token).WithTimeout(timeout);
		public static Task<MemoryStream> DecodeStreamAsync(this IDecoder decoder, Stream source, CancellationToken token)
		{
			decoder.NotNull(nameof(decoder));
			source.NotNull(nameof(source));

			return Task.Run(() => decoder.DecodeStream(source, token), token);
		}
		#endregion

		#region Decode bytes
		public static byte[] DecodeBytes(this IDecoder decoder, byte[] source) =>
			DecodeBytes(decoder, source, CancellationToken.None);
		public static byte[] DecodeBytes(this IDecoder decoder, byte[] source, CancellationToken token)
		{
			decoder.NotNull(nameof(decoder));
			source.NotNull(nameof(source));

			using (var memory = new MemoryStream(source))
			{
				return decoder.DecodeStream(memory, token).ToArray();
			}
		}
		#endregion

		#region Decode stream
		public static MemoryStream DecodeStream(this IDecoder decoder, Stream source)
		{
			decoder.NotNull(nameof(decoder));

			return decoder.DecodeStream(source, CancellationToken.None);
		}
		#endregion
	}
}
