﻿using System.ComponentModel;

namespace Cryptography.Security
{
	public enum EncryptionTarget
	{
		[Description("Введенный текст")]
		Text,

		[Description("Локальный файл")]
		File,

		[Description("Папка")]
		Folder,

		[Description("Удаленный ресурс")]
		ExternalResource
	}
}
