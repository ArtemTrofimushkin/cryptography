﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Cryptography.Security.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Security.Servises
{
	public class PasswordStorage : IPasswordStorage, IDisposable
	{
		public PasswordStorage()
		{
			_data = this.LoadPasswords();
		}

		public void Create(string key, byte[] value) =>
			_data[key] = value;
		public byte[] Get(string key) =>
			_data.GetValueOrDefault(key);
		public void Update(string key, byte[] value) =>
			_data[key] = value;
		public void Remove(string key) =>
			_data.Remove(key);
		public void Dispose() =>
			this.SavePasswords();

		private string GetFilepath() =>
			Path.Combine(Environment.CurrentDirectory, Filename);
		private Dictionary<string, byte[]> LoadPasswords()
		{
			var filepath = this.GetFilepath();

			if (File.Exists(filepath))
			{
				try
				{
					using (var file = File.OpenRead(filepath))
					{
						var formatter = new BinaryFormatter();
						return (Dictionary<string, byte[]>)formatter.Deserialize(file);
					}
				}
				catch (IOException exception)
				{
					//TODO: Log exception
				}
				catch (Exception exception)
				{
					// Unexpected exception
					// TODO: Log + rethrow
					throw;
				}
			}

			return new Dictionary<string, byte[]>();
		}
		private void RemoveOldStorage(string path)
		{
			try
			{
				File.Delete(path);
			}
			catch(IOException exception)
			{
				throw;
			}
			catch(Exception exception)
			{
				throw;
			}
		}
		private void SavePasswords()
		{
			var filepath = this.GetFilepath();

			if (File.Exists(filepath))
			{
				this.RemoveOldStorage(filepath);
			}

			try
			{
				using (var file = File.Create(filepath))
				{
					var formatter = new BinaryFormatter();
					formatter.Serialize(file, _data);
				}
			}
			catch (IOException exception)
			{
				//TODO: Log exception
			}
			catch (Exception exception)
			{
				// Unexpected exception
				// TODO: Log + rethrow
				throw;
			}
		}

		private const string Filename = ".passwords";
		private readonly IDictionary<string, byte[]> _data;
	}
}
