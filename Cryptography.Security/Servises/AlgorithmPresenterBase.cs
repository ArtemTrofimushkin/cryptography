﻿using System;
using Cryptography.Security.Abstract;

namespace Cryptography.Security.Servises
{
	public abstract class AlgorithmPresenterBase : IAlgorithmPresenter
	{
		public AlgorithmType AlgorithmType { get; protected set; }
		public string Description { get; protected set; }
		public string ImageKey { get; protected set; }
		public string Title { get; protected set; }
		public string ProviderKey { get; protected set; }

		protected AlgorithmPresenterBase()
		{
			ProviderKey = this.GetType().FullName;
		}
		protected AlgorithmPresenterBase(IAlgorithmProvider provider): this()
		{
			if (provider == null)
			{
				throw new ArgumentNullException(nameof(provider));
			}

			_provider = provider;
		}
		protected virtual IAlgorithmProvider CreateProvider()
		{
			return _provider;
		}
		IAlgorithmProvider IAlgorithmPresenter.CreateProvider() =>
			this.CreateProvider();

		protected IAlgorithmProvider _provider;
	}
}
