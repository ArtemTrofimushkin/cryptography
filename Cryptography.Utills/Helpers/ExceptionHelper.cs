﻿using System;
using System.Linq;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Utills.Helpers
{
	public static class ExceptionHelper
	{
		public static string GetErrorMessage(Exception exception)
		{
			exception.NotNull(nameof(exception));

			return exception.InnerException == null ?
				exception.Message : exception.GetBaseException().Message;
		}

		public static string GetErrorMessage(AggregateException exception)
		{
			exception.NotNull(nameof(exception));

			var messages = exception.Flatten()
				.InnerExceptions
				.Select(GetErrorMessage);

			return String.Join($"{Environment.NewLine}", messages);
		}
	}
}
