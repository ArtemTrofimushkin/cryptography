﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Cryptography.Utills.Helpers
{
	public static class EnumHelper
	{
		public static string GetDescription<TEnum>(TEnum value)
			where TEnum : struct, IConvertible
		{
			if (!typeof(TEnum).IsEnum)
			{
				throw new ArgumentException(
					$"Generic parameter must be {nameof(Enum)}. Actual type is: {typeof(TEnum)}");
			}

			return EnumInternalCache<TEnum>.GetCachedEnumDescription(value);
		}
		public static IReadOnlyCollection<string> GetEnumDescriptions<TEnum>()
			where TEnum : struct, IConvertible
		{
			if (!typeof(TEnum).IsEnum)
			{
				throw new ArgumentException(
					$"Generic parameter must be {nameof(Enum)}. Actual type is: {typeof(TEnum)}");
			}

			return EnumInternalCache<TEnum>.GetCachedEnumDescriptions();
		}

		private class EnumInternalCache<TEnum>
			where TEnum : struct, IConvertible
		{
			static EnumInternalCache()
			{
				Descriptions = Enum.GetValues(typeof(TEnum))
					.Cast<TEnum>()
					.ToDictionary(x => x, GetDescription);
			}

			public static string GetCachedEnumDescription(TEnum value) =>
				Descriptions[value];

			public static IReadOnlyCollection<string> GetCachedEnumDescriptions() =>
				Descriptions.Values.ToArray();

			private static string GetDescription(TEnum value)
			{
				var attribute = GetAttribute<DescriptionAttribute>(value);

				return attribute == null ? value.ToString() :
					attribute.Description;
			}
			private static TAttribute GetAttribute<TAttribute>(TEnum value)
				where TAttribute : Attribute
			{
				return value.GetType()
					.GetMember(value.ToString())
					.ElementAt(0)
					.GetCustomAttribute<TAttribute>(inherit: false);
			}

			private static readonly Dictionary<TEnum, string> Descriptions;
		}
	}
}
