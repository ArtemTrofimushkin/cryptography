﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Utills.Helpers
{
	public struct VoidResult
	{
	}

	public static class TaskHelper
	{
		public static VoidResult EmptyResult => new VoidResult();
		public static Task FromCancellation()
		{
			var source = new TaskCompletionSource<VoidResult>();
			source.SetCanceled();

			return source.Task;
		}
		public static Task FromException(this Exception exception)
		{
			exception.NotNull(nameof(exception));

			var source = new TaskCompletionSource<VoidResult>();
			source.SetException(exception);

			return source.Task;
		}

		public static async Task WithCancellation(this Task task, CancellationToken token)
		{
			task.NotNull(nameof(task));
			token.ThrowIfCancellationRequested();

			var source = new TaskCompletionSource<VoidResult>();
			using (token.Register(state => ( (TaskCompletionSource<VoidResult>)state ).SetResult(default(VoidResult)), source))
			{
				var any = await Task.WhenAny(task, source.Task);
				if (any != task)
				{
					token.ThrowIfCancellationRequested();
				}
			}
			await task;
		}
		public static async Task<TResult> WithCancellation<TResult>(this Task<TResult> task, CancellationToken token)
		{
			task.NotNull(nameof(task));
			token.ThrowIfCancellationRequested();

			var source = new TaskCompletionSource<TResult>();
			using (token.Register(state => ( (TaskCompletionSource<TResult>)state ).SetResult(default(TResult)), source))
			{
				var any = await Task.WhenAny(task, source.Task);
				if (any != task)
				{
					token.ThrowIfCancellationRequested();
				}
			}
			return await task;
		}

		public static Task WithTimeout(this Task task, int timeout)
		{
			task.NotNull(nameof(task));

			if (task.IsCompleted ||
				timeout == Timeout.Infinite)
			{
				return task;
			}

			var taskSource = new TaskCompletionSource<VoidResult>();

			if (timeout == 0)
			{
				taskSource.SetException(
					CreateTimeoutException(timeout));

				return taskSource.Task;
			}

			var timer = CreateTimer(timeout, taskSource);

			task.ContinueWith(
				(prev, state) => ContinuationFunction(prev, (TaskCompletionSourceTimerPair<VoidResult>)state),
					CreatePair(taskSource, timer), TaskContinuationOptions.ExecuteSynchronously);

			return taskSource.Task;
		}
		public static Task<TResult> WithTimeout<TResult>(this Task<TResult> task, int timeout)
		{
			task.NotNull(nameof(task));

			if (task.IsCompleted || 
				timeout == Timeout.Infinite)
			{
				return task;
			}

			var taskSource = new TaskCompletionSource<TResult>();

			if (timeout == 0)
			{
				taskSource.SetException(
					CreateTimeoutException(timeout));

				return taskSource.Task;
			}

			var timer = CreateTimer(timeout, taskSource);

			task.ContinueWith(
				(prev, state) => ContinuationFunction(prev, (TaskCompletionSourceTimerPair<TResult>)state), 
					CreatePair(taskSource, timer), TaskContinuationOptions.ExecuteSynchronously);

			return taskSource.Task;
		}

		#region Helper methods
		private static TimeoutException CreateTimeoutException(int timeout) =>
			new TimeoutException($"Expired timeout, value: {timeout}");
		private static TaskCompletionSourceTimerPair<TResult> CreatePair<TResult>(TaskCompletionSource<TResult> tcs, Timer timer) =>
			new TaskCompletionSourceTimerPair<TResult>(tcs, timer);
		private static Timer CreateTimer<TResult>(int timeout, TaskCompletionSource<TResult> tcs)
		{
			return new Timer(
				state => TimerTaskFunction((TaskCompletionSource<TResult>)state, timeout), 
					tcs, timeout, Timeout.Infinite);
		}

		private static void TimerTaskFunction<TResult>(TaskCompletionSource<TResult> source, int timeout)
		{
			source.TrySetException(
				CreateTimeoutException(timeout));
		}
		private static void ContinuationFunction(Task task, TaskCompletionSourceTimerPair<VoidResult> pair)
		{
			pair.Timer.Dispose();
			var proxy = pair.TaskCompletionSource;

			switch (task.Status)
			{
				case TaskStatus.Faulted:
					proxy.TrySetException(task.Exception);
					break;
				case TaskStatus.Canceled:
					proxy.TrySetCanceled();
					break;
				case TaskStatus.RanToCompletion:
					proxy.TrySetResult(new VoidResult());
					break;
			}
		}
		private static void ContinuationFunction<TResult>(Task<TResult> task, TaskCompletionSourceTimerPair<TResult> pair)
		{
			pair.Timer.Dispose();
			var proxy = pair.TaskCompletionSource;

			switch (task.Status)
			{
				case TaskStatus.Faulted:
					proxy.TrySetException(task.Exception);
					break;
				case TaskStatus.Canceled:
					proxy.TrySetCanceled();
					break;
				case TaskStatus.RanToCompletion:
					proxy.TrySetResult(task.Result);
					break;
			}
		}
		#endregion

		private struct TaskCompletionSourceTimerPair<TResult>
		{
			public TaskCompletionSource<TResult> TaskCompletionSource { get; }
			public Timer Timer { get; }

			public TaskCompletionSourceTimerPair(TaskCompletionSource<TResult> source, Timer timer)
			{
				TaskCompletionSource = source;
				Timer = timer;
			}
		}
	}
}
