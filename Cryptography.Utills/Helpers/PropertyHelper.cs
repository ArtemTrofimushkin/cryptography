﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Utills.Helpers
{
	public static class PropertyHelper
	{
		public static IReadOnlyCollection<PropertyInfo> GetProperties<T>(BindingFlags flags) =>
			GetProperties(typeof(T), flags);

		public static IReadOnlyCollection<PropertyInfo> GetProperties(Type type, BindingFlags flags)
		{
			type.NotNull(nameof(type));

			var key = new PropertyKey(type, flags);
			var value = CachedProperties.GetValueOrDefault(key);
			if (value == null)
			{
				value = GetReflectedProperties(type, flags);
				CachedProperties[key] = value;
			}

			return value;
		}

		private static IReadOnlyCollection<PropertyInfo> GetReflectedProperties(Type type, BindingFlags flags) =>
			type.GetProperties(flags);

		private struct PropertyKey : IEquatable<PropertyKey>
		{
			public Type TargetType { get; }
			public BindingFlags BindingFlags { get; }

			public PropertyKey(Type targetType, BindingFlags bindingFlags)
			{
				targetType.NotNull(nameof(targetType));

				TargetType = targetType;
				BindingFlags = bindingFlags;
			}

			public bool Equals(PropertyKey other)
			{
				return TargetType.Equals(other.TargetType) &&
					BindingFlags == other.BindingFlags;
			}

			public override bool Equals(object obj)
			{
				return obj is PropertyKey &&
					this.Equals((PropertyKey)obj);
			}

			public override int GetHashCode() =>
				TargetType.GetHashCode();

			public override string ToString() =>
				$"Type: {TargetType} Flags: {BindingFlags}";
		}

		private static IDictionary<PropertyKey, IReadOnlyCollection<PropertyInfo>> CachedProperties =
			new Dictionary<PropertyKey, IReadOnlyCollection<PropertyInfo>>();
	}
}
