﻿using System;
using System.Collections.Generic;

namespace Cryptography.Utills.Extenssions
{
	public static class EnumerableExtenssions
	{
		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
		{
			source.NotNull(nameof(source));
			action.NotNull(nameof(action));

			foreach (var item in source)
			{
				action(item);
			}
		}

		public static T[] OneToArray<T>(this T source)
		{
			return new[] { source };
		}

		public static IList<T> OneToList<T>(this T source)
		{
			return new List<T>
			{
				source
			};
		}

		public static ISet<T> ToSet<T>(this IEnumerable<T> source)
		{
			source.NotNull(nameof(source));

			var set = new HashSet<T>();
			set.AddRange(source);

			return set;
		}
	}
}
