﻿using System;
using Cryptography.Utills.Helpers;

namespace Cryptography.Utills.Extenssions
{
	public static class EnumExtenssions
	{
		public static string GetDescription<TEnum>(this TEnum value) where TEnum : struct, IConvertible =>
			EnumHelper.GetDescription(value);
	}
}
