﻿using System;
using Cryptography.Utills.Helpers;

namespace Cryptography.Utills.Extenssions
{
	public static class ExceptionExtenssions
	{
		public static string GetAllErrorMessages(this AggregateException exception) =>
			ExceptionHelper.GetErrorMessage(exception);
	}
}
