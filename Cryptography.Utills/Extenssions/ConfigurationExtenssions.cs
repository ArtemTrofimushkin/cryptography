﻿using System;
using System.Configuration;

namespace Cryptography.Utills.Extenssions
{
	public static class ConfigurationExtenssions
	{
		public static TSection GetSectionOrDefault<TSection>(string sectionName, Func<TSection> defaultSection)
			where TSection : ConfigurationSection
		{
			sectionName.NotNull(nameof(sectionName));
			defaultSection.NotNull(nameof(defaultSection));

			var section = ConfigurationManager.GetSection(sectionName) as TSection;
			return section ?? defaultSection();
		}

		public static TSection GetExeSectionOrDefault<TSection>(string sectionName, Func<TSection> defaultSection)
			where TSection : ConfigurationSection
		{
			sectionName.NotNull(nameof(sectionName));
			defaultSection.NotNull(nameof(defaultSection));

			var section = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
				.GetSection(sectionName) as TSection;

			return section ?? defaultSection();
		}

		public static TSection GetSection<TSection>(string sectionName) 
			where TSection: ConfigurationSection
		{
			sectionName.NotNull(nameof(sectionName));

			var section = ConfigurationManager.GetSection(sectionName) as TSection;

			if (section == null)
			{
				throw new InvalidOperationException(
					$"{sectionName} is not presented or has invalid type.");
			}

			return section;
		}

		public static TSection GetExeSection<TSection>(string sectionName)
			where TSection : ConfigurationSection
		{
			sectionName.NotNull(nameof(sectionName));

			var section = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
				.GetSection(sectionName) as TSection;

			if (section == null)
			{
				throw new InvalidOperationException(
					$"{sectionName} is not presented or has invalid type.");
			}

			return section;
		}
	}
}
