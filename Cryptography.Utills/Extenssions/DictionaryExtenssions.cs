﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Cryptography.Utills.Extenssions
{
	public static class DictionaryExtenssions
	{
		public static TValue GetValueOrDefault<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary, 
			TKey key, TValue defaultValue = default(TValue))
		{
			dictionary.NotNull(nameof(dictionary));

			TValue value;
			return dictionary.TryGetValue(key, out value) ? value : defaultValue;
		}

		public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary,
			TKey key, TValue defaultValue = default(TValue))
		{
			dictionary.NotNull(nameof(dictionary));

			TValue value;
			return dictionary.TryGetValue(key, out value) ? value : defaultValue;
		}

		public static IReadOnlyDictionary<TKey, TValue> AsReadOnly<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
		{
			dictionary.NotNull(nameof(dictionary));
			return new ReadOnlyDictionary<TKey, TValue>(dictionary);
		}
	}
}
