﻿using System;

namespace Cryptography.Utills.Extenssions
{
	public static class RequiredExtenssions
	{
		public static void NotNull(this string argument, string name)
		{
			if (String.IsNullOrWhiteSpace(argument))
			{
				throw new ArgumentNullException(name);
			}
		}

		public static void NotNull<TClass>(this TClass argument, string name) where TClass : class
		{
			if (argument == null)
			{
				throw new ArgumentNullException(name);
			}
		}

		public static void NotNull<TClass>(this TClass argument, string name, Func<TClass, bool> nullCheck) where TClass : class
		{
			if (nullCheck(argument))
			{
				throw new ArgumentNullException(name);
			}
		}

		public static void AssignTo(this string argument, out string storage, string name)
		{
			argument.NotNull(name);
			storage = argument;
		}

		public static void AssignTo<TClass>(this TClass argument, out TClass storage, string name) where TClass : class
		{
			argument.NotNull(name);
			storage = argument;
		}

		public static void AssignTo<TClass>(this TClass argument, out TClass storage, string name, Func<TClass, bool> nullCheck) where TClass : class
		{
			argument.NotNull(name, nullCheck);
			storage = argument;
		}
	}
}
