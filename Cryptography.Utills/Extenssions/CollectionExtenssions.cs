﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.Utills.Extenssions
{
	public static class CollectionExtenssions
	{
		public static void AddRange<T>(this ICollection<T> source, IEnumerable<T> elements)
		{
			source.NotNull(nameof(source));
			elements.NotNull(nameof(elements));

			var elementsCollection = elements.ToArray();
			if (!elementsCollection.Any())
			{
				return;
			}

			foreach(var element in elements)
			{
				source.Add(element);
			}
		}
		public static void RemoveRange<T>(this ICollection<T> source, IEnumerable<T> elements)
		{
			source.NotNull(nameof(source));
			elements.NotNull(nameof(elements));

			var elementsCollection = elements.ToArray();
			if (!elementsCollection.Any())
			{
				return;
			}

			foreach (var element in elementsCollection)
			{
				source.Remove(element);
			}
		}
		public static int LastIndexOf<T>(this T[] array, Func<T, bool> predicate)
		{
			var item = array.Last(predicate);
			return Array.LastIndexOf(array, item);
		}
		public static T[] Copy<T>(this T[] array, int lenth)
		{
			var result = new T[lenth];
			Array.Copy(array, result, lenth);
			return result;
		}
		public static T[] Copy<T>(this T[] array, ulong lenth)
		{
			var result = new T[lenth];
			Array.Copy(array, result, (long)lenth);
			return result;
		}
		public static void Swap<T>(this T[] array, int left, int right)
		{
			#region Validate
			array.NotNull(nameof(array));

			if (left < 0 || left >= array.Length)
			{
				throw new ArgumentOutOfRangeException(nameof(left));
			}

			if (right < 0 || right >= array.Length)
			{
				throw new ArgumentOutOfRangeException(nameof(left));
			}
			#endregion

			T temporary = array[left];
			array[left] = array[right];
			array[right] = temporary;
		}
	}
}
