﻿using System;
using System.Globalization;
using System.Windows.Data;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.UI.Converters
{
	public class TypeToBoolConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
			{
				return false;
			}
			parameter.NotNull(nameof(parameter));

			return ( (Type)parameter ).IsInstanceOfType(value);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
