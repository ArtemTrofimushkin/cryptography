﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.UI.Converters
{
	public class ResourceImageConverter : IValueConverter
	{
		public static ResourceImageConverter Converter { get; } = new ResourceImageConverter();

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			value.NotNull(nameof(value));

			var application = Application.Current;
			if (application == null)
			{
				return null;
			}

			return application.FindResource(value.ToString());
		}
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
