﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Cryptography.Host.UI.Converters
{
	public class TypeIsMatchConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null || 
				parameter == null)
			{
				return null;
			}

			return ( (Type)parameter ).IsAssignableFrom(value.GetType()) ? value : null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
