﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Host.UI.Converters
{
	public class TypeToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			parameter.NotNull(nameof(parameter));
			if (value == null || !( parameter is Type ))
			{
				return Visibility.Hidden;
			}

			return value.GetType().IsAssignableFrom((Type)parameter) ? Visibility.Visible : Visibility.Hidden;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
