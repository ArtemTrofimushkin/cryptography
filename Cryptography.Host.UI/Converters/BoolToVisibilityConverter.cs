﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Cryptography.Host.UI.Converters
{
	public class BoolToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is bool))
			{
				return Visibility.Hidden;
			}

			return ( (bool)value ) ? Visibility.Visible : Visibility.Hidden;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
