﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Cryptography.Host.UI.Converters
{
	public class InvertBoolToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!( value is bool ))
			{
				return Visibility.Visible;
			}

			return ( (bool)value ) ? Visibility.Hidden : Visibility.Visible;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
