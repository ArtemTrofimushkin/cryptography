﻿using System;
using System.Collections.Generic;

namespace Cryptography.Host.UI.Abstract
{
	public interface IResourceProvider
	{ 
		IReadOnlyCollection<Uri> GetResourceDictionaries();
	}
}
