﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Cryptography.Host.UI.Behaviors
{
	public class PasswordBehavior : Behavior<PasswordBox>
	{
		public static readonly DependencyProperty PasswordProperty =
			DependencyProperty.Register(nameof(Password), typeof(string), typeof(PasswordBehavior), new PropertyMetadata(null));

		public string Password
		{
			get { return (string)GetValue(PasswordProperty); }
			set { SetValue(PasswordProperty, value); }
		}

		public static readonly DependencyProperty AllowEmptyProperty =
			DependencyProperty.Register(nameof(AllowEmpty), typeof(bool), typeof(PasswordBehavior), new PropertyMetadata(false));

		public bool AllowEmpty
		{
			get { return (bool)GetValue(AllowEmptyProperty); }
			set { SetValue(AllowEmptyProperty, value); }
		}

		public static readonly DependencyProperty MinPasswordLengthProperty =
			DependencyProperty.Register(nameof(MinPasswordLength), typeof(int), typeof(PasswordBehavior), new PropertyMetadata(10));

		public int MinPasswordLength
		{
			get { return (int)GetValue(MinPasswordLengthProperty); }
			set { SetValue(MinPasswordLengthProperty, value); }
		}

		protected override void OnAttached()
		{
			AssociatedObject.PasswordChanged += this.OnPasswordChanged;
		}
		protected override void OnDetaching()
		{
			AssociatedObject.PasswordChanged -= this.OnPasswordChanged;
		}
		protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs args)
		{
			base.OnPropertyChanged(args);

			if (args.Property != PasswordProperty || !_skipUpdate)
			{
				return;
			}

			var password = (string)args.NewValue;

			_skipUpdate = true;
			AssociatedObject.Password = password;
			_skipUpdate = false;
		}

		private void OnPasswordChanged(object sender, RoutedEventArgs e)
		{
			_skipUpdate = true;
			Password = AssociatedObject.Password;
			_skipUpdate = false;
		}

		private bool _skipUpdate;
	}
}
