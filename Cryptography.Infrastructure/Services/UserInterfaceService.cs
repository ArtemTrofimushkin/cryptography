﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;

using Cryptography.Host.Api.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Infrastructure.Services
{
	using Application = System.Windows.Application;
	using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
	using SaveFileDialog = Microsoft.Win32.SaveFileDialog;
	using IWin32Window = System.Windows.Forms.IWin32Window;
	using MessageBox = System.Windows.MessageBox;

	public class UserInterfaceService : IUserInterfaceService
	{
		public bool DisplayQuestion(string caption, string text)
		{
			caption.NotNull(nameof(caption));
			text.NotNull(nameof(text));

			return this.DisplayMessage(caption, text, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;
		}
		public void DisplayErrorMessage(string caption, string text)
		{
			caption.NotNull(nameof(caption));
			text.NotNull(nameof(text));

			this.DisplayMessage(caption, text, MessageBoxButton.OK, MessageBoxImage.Error);
		}
		public void DisplayWarningMessage(string caption, string text)
		{
			caption.NotNull(nameof(caption));
			text.NotNull(nameof(text));

			this.DisplayMessage(caption, text, MessageBoxButton.OK, MessageBoxImage.Warning);
		}
		public void DisplayInfoMessage(string caption, string text)
		{
			caption.NotNull(nameof(caption));
			text.NotNull(nameof(text));

			this.DisplayMessage(caption, text, MessageBoxButton.OK, MessageBoxImage.Information);
		}

		public string OpenFileDialog(string mask)
		{
			var filter = mask ?? DefaultMask;
			var dialog = new OpenFileDialog
			{
				CheckFileExists = true,
				CheckPathExists = true,
				Filter = filter
			};

			if (dialog.ShowDialog() == true)
			{
				return dialog.FileName;
			}
			return String.Empty;
		}
		public string SaveFileDialog(string mask)
		{
			var filter = mask ?? DefaultMask;
			var dialog = new SaveFileDialog
			{
				Filter = filter,
				ValidateNames = true
			};

			if (dialog.ShowDialog() == true)
			{
				return dialog.FileName;
			}
			return String.Empty;
		}
		public string OpenFolderDialog()
		{
			var window = Application.Current?.MainWindow;
			if (window == null)
			{
				return String.Empty;
			}

			var source = PresentationSource.FromVisual(window) as HwndSource;
			using (var dialog = new FolderBrowserDialog())
			{
				var result = dialog.ShowDialog(
					new NativeWindow(source.Handle));

				if (result == DialogResult.OK)
				{
					return dialog.SelectedPath;
				}
			}
			
			return String.Empty;
		}

		private MessageBoxResult DisplayMessage(string caption, string text, MessageBoxButton button, MessageBoxImage icon) =>
			MessageBox.Show(text, caption, button, icon);

		private sealed class NativeWindow : IWin32Window
		{
			public IntPtr Handle { get; }

			public NativeWindow(IntPtr handle) 
			{
				Handle = handle;
			}
		}

		private const string DefaultMask = "*.* (Все файлы)|*.*";
	}
}
