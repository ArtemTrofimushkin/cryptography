﻿using System.Windows;
using Microsoft.Windows.Controls.Ribbon;

namespace Cryptography.Infrastructure.Ribbon
{
	public static class RibbonHelper
	{
		public static bool GetIsSelected(RibbonTab tab) =>
			(bool)tab.GetValue(IsSelectedProperty);

		public static void SetIsSelected(RibbonTab tab, bool value) =>
			tab.SetValue(IsSelectedProperty, value);

		private static object OnCoerceValue(DependencyObject sender, object parameter)
		{
			var tab = sender as RibbonTab;
			if ((tab != null) && 
				(parameter is bool))
			{
				tab.IsSelected = (bool)parameter;
			}

			return parameter;
		}

		public static DependencyProperty IsSelectedProperty = DependencyProperty.RegisterAttached("IsSelected", 
			typeof(bool), typeof(RibbonHelper), new FrameworkPropertyMetadata(false, null, OnCoerceValue));
	}
}
