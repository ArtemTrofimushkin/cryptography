﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Controls.Primitives;

using Cryptography.Host.UI.Converters;
using Cryptography.Host.Api.VisualActions;

using Microsoft.Windows.Controls.Ribbon;

namespace Cryptography.Infrastructure.Ribbon
{
	using Ribbon = Microsoft.Windows.Controls.Ribbon.Ribbon;

	public static class RibbonAdapter
	{
		#region VisualActionGroupTabProperty
		public static VisualActionGroupTab GetGroupTab(DependencyObject obj) =>
			(VisualActionGroupTab)obj.GetValue(VisualActionGroupTabProperty);

		public static void SetGroupTab(DependencyObject obj, VisualActionGroupTab value) =>
			obj.SetValue(VisualActionGroupTabProperty, value);

		public static DependencyProperty VisualActionGroupTabProperty = 
			DependencyProperty.RegisterAttached("ContextualGroupTab", typeof(VisualActionGroupTab), typeof(RibbonAdapter));
		#endregion

		#region ActionContainersProperty
		public static IReadOnlyCollection<VisualActionContainer> GetActionContainers(Ribbon obj) =>
			(IReadOnlyCollection<VisualActionContainer>)obj.GetValue(ActionContainersProperty);

		public static void SetActionContainers(Ribbon obj, IEnumerable<VisualActionContainer> value) =>
			obj.SetValue(ActionContainersProperty, value);

		private static void OnRibbonLoaded(object sender, EventArgs args)
		{
			var ribbon = sender as Ribbon;
			if (ribbon == null)
			{
				return;
			}

			ribbon.Loaded -= OnRibbonLoaded;
			ConfigureRibbon(ribbon);
		}
		private static void OnActionProvidersPropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
		{
			if (args.NewValue == null)
			{
				return;
			}

			var ribbon = sender as Ribbon;
			if (ribbon != null)
			{
				ribbon.Loaded += OnRibbonLoaded;
			}
		}

		public static DependencyProperty ActionContainersProperty = DependencyProperty.RegisterAttached("ActionContainers", 
				typeof(IReadOnlyCollection<VisualActionContainer>), typeof(RibbonAdapter), new UIPropertyMetadata(null, OnActionProvidersPropertyChanged));
		#endregion

		#region Visual actions configuration
		private static void ConfigureRibbon(Ribbon ribbon)
		{
			var containers = GetActionContainers(ribbon);
			if (containers == null)
			{
				return;
			}

			foreach (var container in containers)
			{
				ConfigureContextualGroups(container, ribbon);
				ConfigureGroupTabs(container, ribbon);
			}
		}
		private static void ConfigureContextualGroups(VisualActionContainer container, Ribbon ribbon)
		{
			foreach(var contextualGroup in container.ContextualGroups)
			{
				ValidateContextualGroup(contextualGroup, ribbon);

				var ribbonContextualGroup = new RibbonContextualTabGroup
				{
					Header = contextualGroup.Title,
					Name = contextualGroup.Name,
				};

				if (!String.IsNullOrWhiteSpace(contextualGroup.BackgroundAndBorder))
				{
					var color = (Color)ColorConverter.ConvertFromString(contextualGroup.BackgroundAndBorder);
					ribbonContextualGroup.Background = new SolidColorBrush(color);
				}

				var dataContextBinding = new Binding(ActiveDocument)
				{
					Converter = new TypeToVisibilityConverter(),
					ConverterParameter = contextualGroup.ContextualObjectType,
					Mode = BindingMode.OneWay,
					Source = ribbon.DataContext
				};

				ribbonContextualGroup.SetBinding(UIElement.VisibilityProperty, dataContextBinding);
				ribbon.ContextualTabGroups.Add(ribbonContextualGroup);
				ribbon.RegisterName(contextualGroup.Name, ribbonContextualGroup);
			}
		}
		private static void ConfigureGroupTabs(VisualActionContainer container, Ribbon ribbon)
		{
			for (int i = 0; i != container.GroupTabs.Count; ++i)
			{
				var groupTab = container.GroupTabs[i];

				ValidateGroupTab(groupTab);

				var ribbonTab = new RibbonTab
				{
					Header = groupTab.Header,
					Name = $"TabGroup_{i}_{groupTab.ContextualGroupName}"
				};

				if (groupTab.IsContextual)
				{
					ConfigureContextualGroupTab(container, ribbon, groupTab, ribbonTab);
				}

				CreateVisualActionGroup(groupTab.Groups, ribbonTab);
				ribbon.Items.Add(ribbonTab);
			}
		}
		private static void ConfigureContextualGroupTab(VisualActionContainer container, Ribbon ribbon, 
			VisualActionGroupTab groupTab, RibbonTab ribbonTab)
		{
			var contextualGroup = container.ContextualGroups.FirstOrDefault(
				x => String.Equals(x.Name, groupTab.ContextualGroupName, StringComparison.Ordinal));

			if (contextualGroup == null)
			{
				throw new InvalidOperationException(
					$"Contextual group with identity: {groupTab.ContextualGroupName} not presented.");
			}

			ribbonTab.ContextualTabGroupHeader = contextualGroup.Title;

			var dataContextBinding = new Binding(ActiveDocument)
			{
				Converter = new TypeIsMatchConverter(),
				ConverterParameter = contextualGroup.ContextualObjectType,
				Source = ribbon.DataContext,
			};

			var selectedBinding = new Binding()
			{
				Converter = new TypeToBoolConverter(),
				ConverterParameter = contextualGroup.ContextualObjectType
			};

			ribbonTab.SetBinding(FrameworkContentElement.DataContextProperty, dataContextBinding);
			ribbonTab.SetBinding(RibbonHelper.IsSelectedProperty, selectedBinding);

			SetGroupTab(ribbonTab, groupTab);
		}
		private static void CreateVisualActionGroup(List<VisualActionGroup> groups, RibbonTab tab)
		{
			foreach (var group in groups)
			{
				var ribbonGroup = new RibbonGroup
				{
					Header = group.Title
				};

				foreach (var action in group.Actions)
				{
					CreateVisualAction(action, ribbonGroup);
				}

				tab.Items.Add(ribbonGroup);
			}
		}
		private static void CreateVisualAction(VisualAction action, RibbonGroup group)
		{
			var button = new RibbonButton
			{
				Label = action.Title
			};

			ConfigureActionCommandBinding(action, button);

			if (!String.IsNullOrWhiteSpace(action.Icon))
			{
				ConfigureActionIcon(action, button);
			}

			group.Items.Add(button);
		}
		private static void ConfigureActionIcon(VisualAction action, RibbonButton button)
		{
			var image = (ImageSource)ResourceImageConverter.Converter
				.Convert(action.Icon, typeof(ImageSource), null, CultureInfo.CurrentUICulture);

			if (action.IconSize == VisalActionIconSize.Small)
			{
				button.SmallImageSource = image;
			} 
			else
			{
				button.LargeImageSource = image;
			}
		}
		private static void ConfigureActionCommandBinding(VisualAction action, RibbonButton button)
		{
			if (action.IsRootCommand)
			{
				if (action.CommandParameter == null)
				{
					throw new InvalidOperationException(
						$"For root actions, {nameof(action.CommandParameter)} is required");
				}

				button.SetBinding(ButtonBase.CommandProperty, CreateDocumentCommand);
				button.CommandParameter = action.CommandParameter;
			} 
			else
			{
				if (action.CommandParameter != null)
				{
					button.CommandParameter = action.CommandParameter;
				}
				button.SetBinding(ButtonBase.CommandProperty, action.Command);
			}
		}

		#region Validate
		private static void ValidateContextualGroup(VisualActionContextualGroup contextualGroup, Ribbon ribbon)
		{
			if (String.IsNullOrWhiteSpace(contextualGroup.Title))
			{
				throw new InvalidOperationException(
					"Contextual group title is required.");
			}
			if (String.IsNullOrWhiteSpace(contextualGroup.Name))
			{
				throw new InvalidOperationException(
					"Contextual group name is required.");
			}
			if (contextualGroup.ContextualObjectType == null)
			{
				throw new InvalidOperationException(
					"Contextual object type is missing. ");
			}
			var source = ribbon.FindName(contextualGroup.Name);
			if (source != null)
			{
				throw new InvalidOperationException(
					$"Contextual group name ({contextualGroup.Name}) already registered. ");
			}
		}

		private static void ValidateGroupTab(VisualActionGroupTab groupTab)
		{
			if (String.IsNullOrWhiteSpace(groupTab.Header))
			{
				throw new InvalidOperationException(
					"Group tab header is required.");
			}

			if (groupTab.IsContextual && String.IsNullOrWhiteSpace(groupTab.ContextualGroupName))
			{
				throw new InvalidOperationException(
					"Contextual group tab name is required.");
			}
		}
		#endregion
		#endregion

		#region Constants 
		private const string ActiveDocument = "DocumentManager.ActiveDocument";
		private const string CreateDocumentCommand = "CreateDocumentCommand";
		#endregion
	}
}
