﻿using System;
using System.Threading;
using Cryptography.Host.Api.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Infrastructure.Commands
{
	public sealed class AsyncCancelCommand : RelayCommandBase, IAsyncCancelCommand
	{
		public CancellationToken Token =>
			_source.Token;

		public AsyncCancelCommand()
		{
		}
		public AsyncCancelCommand(Func<object, bool> validate)
		{
			validate.AssignTo(out _validate, nameof(validate));
		}
		public override bool CanExecute(object parameter)
		{
			return (_validate == null || _validate(parameter)) &&
				_executing && !_source.IsCancellationRequested;
		}
		public override void Execute(object parameter)
		{
			_source.Cancel();
			this.RaiseCanExecuteChanged();
		}

		public void Accuire()
		{
			_executing = true;
			if (!_source.IsCancellationRequested)
			{
				return;
			}
			_source = new CancellationTokenSource();

			this.RaiseCanExecuteChanged();
		}
		public void Release()
		{
			_executing = false;
			this.RaiseCanExecuteChanged();
		}

		private bool _executing;
		private readonly Func<object, bool> _validate;
		private CancellationTokenSource _source = new CancellationTokenSource();
	}
}
