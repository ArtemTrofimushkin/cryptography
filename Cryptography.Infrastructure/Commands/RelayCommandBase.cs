﻿using System;
using System.Windows.Input;

namespace Cryptography.Infrastructure.Commands
{
	public abstract class RelayCommandBase : ICommand
	{
		public event EventHandler CanExecuteChanged
		{
			add
			{
				CommandManager.RequerySuggested += value;
			}
			remove
			{
				CommandManager.RequerySuggested -= value;
			}
		}

		public abstract bool CanExecute(object parameter);
		public abstract void Execute(object parameter);

		protected virtual void RaiseCanExecuteChanged() =>
			CommandManager.InvalidateRequerySuggested();
	}
}
