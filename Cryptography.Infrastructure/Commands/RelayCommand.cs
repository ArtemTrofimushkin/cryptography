﻿using System;
using System.Windows.Input;

using Cryptography.Utills.Extenssions;

namespace Cryptography.Infrastructure.Commands
{
	public sealed class RelayCommand : RelayCommandBase
	{
		public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
		{
			execute.AssignTo(out _execute, nameof(execute));
			_canExecute = canExecute;
		}

		public override bool CanExecute(object parameter) =>
			_canExecute == null || _canExecute(parameter);

		public override void Execute(object parameter) =>
			_execute(parameter);

		private readonly Action<object> _execute;
		private readonly Func<object, bool> _canExecute;
	}
}
