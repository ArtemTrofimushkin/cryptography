﻿using System;
using System.Windows.Input;

using Cryptography.Host.Api.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.Infrastructure.Commands
{
	public class RelayCommandFactory : ICommandFactory
	{
		#region ICommand
		public ICommand CreateCommand(Action action)
		{
			action.NotNull(nameof(action));
			return new RelayCommand(o => action());
		}
		public ICommand CreateCommand(Action action, Func<bool> validate)
		{
			action.NotNull(nameof(action));
			validate.NotNull(nameof(action));
			return new RelayCommand(o => action(), o => validate());
		}

		public ICommand CreateCommand(Action<object> action) =>
			new RelayCommand(action);
		public ICommand CreateCommand(Action<object> action, Func<object, bool> validate)
		{
			validate.NotNull(nameof(validate));
			return new RelayCommand(action, validate);
		}

		public ICommand CreateCommand<T>(Action<T> action)
		{
			action.NotNull(nameof(action));
			return new RelayCommand(o => action((T)o));
		}
		public ICommand CreateCommand<T>(Action<T> action, Func<T, bool> validate)
		{
			action.NotNull(nameof(action));
			validate.NotNull(nameof(action));
			return new RelayCommand(o => action((T)o), o => validate((T)o));
		}
		#endregion

		#region IAsyncCancelCommand
		public IAsyncCancelCommand CreateCancelCommand() =>
			new AsyncCancelCommand();
		public IAsyncCancelCommand CreateCancelCommand(Func<object, bool> validate) =>
			new AsyncCancelCommand(validate);
		#endregion
	}
}
