﻿using System;
using System.IO;
using System.Text;
using System.Threading;

using Cryptography.Security.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.RC4.Core
{
	public sealed class AlgorithmProvider : 
		IAlgorithmProvider
	{
		public AlgorithmProvider()
		{
		}

		public IDecoder CreateDecoder(byte[] key) =>
			new EncoderBase(key);
		public IEncoder CreateEncoder(string passPhrase) =>
			new EncoderBase(passPhrase);

		private class EncoderBase:
			IEncoder,
			IDecoder
		{
			public byte[] DecodeKey => _key;
			public byte[] EncodeKey => _key;

			public EncoderBase(string passPhrase)
			{
				passPhrase.NotNull(nameof(passPhrase));

				_key = Encoding.UTF8.GetBytes(passPhrase);
				_table = this.CreateStateTable();
			}
			public EncoderBase(byte[] key)
			{
				key.AssignTo(out _key, nameof(key));

				_table = this.CreateStateTable();
			}

			public MemoryStream DecodeStream(Stream source, CancellationToken token)
			{
				this.ValidateSourceStream(source);
				return this.ProcessStream(source, token);
			}
			public MemoryStream EncodeStream(Stream source, CancellationToken token)
			{
				this.ValidateSourceStream(source);
				return this.ProcessStream(source, token);
			}

			#region Helpers
			private byte[] CreateStateTable()
			{
				var table = new byte[TableSize];

				for(int i = 0; i != table.Length; ++i)
				{
					table[i] = (byte)i;
				}

				for(int i = 0, j = 0; i < TableSize; ++i)
				{
					j = ( j + table[i] + _key[i % _key.Length] ) % TableSize;
					table.Swap(i, j);
				}

				return table;
			}
			private byte GenerateNextKeyNumber()
			{
				_x = ( _x + 1 ) % TableSize;
				_y = ( _y + _table[_x] ) % TableSize;

				_table.Swap(_x, _y);

				return _table[( _table[_x] + _table[_y] ) % TableSize];
			}
			private void ValidateSourceStream(Stream source)
			{
				source.NotNull(nameof(source));

				if (!source.CanRead)
				{
					throw new InvalidOperationException(
						$"{nameof(source)} must be readable");
				}
			}
			private MemoryStream ProcessStream(Stream source, CancellationToken token)
			{
				var iteration = 0;
				var data = 0;
				var memory = new MemoryStream(BufferSize);
				
				while (( data = source.ReadByte() ) != -1)
				{
					if (++iteration % DefaultCheckIteration == 0)
					{
						token.ThrowIfCancellationRequested();
					}

					var encrypted = (byte)( data ^ this.GenerateNextKeyNumber() );

					memory.WriteByte(encrypted);
				}

				return memory;
			}
			#endregion

			private int _x;
			private int _y;
			private readonly byte[] _key;
			private readonly byte[] _table;
			private const int TableSize = 256;
			private const int BufferSize = 128 * 256;
			private const int DefaultCheckIteration = 1000;
		}
	}
}
