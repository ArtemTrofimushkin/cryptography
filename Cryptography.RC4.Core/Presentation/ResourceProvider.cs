﻿using System;
using System.Collections.Generic;

using Cryptography.Host.UI.Abstract;

namespace Cryptography.RC4.Core.Presentation
{
	public class ResourceProvider : IResourceProvider
	{
		public IReadOnlyCollection<Uri> GetResourceDictionaries()
		{
			var assemblyName = this.GetType().Assembly.GetName().Name;
			var baseLocation = $"pack://application:,,,/{assemblyName};component/Resources/";

			return new[]
			{
				new Uri($"{baseLocation}GenericResources.xaml", UriKind.RelativeOrAbsolute),
			};
		}
	}
}
