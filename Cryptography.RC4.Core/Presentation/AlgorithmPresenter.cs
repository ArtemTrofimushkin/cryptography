﻿using Cryptography.Security;
using Cryptography.Security.Abstract;
using Cryptography.Security.Servises;

namespace Cryptography.RC4.Core.Presentation
{
	public sealed class AlgorithmPresenter : AlgorithmPresenterBase
	{
		public AlgorithmPresenter()
		{
			AlgorithmType = AlgorithmType.Symmetric | AlgorithmType.Stream;
			Title = "Симетричный алгоритм шифрования RC4";
			ImageKey = $"RC4SecurityAlgorithmImage";

			#region Description
			Description = @"RC4 (от англ. Rivest cipher 4 или Ron’s code), также известен как ARC4 или ARCFOUR (alleged RC4) — потоковый шифр, широко применяющийся в различных системах защиты информации в компьютерных сетях (например, в протоколах SSL и TLS, алгоритмах обеспечения безопасности беспроводных сетей WEP и WPA).

Шифр разработан компанией «RSA Security», и для его использования требуется лицензия.

Алгоритм RC4, как и любой потоковый шифр, строится на основе генератора псевдослучайных битов. На вход генератора записывается ключ, а на выходе читаются псевдослучайные биты. Длина ключа может составлять от 40 до 2048 бит[1]. Генерируемые биты имеют равномерное распределение.

Основные преимущества шифра:
высокая скорость работы;
переменный размер ключа.

RC4 довольно уязвим, если:
используются не случайные или связанные ключи;
один ключевой поток используется дважды.
Эти факторы, а также способ использования могут сделать криптосистему небезопасной (например, WEP).";
			#endregion
		}

		protected override IAlgorithmProvider CreateProvider()
		{
			return _provider ?? ( _provider = new AlgorithmProvider() );
		}
	}
}
