﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.Math.Tests
{
    using Services;

    [TestClass]
    public class TransposDecompPermutTest
    {
        [TestMethod]
        public void TransposDecompPermutFull()
        {
            var service = new TransposDecompPermut();
            var result = service.GetTranspositionsMultiplication(
				new uint[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
                new uint[] { 4, 7, 1, 5, 2, 8, 3, 10, 6, 9 });

            var resString = string.Join(string.Empty, result);

            Assert.AreEqual(resString, "(1 4)(1 5)(1 2)(1 7)(1 3)(6 8)(6 10)(6 9)");
        }

        [TestMethod]
        public void TransposDecompPermutSome()
        {
            var service = new TransposDecompPermut();
            var result = service.GetTranspositionsMultiplication(
				new uint[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
                new uint[] { 2, 1, 3, 4, 5, 6, 7, 10, 8, 9 });

            var resString = string.Join(string.Empty, result);

            Assert.AreEqual(resString, "(1 2)(8 10)(8 9)");
        }
    }
}
