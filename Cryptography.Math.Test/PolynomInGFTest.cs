﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.Math.Test
{
	[TestClass]
	public class PolynomInGFTest
	{
		[TestMethod]
		public void AdditionTest()
		{
			var p1 = new PolynomGF(new[] { 1UL, 1UL, 2UL }, 3);
			var p2 = new PolynomGF(new[] { 2UL, 1UL, 2UL, 1UL }, 3);
			var result = p1.Add(p2);

			Assert.IsTrue(
				this.Equal(result, 0UL, 2UL, 1UL, 1UL));
		}

		[TestMethod]
		public void MultiplyTest()
		{
			var p1 = new PolynomGF(new[] { 1UL, 1UL }, 3);
			var p2 = new PolynomGF(new[] { 0UL, 1UL, 2UL }, 3);
			var result = p1.Multiply(p2);

			Assert.IsTrue(
				this.Equal(result, 0UL, 1UL, 0UL, 2UL));
		}

		[TestMethod]
		public void LongDivisionTest1()
		{
			var p1 = new PolynomGF(new[] { 4UL, 1UL, 2UL, 3UL, 4UL }, 5);
			var p2 = new PolynomGF(new[] { 1UL, 0UL, 3UL }, 5);
			var result = p1.LongDivision(p2);

			Assert.IsTrue(
				this.Equal(result, 1UL));
		}

		[TestMethod]
		public void LongDivisionTest2()
		{
			var p1 = new PolynomGF(new[] { 4UL, 1UL, 2UL, 3UL, 4UL }, 5);
			var p2 = new PolynomGF(new[] { 1UL, 1UL, 3UL }, 5);
			var result = p1.LongDivision(p2);

			Assert.IsTrue(
				this.Equal(result, 1UL, 3UL));
		}

		[TestMethod]
		public void LongDivisionTest3()
		{
			var p1 = new PolynomGF(new[] { 1UL, 1UL, 3UL }, 5);
			var p2 = new PolynomGF(new[] { 4UL, 1UL, 2UL, 3UL, 4UL }, 5);
			var result = p1.LongDivision(p2);

			Assert.IsTrue(
				this.Equal(result, 1UL, 1UL, 3UL));
		}

		[TestMethod]
		public void LongDivisionTest4()
		{
			var p1 = new PolynomGF(new[] { 4UL, 1UL, 2UL, 3UL, 4UL }, 5);
			var p2 = new PolynomGF(new[] { 4UL, 1UL, 2UL, 3UL, 4UL }, 5);

			var result = p1.LongDivision(p2);

			Assert.IsTrue(
				this.Equal(result, 0UL));
		}

		[TestMethod]
		public void LongDivisionTest5()
		{
			var p1 = new PolynomGF(new[] { 0UL, 0UL, 0UL, 0UL, 1UL }, 13);
			var p2 = new PolynomGF(new[] { 2UL }, 13);

			var result = p1.LongDivision(p2);

			Assert.IsNotNull(result);
		}

		[TestMethod]
		public void StringParseTest()
		{
			var poly = new PolynomGF(new[] { 1UL, 1UL, 2UL }, 3);

			Assert.AreEqual(poly.ToString(), "1 + x + 2*x^2");

			var parsed = PolynomGF.Parse(poly.ToString(), 3UL);

			Assert.AreEqual(poly, parsed);
		}

		private bool Equal(PolynomGF polynome, params ulong[] values)
		{
			return polynome.Elements
				.SequenceEqual(values);
		}
	}
}
