﻿using System;
using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.DiscreteLogarithm.Core.Services;

namespace Cryptography.DiscreteLogarithm.Core.ViewModels
{
	public class PollardRhoFactory : ReducerFactoryBase
	{
		public ulong Order
		{
			get { return _order; }
			set
			{
				this.UpdateProperty(value, ref _order);
				Updated = true;
			}
		}

		public PollardRhoFactory() : 
			base("Ро алгоритм Полларда")
		{
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals(nameof(Order), propertyName, StringComparison.Ordinal))
			{
				return Order <= 1UL
					? "Неверное значение порядка, должно быть более 1"
					: String.Empty;
			}

			return base.ValidateProperty(propertyName);
		}
		protected override INumberDiscreteLogarithmReducer CreateReducer() =>
			new PollardRhoReducer(Order);

		private ulong _order;
	}
}
