﻿using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.DiscreteLogarithm.Core.Services;

namespace Cryptography.DiscreteLogarithm.Core.ViewModels
{
	public class ShanksFactory : ReducerFactoryBase
	{
		public ShanksFactory() : 
			base("Алгоритм Шэнкса")
		{
		}

		protected override INumberDiscreteLogarithmReducer CreateReducer() => 
			new ShanksReducer();
	}
}
