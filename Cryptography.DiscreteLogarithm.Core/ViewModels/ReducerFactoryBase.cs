﻿using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Utills.Extenssions;

namespace Cryptography.DiscreteLogarithm.Core.ViewModels
{
	public abstract class ReducerFactoryBase : ViewModelBase, INumberReducerFactory
	{
		public string Title { get; }
		protected ReducerFactoryBase(string title)
		{
			title.NotNull(nameof(title));
			Title = title;
		}

		protected bool Updated { get; set; }
		protected abstract INumberDiscreteLogarithmReducer CreateReducer();

		#region IReducerFactory
		INumberDiscreteLogarithmReducer INumberReducerFactory.CreateReducer()
		{
			if (Updated || _cache == null)
			{
				Updated = false;
				_cache = this.CreateReducer();
			}

			return _cache;
		}
		#endregion

		private INumberDiscreteLogarithmReducer _cache;
	}
}
