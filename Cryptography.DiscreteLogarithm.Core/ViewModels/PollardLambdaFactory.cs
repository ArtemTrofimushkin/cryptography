﻿using System;
using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.DiscreteLogarithm.Core.Services;

namespace Cryptography.DiscreteLogarithm.Core.ViewModels
{
	public class PollardLambdaFactory : ReducerFactoryBase
	{
		public ulong Begin
		{
			get { return _begin; }
			set
			{
				this.UpdateProperty(value, ref _begin);
				Updated = true;
			}
		}
		public ulong End
		{
			get { return _end; }
			set
			{
				this.UpdateProperty(value, ref _end);
				Updated = true;
			}
		}
		public ulong Order
		{
			get { return _order; }
			set
			{
				this.UpdateProperty(value, ref _order);
				Updated = true;
			}
		}

		public PollardLambdaFactory() : 
			base("Лямбда алгоритм Полларда")
		{
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals(nameof(Begin), propertyName, StringComparison.Ordinal) ||
				String.Equals(nameof(End), propertyName, StringComparison.Ordinal))
			{
				if ( Begin >= End)
				{
					return "Начало отрезка не может превышать или равняться концу";
				}
				
				return (End - Begin) <= 10 
					? "Укажите больший отрезок"
					: String.Empty;
			}

			if (String.Equals(nameof(Order), propertyName, StringComparison.Ordinal))
			{
				return Order <= 1UL
					? "Неверное значение порядка, должно быть более 1"
					: String.Empty;
			}

			return base.ValidateProperty(propertyName);
		}
		protected override INumberDiscreteLogarithmReducer CreateReducer() =>
			new PollardLambdaReducer(Begin, End, Order);

		private ulong _begin;
		private ulong _end;
		private ulong _order;
	}
}
