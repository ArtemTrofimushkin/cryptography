﻿using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.DiscreteLogarithm.Core.Services;
using Cryptography.Math.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.DiscreteLogarithm.Core.ViewModels
{
	public class PohligHellmanFactory : ReducerFactoryBase
	{
		public PohligHellmanFactory(
			IFactorizeService factorizeService,
			IChineseReminderService reminderService) : base("Алгоритм Полига-Хэллмана")
		{
			factorizeService.AssignTo(out _factorizeService, nameof(factorizeService));
			reminderService.AssignTo(out _reminderService, nameof(reminderService));
		}

		protected override INumberDiscreteLogarithmReducer CreateReducer() =>
			new PohligHellmanReducer(_factorizeService, _reminderService);

		private readonly IFactorizeService _factorizeService;
		private readonly IChineseReminderService _reminderService;
	}
}
