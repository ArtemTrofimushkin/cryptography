﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.Math.Helpers;

namespace Cryptography.DiscreteLogarithm.Core.Services
{
	using Math = System.Math;

	public class PollardLambdaReducer : INumberDiscreteLogarithmReducer
	{
		public PollardLambdaReducer(ulong a, ulong b, ulong order)
		{
			#region Validate
			if (b < a)
			{
				throw new ArgumentOutOfRangeException(
					nameof(b), b, $"Argument less then {a}");
			}
			if (order == 0UL)
			{
				throw new ArgumentException(
					"Must be natural number", nameof(order));
			}

			var n = (ulong)Math.Log(b - a, 2);
			if (n == 0UL)
			{
				throw new InvalidOperationException(
					$"Too small distance, specified: {a} - {b}");
			}
			#endregion

			_b = b;
			_n = n;
			_order = order;
			_set = this.GenerateSet().ToArray();
		}

		public IReducedResult<ulong> ReduceDicreteLogarithm(ulong module, ulong basis, ulong result, 
			CancellationToken token)
		{
			var c = _b;
			var g = this.GenerateWalk(ref c, module, basis);
			var d = this.TryFindIntersection(g, module, basis, result);

			return d == null
				? ReducedResult.Error<ulong>()
				: this.GenerateResult(c, d.Value, module, basis);
		}

		private ulong GenerateWalk(ref ulong c, ulong module, ulong basis)
		{
			var g = basis.ModulePower(c, module);

			for (var i = 0UL; i < _n; ++i)
			{
				var j = this.GroupNumber(g);

				g = this.NextWalk(g, _set[j], basis, module);
				c = ( c + _set[j] ) % _order;
			}

			return g;
		}
		private ulong? TryFindIntersection(ulong g, ulong module, ulong basis, ulong result)
		{
			var d = 0UL;
			var h = result;

			for (int i = 0; i < MaxIteration && ( h != g ); i++)
			{
				var j = this.GroupNumber(h);
				h = this.NextWalk(h, _set[j], basis, module);
				d = ( d + _set[j] ) % _order;
			}

			return ( h == g ) ? (ulong?)d : null;
		}
		private IReducedResult<ulong> GenerateResult(ulong c, ulong d, ulong module, ulong basis)
		{
			if (c < d)
			{
				c += _order;
			}

			var exponent = ( c - d ) % _order;
			return ReducedResult.Success(exponent, 
				basis.ModulePower(exponent, module));
		}
		private int GroupNumber(ulong value) =>
			(int)( value % _n );
		private ulong NextWalk(ulong g, ulong s, ulong basis, ulong module)
		{
			var exponent = basis.ModulePower(s, module);
			return g.ModuleMultiply(exponent, module);
		}
		private IEnumerable<ulong> GenerateSet()
		{
			for (ulong i = 0, first = 1; i < _n; i++)
			{
				yield return first;
				first *= 2UL;
			}
		}

		private readonly ulong _b;
		private readonly ulong _n;
		private readonly ulong _order;
		private readonly IReadOnlyList<ulong> _set;

		private const int MaxIteration = 10000;
	}
}
