﻿using System;
using System.Collections.Generic;
using System.Threading;

using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.Math.Helpers;

namespace Cryptography.DiscreteLogarithm.Core.Services
{
	public class PollardRhoReducer : INumberDiscreteLogarithmReducer
	{
		private struct Triple : IEquatable<Triple>
		{
			public ulong U { get; }
			public ulong V { get; }
			public ulong Z { get; }

			public Triple(ulong u, ulong v, ulong z)
			{
				U = u;
				V = v;
				Z = z;
			}

			public bool Equals(Triple other) =>
				Z == other.Z;
			public override bool Equals(object obj) =>
				obj is Triple && this.Equals((Triple)obj);
			public override string ToString() =>
				$"(U: {U}; V: {V}; Z: {Z})";
			public override int GetHashCode() =>
				Z.GetHashCode();
		}

		public PollardRhoReducer(ulong order)
		{
			if (order == 0UL)
			{
				throw new ArgumentException(
					"Must be natural", nameof(order));
			}
			_order = order;
		}

		public IReducedResult<ulong> ReduceDicreteLogarithm(ulong module, ulong basis, ulong result, 
			CancellationToken token)
		{
			_module = module;

			var found = false;
			var triple = new Triple(0, 0, 1);
			var tripleMap = new Dictionary<int, Triple>
			{
				[0] = triple
			};

			// TODO: Floyd algorithm
			for(int k = 1; !found; ++k)
			{
				var nextTriple = this.Next(triple, basis, result);
				var index = k / 2;

				if (k.IsEven() && 
					tripleMap.ContainsKey(index))
				{
					token.ThrowIfCancellationRequested();

					var oldTriple = tripleMap[index];

					if (this.CheckCandidates(oldTriple, nextTriple))
					{
						return this.GenerateResult(oldTriple, nextTriple, basis);
					}
				}

				triple = nextTriple;
				tripleMap.Add(k, nextTriple);
			}

			return ReducedResult.Error<ulong>();
		}

		private Triple Next(Triple old, ulong basis, ulong result)
		{
			var nextU = this.NextU(old.U, old.Z);
			var nextV = this.NextV(old.V, old.Z);

			return new Triple(nextU, nextV,
				this.NextZ(old.Z, basis, result));
		}
		private ulong NextU(ulong u, ulong z)
		{
			return ( z < FirstGroupBorder )
				? ( u + 1UL ) % _order
				: ( z < SecondGroupBorder )
					? u.ModuleMultiply(2UL, _order)
					: u;
		}
		private ulong NextV(ulong v, ulong z)
		{
			return ( z < FirstGroupBorder )
				? v
				: ( z < SecondGroupBorder )
					? v.ModuleMultiply(2UL, _order)
					: ( v + 1UL ) % _order;
		}
		private ulong NextZ(ulong z, ulong basis, ulong result)
		{
			return ( z < FirstGroupBorder )
				? result.ModuleMultiply(z, _module)
				: ( z < SecondGroupBorder )
					? z.ModuleMultiply(z, _module)
					: basis.ModuleMultiply(z, _module);
		}
		private bool CheckCandidates(Triple oldTriple, Triple nextTriple)
		{
			if (!oldTriple.Equals(nextTriple))
			{
				return false;
			}

			var diffU = this.PositiveSubstruct(oldTriple.U, nextTriple.U);
			return AlgebraicHelper.EuclideAlgorithm(diffU, _order) == 1UL;
		}
		private IReducedResult<ulong> GenerateResult(Triple oldTriple, Triple nextTriple, ulong basis)
		{
			var diffU = this.PositiveSubstruct(oldTriple.U, nextTriple.U);
			var inversed = AlgebraicHelper.MultiplicativeInverse(diffU, _order);
			var exponent = basis.ModuleMultiply(inversed, _order);

			return ReducedResult.Success(exponent, 
				basis.ModulePower(exponent, _module));
		}
		private ulong PositiveSubstruct(ulong x, ulong y)
		{
			if (x < y)
			{
				x += _order;
			}

			return (x - y) % _order;
		}
		private ulong FirstGroupBorder =>
			_module / 3UL;
		private ulong SecondGroupBorder =>
			2UL * _module / 3UL;

		private ulong _module;
		private readonly ulong _order;
	}
}
