﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading;
using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.Math.Abstract;
using Cryptography.Math.Helpers;
using Cryptography.Utills.Extenssions;

namespace Cryptography.DiscreteLogarithm.Core.Services
{
	public class PohligHellmanReducer : INumberDiscreteLogarithmReducer
	{
		public PohligHellmanReducer(
			IFactorizeService factorizeService,
			IChineseReminderService reminderService)
		{
			factorizeService.AssignTo(out _factorizeService, nameof(factorizeService));
			reminderService.AssignTo(out _reminderService, nameof(reminderService));
		}

		public IReducedResult<ulong> ReduceDicreteLogarithm(ulong module, ulong basis, ulong result, 
			CancellationToken token)
		{
			#region Validate
			if (module == 0)
			{
				throw new ArgumentException(
					"Module must be positive", nameof(module));
			}
			if (basis == 0)
			{
				throw new ArgumentException(
					"Basis must be positive", nameof(basis));
			}
			if (result == 0)
			{
				throw new ArgumentException(
					"Result must be positive", nameof(result));
			}
			#endregion

			var factorizeMap = _factorizeService.Factorize(module - 1, token);
			var tableMap = this.ComputeTable(factorizeMap, module, basis);
			var reminderMap = new Dictionary<ulong, ulong>();

			foreach (var pair in factorizeMap)
			{
				token.ThrowIfCancellationRequested();

				var partialModule = 1UL;
				var partialReminder = 0UL;
				var computedBasis = result;

				for(var i = 0; i < pair.Value; ++i)
				{
					var prevModule = partialModule;
					partialModule *= pair.Key;

					var gamma = computedBasis.ModulePower(( module - 1 ) / partialModule, module);
					var partialExponent = this.FindPower(tableMap, pair.Key, gamma);
					var inversed = AlgebraicHelper.MultiplicativeInverse(
						basis.ModulePower(partialExponent, module), module);

					partialReminder += partialExponent * prevModule;
					computedBasis = computedBasis.ModuleMultiply(inversed, module);
				}
				reminderMap.Add(partialModule, partialReminder);
			}

			var exponent = _reminderService.FindReminder(reminderMap);

			return ReducedResult.Success(exponent,
				basis.ModulePower(exponent, module));
		}

		private IDictionary<ulong, ulong[]> ComputeTable(IDictionary<ulong, int> factorizeMap, ulong module, ulong basis)
		{
			var result = new Dictionary<ulong, ulong[]>(factorizeMap.Count);

			foreach (var pair in factorizeMap)
			{
				var table = new ulong[pair.Key];

				for(int i = 0; i < (int)pair.Key; ++i)
				{
					var exponent = ( module - 1 ) * (ulong)i / pair.Key;
					table[i] = basis.ModulePower(exponent, module);
				}
				result.Add(pair.Key, table);
			}
			return result;
		}
		private ulong FindPower(IDictionary<ulong, ulong[]> tableMap, ulong prime, ulong gamma)
		{
			var table = tableMap[prime];
			var value = table.First(x => x == gamma);

			return (ulong)Array.IndexOf(table, value);
		}

		private readonly IFactorizeService _factorizeService;
		private readonly IChineseReminderService _reminderService;
	}
}
