﻿using Cryptography.DiscreteLogarithm.Core.Abstract;

namespace Cryptography.DiscreteLogarithm.Core.Services
{
	public class PolynomReducerFactory : IPolynomReducerFactory
	{
		public IPolynomDiscreteLogarithmReducer CreateReducer(ulong character) =>
			new GaloisFieldReducer(character);
	}
}
