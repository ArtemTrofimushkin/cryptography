﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.Math;

namespace Cryptography.DiscreteLogarithm.Core.Services
{
	using Math = System.Math;

	public class IrreducibleGenerator : IIrreducibleGenerator
	{
		public IrreducibleGenerator()
		{
		}

		public IEnumerable<PolynomGF> GenerateField(ulong character, ulong power)
		{
			int n = (int)power / 2,
				q = (int)character,
				count = (int)( ( q - 1 ) * q * ( 1 - Math.Pow(q, n) ) / ( 1 - q ) );

			var candidate = new PolynomGF(new[] { 0UL, 1UL }, character);

			for (int i = 0; i < count; ++i)
			{
				yield return candidate;
				candidate = candidate.Increment();
			}
		}
		public IEnumerable<PolynomGF> GenerateIrreduciblePolynomials(ulong character, ulong power, 
			CancellationToken token)
		{
			for (var i = 1UL; i < power; i++)
			{
				for (var j = 1UL; j < character; j++)
				{
					for (var k = 1UL; k < character; k++)
					{
						if (i * j * k == IterationCheck)
						{
							token.ThrowIfCancellationRequested();
						}

						var polynom = this.CreateIrreduciblePolynom(i, j, k, character, power);
						var irreducible = this.CheckIrreducible(polynom, character, power);

						if (irreducible)
						{
							yield return polynom;
						}
					}
				}
			}
		}
		public PolynomGF GenerateIrreduciblePolynom(ulong character, ulong power, 
			CancellationToken token)
		{
			return this.GenerateIrreduciblePolynomials(character, power, token)
				.FirstOrDefault();
		}

		private bool CheckIrreducible(PolynomGF polynom, ulong character, ulong power)
		{
			return this.GenerateField(character, power)
				.All(gf => !polynom.LongDivision(gf).IsZero);
		}
		private PolynomGF CreateIrreduciblePolynom(ulong p1, ulong k1, ulong k0, ulong character, ulong power)
		{
			var source = Enumerable.Range(0, (int)power + 1)
				.Select(i => 0UL)
				.ToArray();

			source[source.Length - 1] = 1;
			source[p1] = k1;
			source[0] = k0;

			return new PolynomGF(source, character);
		}

		private const int IterationCheck = 1000;
	}
}
