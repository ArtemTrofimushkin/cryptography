﻿using System.Collections.Generic;
using System.Threading;

using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.Math.Helpers;

namespace Cryptography.DiscreteLogarithm.Core.Services
{
	public class ShanksReducer : INumberDiscreteLogarithmReducer
	{
		public ShanksReducer()
		{
		}

		public IReducedResult<ulong> ReduceDicreteLogarithm(ulong module, ulong basis, ulong result, 
			CancellationToken token)
		{
			var n = AlgebraicHelper.Sqrt(module) + 1;
			var gamma = this.CalculateGamma(basis, module, n);
			var reduceMap = this.GetReduceMap(gamma, module, n);

			for (ulong i = 0, current = result; i <= n; ++i)
			{
				token.ThrowIfCancellationRequested();

				if (reduceMap.ContainsKey(current))
				{
					var reducedResult = reduceMap[current] * n - i;
					if (reducedResult < module)
					{
						return ReducedResult.Success(reducedResult,
							basis.ModulePower(reducedResult, module));
					}
				}
				current = AlgebraicHelper.ModuleMultiply(current, basis, module);
			}

			return ReducedResult.Error<ulong>();
		}

		private ulong CalculateGamma(ulong basis, ulong module, ulong m)
		{
			var result = 1UL;

			for(ulong i = 0; i < m; ++i)
			{
				result = AlgebraicHelper.ModuleMultiply(result, basis, module);
			}
			return result;
		}
		private IDictionary<ulong, ulong> GetReduceMap(ulong gamma, ulong module, ulong m)
		{
			var result = new SortedDictionary<ulong, ulong>();

			for (ulong i = 1, current = gamma; i <= m; ++i)
			{
				if (!result.ContainsKey(current))
				{
					result[current] = i;
				}
				current = AlgebraicHelper.ModuleMultiply(current, gamma, module);
			}

			return result;
		}
	}
}
