﻿using System;
using System.Threading;

using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.Math;
using Cryptography.Math.Helpers;
using Cryptography.Utills.Extenssions;

namespace Cryptography.DiscreteLogarithm.Core.Services
{
	public class GaloisFieldReducer : INumberDiscreteLogarithmReducer,
		IPolynomDiscreteLogarithmReducer
	{
		public GaloisFieldReducer(ulong character)
		{
			if (character < 2)
			{
				throw new ArgumentException(
					"Must be prime number", nameof(character));
			}

			_character = character;
		}

		public IReducedResult<ulong> ReduceDicreteLogarithm(ulong module, ulong basis, ulong result,
			CancellationToken token)
		{
			var exponentGF = this.ReduceDicreteLogarithm(
				module.ToPolynom(_character),
				basis.ToPolynom(_character),
				result.ToPolynom(_character),
				token);

			var exponent = exponentGF.Exponent;

			return ReducedResult.Success(exponent,
				basis.ModulePower(exponent, module));
		}
		public IReducedResult<PolynomGF> ReduceDicreteLogarithm(PolynomGF module, PolynomGF basis, PolynomGF result, 
			CancellationToken token)
		{
			module.NotNull(nameof(module));
			basis.NotNull(nameof(basis));
			result.NotNull(nameof(result));

			basis = basis.LongDivision(module);
			result = result.LongDivision(module);

			var exponent = 0U;
			var multiplier = PolynomGF.One(0UL, module.Character);

			for (; !result.Equals(multiplier); exponent++)
			{
				if (exponent % CheckIteration == 0)
				{
					token.ThrowIfCancellationRequested();
				}

				multiplier = basis.ModulePower(exponent, module);
			}

			return ReducedResult.Success(exponent - 1,
				basis.ModulePower(exponent - 1, module));
		}

		private readonly ulong _character;
		private const int CheckIteration = 100;
	}
}
