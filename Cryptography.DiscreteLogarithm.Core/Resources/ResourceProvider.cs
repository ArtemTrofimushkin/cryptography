﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cryptography.Host.UI.Abstract;

namespace Cryptography.DiscreteLogarithm.Core.Resources
{
	public class ResourceProvider : IResourceProvider
	{
		public IReadOnlyCollection<Uri> GetResourceDictionaries()
		{
			var assemblyName = this.GetType().Assembly.GetName().Name;
			var baseLocation = $"pack://application:,,,/{assemblyName};component/Resources/";

			return new[]
			{
				new Uri($"{baseLocation}DataTemplates.xaml", UriKind.RelativeOrAbsolute),
				new Uri($"{baseLocation}GenericResources.xaml", UriKind.RelativeOrAbsolute),
			};
		}
	}
}
