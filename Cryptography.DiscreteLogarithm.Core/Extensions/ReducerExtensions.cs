﻿using System.Collections.Generic;
using System.Threading;
using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.Math;

namespace Cryptography.DiscreteLogarithm.Core.Extensions
{
	public static class ReducerExtensions
	{
		public static IReducedResult<ulong> ReduceDicreteLogarithm(this INumberDiscreteLogarithmReducer reduser,
			ulong module, ulong basis, ulong result)
		{
			return reduser.ReduceDicreteLogarithm(module, basis, result, CancellationToken.None);
		}

		public static IEnumerable<PolynomGF> GenerateField(this IIrreducibleGenerator generator,
			ulong character, ulong power)
		{
			return generator.GenerateIrreduciblePolynomials(character, power, CancellationToken.None);
		}
	}
}
