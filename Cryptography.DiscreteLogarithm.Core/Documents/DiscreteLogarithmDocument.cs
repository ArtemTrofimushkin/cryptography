﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.DiscreteLogarithm.Core.Extensions;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Utills.Extenssions;

namespace Cryptography.DiscreteLogarithm.Core.Documents
{
	public class DiscreteLogarithmDocument : DocumentBase
	{
		#region Properties
		public ulong Module
		{
			get { return _module; }
			set { this.UpdateProperty(value, ref _module); }
		}
		public ulong Basis
		{
			get { return _basis; }
			set { this.UpdateProperty(value, ref _basis); }
		}
		public ulong Result
		{
			get { return _result; }
			set { this.UpdateProperty(value, ref _result); }
		}
		public IReducedResult<ulong> ReduceResult
		{
			get { return _reduceResult; }
			set { this.UpdateProperty(value, ref _reduceResult); }
		}
		public IReadOnlyList<INumberReducerFactory> Factories { get; }
		public INumberReducerFactory SelectedFactory
		{
			get { return _factory; }
			set { this.UpdateProperty(value, ref _factory); }
		}

		public ICommand ReduceLogarithmCommand { get; private set; }
		#endregion

		public DiscreteLogarithmDocument(IEnumerable<INumberReducerFactory> factories)
		{
			factories.NotNull(nameof(factories));

			Title = "Дискретное логарифмирование";
			Factories = factories.ToArray();
		}

		protected override void CreateCommands()
		{
			base.CreateCommands();

			ReduceLogarithmCommand = CommandFactory.CreateCommand(
				this.ReduceLogarithmExecute,
				this.ReduceLogarithmCanExecute);
		}
		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals(propertyName, nameof(Module), StringComparison.Ordinal))
			{
				return Module == 0UL || Module == 1UL 
					? "Модуль не может быть равным нулю или единице" 
					: String.Empty;
			}
			if (String.Equals(propertyName, nameof(Basis), StringComparison.Ordinal))
			{
				return Basis == 0UL 
					? "Основание не может быть нулевым или равным единице" 
					: String.Empty;
			}

			return base.ValidateProperty(propertyName);
		}

		#region Command methods
		private async void ReduceLogarithmExecute()
		{
			await this.ExecuteOperationAsync(this.ReduceLogarithmAsync);
		}
		private bool ReduceLogarithmCanExecute()
		{
			return IsValid && 
				SelectedFactory != null && 
				( (ViewModelBase)SelectedFactory ).IsValid;
		}
		#endregion

		#region Helpers
		private async Task ReduceLogarithmAsync(CancellationToken token)
		{
			var reducer = SelectedFactory.CreateReducer();
			var result = await Task.Run(
				() => reducer.ReduceDicreteLogarithm(Module, Basis, Result, token));

			ReduceResult = result;
		}
		#endregion

		private ulong _module;
		private ulong _basis;
		private ulong _result;
		private INumberReducerFactory _factory;
		private IReducedResult<ulong> _reduceResult;
	}
}
