﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Cryptography.DiscreteLogarithm.Core.Abstract;
using Cryptography.Host.Api.ViewModels;
using Cryptography.Math;
using Cryptography.Utills.Extenssions;

namespace Cryptography.DiscreteLogarithm.Core.Documents
{
	public class GaloisFieldLogarithmDocument : DocumentBase
	{
		#region Properties
		public ulong Character
		{
			get { return _character; }
			set
			{
				this.UpdateProperty(value, ref _character);
				this.UpdateIrreducibleCollection();
			}
		}
		public ulong Power
		{
			get { return _power; }
			set
			{
				this.UpdateProperty(value, ref _power);
				this.UpdateIrreducibleCollection();
			}
		}
		public ulong ResultPower
		{
			get { return _resultPower; }
			set { this.UpdateProperty(value, ref _resultPower); }
		}
		public string Basis
		{
			get { return _basis; }
			set { this.UpdateProperty(value, ref _basis); }
		}
		public string Result
		{
			get { return _result; }
			set { this.UpdateProperty(value, ref _result); }
		}
		public PolynomGF Module
		{
			get { return _module; }
			set { this.UpdateProperty(value, ref _module); }
		}
		public PolynomGF ResultCheck
		{
			get { return _reducedCheck; }
			set { this.UpdateProperty(value, ref _reducedCheck); }
		}
		public ICollection<PolynomGF> Irreducibles
		{
			get { return _irreducibles; }
			set { this.UpdateProperty(value, ref _irreducibles); }
		}
		#endregion

		#region Commands
		public ICommand GenerateIrreducibleCommand { get; private set; }
		public ICommand ReduceLogarithmCommand { get; private set; }
		#endregion

		public GaloisFieldLogarithmDocument(
			IPolynomReducerFactory factory,
			IIrreducibleGenerator generator)
		{
			factory.AssignTo(out _factory, nameof(factory));
			generator.AssignTo(out _generator, nameof(generator));

			Title = "Поля галуа";
		}
		
		#region Overrides
		protected override void CreateCommands()
		{
			base.CreateCommands();

			ReduceLogarithmCommand = CommandFactory.CreateCommand(
				this.ReduceLogarithmExecute,
				this.ReduceLogarithmCanExecute);
			GenerateIrreducibleCommand = CommandFactory.CreateCommand(
				this.GenerateIrreducibleExecute,
				this.GenerateIrreducibleCanExecute);
		}
		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals(nameof(Character), propertyName, StringComparison.Ordinal))
			{
				return Character <= 1UL
					? "Неверное значение характеристики, должно быть более 1"
					: String.Empty;
			}
			if (String.Equals(nameof(Power), propertyName, StringComparison.Ordinal))
			{
				return Power == 0UL
					? "Неверное значение степени, должно быть более 0"
					: String.Empty;
			}
			if (String.Equals(nameof(Module), propertyName, StringComparison.Ordinal))
			{
				return Module == null
					? "Укажите модуль"
					: String.Empty;
			}
			if (String.Equals(nameof(Basis), propertyName, StringComparison.Ordinal))
			{
				var poly = default(PolynomGF);
				return String.IsNullOrWhiteSpace(Basis) || !PolynomGF.TryParse(Basis, Character, out poly)
					? "Укажите основание"
					: String.Empty;
			}
			if (String.Equals(nameof(Result), propertyName, StringComparison.Ordinal))
			{
				var poly = default(PolynomGF);
				return String.IsNullOrWhiteSpace(Result) || !PolynomGF.TryParse(Result, Character, out poly)
					? "Укажите результат потенциирования"
					: String.Empty;
			}

			return base.ValidateProperty(propertyName);
		}
		#endregion

		#region Command Methods
		#region Execute
		private async void ReduceLogarithmExecute()
		{
			await this.ExecuteOperationAsync(
				this.ReduceLogarithmAsync);
		}
		private async void GenerateIrreducibleExecute()
		{
			await this.ExecuteOperationAsync(
				this.GenerateIrreducibleAsync);
		}
		#endregion

		#region CanExecute
		private bool ReduceLogarithmCanExecute() => IsValid;
		private bool GenerateIrreducibleCanExecute() =>
			Character > 1 && Power > 0;
		#endregion
		#endregion

		#region Helper methods
		private async Task ReduceLogarithmAsync(CancellationToken token)
		{
			var reducer = _factory.CreateReducer(Character);
			var basis = PolynomGF.Parse(Basis, Character);
			var result = PolynomGF.Parse(Result, Character);

			var reduced = await Task.Run(
				() => reducer.ReduceDicreteLogarithm(Module, basis, result, token));

			ResultCheck = reduced.Check;
			ResultPower = reduced.Exponent;
		}
		private async Task GenerateIrreducibleAsync(CancellationToken token)
		{
			var result = await Task.Run(() =>
				_generator.GenerateIrreduciblePolynomials(Character, Power, token).ToList());

			Irreducibles?.Clear();
			Irreducibles = result;
		}
		private void UpdateIrreducibleCollection()
		{
			if (GenerateIrreducibleCommand.CanExecute(null))
			{
				GenerateIrreducibleCommand.Execute(null);
			}
		}
		#endregion

		private ulong _power;
		private ulong _character;
		private ulong _resultPower;
		private string _basis;
		private string _result;
		private PolynomGF _module;
		private PolynomGF _reducedCheck;
		private ICollection<PolynomGF> _irreducibles;
		private readonly IPolynomReducerFactory _factory;
		private readonly IIrreducibleGenerator _generator;
	}
}
