﻿using Cryptography.Math;

namespace Cryptography.DiscreteLogarithm.Core.Abstract
{
	public interface IPolynomDiscreteLogarithmReducer :
		IDiscreteLogarithmReducer<PolynomGF>
	{
	}
}
