﻿using System.Collections.Generic;
using System.Threading;
using Cryptography.Math;

namespace Cryptography.DiscreteLogarithm.Core.Abstract
{
	public interface IIrreducibleGenerator
	{
		IEnumerable<PolynomGF> GenerateField(ulong character, ulong power);
		PolynomGF GenerateIrreduciblePolynom(ulong character, ulong power, 
			CancellationToken token);
		IEnumerable<PolynomGF> GenerateIrreduciblePolynomials(ulong character, ulong power,
			CancellationToken token);
	}
}
