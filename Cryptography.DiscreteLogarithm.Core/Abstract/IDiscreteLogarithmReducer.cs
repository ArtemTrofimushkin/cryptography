﻿using System.Threading;

namespace Cryptography.DiscreteLogarithm.Core.Abstract
{
	public interface IDiscreteLogarithmReducer<T>
	{
		IReducedResult<T> ReduceDicreteLogarithm(T module, T basis, T result, 
			CancellationToken token);
	}
}
