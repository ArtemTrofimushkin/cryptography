﻿namespace Cryptography.DiscreteLogarithm.Core.Abstract
{
	public interface INumberReducerFactory
	{
		string Title { get; }
		INumberDiscreteLogarithmReducer CreateReducer();
	}
}
