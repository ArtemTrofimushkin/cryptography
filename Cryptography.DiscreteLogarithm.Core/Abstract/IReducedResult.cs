﻿namespace Cryptography.DiscreteLogarithm.Core.Abstract
{
	public interface IReducedResult<T>
	{
		bool Successed { get; }
		ulong Exponent { get; }
		T Check { get; }
	}
}
