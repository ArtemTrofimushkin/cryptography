﻿namespace Cryptography.DiscreteLogarithm.Core.Abstract
{
	public interface IPolynomReducerFactory
	{
		IPolynomDiscreteLogarithmReducer CreateReducer(ulong character);
	}
}
