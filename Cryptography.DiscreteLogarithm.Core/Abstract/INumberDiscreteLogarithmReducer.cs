﻿namespace Cryptography.DiscreteLogarithm.Core.Abstract
{
	public interface INumberDiscreteLogarithmReducer :
		IDiscreteLogarithmReducer<ulong>
	{
	}
}
