﻿using System;
using Cryptography.DiscreteLogarithm.Core.Abstract;

namespace Cryptography.DiscreteLogarithm.Core
{
	public class ReducedResult<T> : IReducedResult<T>
		where T: IEquatable<T>
	{
		public bool Successed { get; }
		public ulong Exponent { get; }
		public T Check { get; }

		internal ReducedResult(ulong exponent, T check, bool succesed)
		{
			Exponent = exponent;
			Check = check;
			Successed = succesed;
		}

		public override string ToString() =>
			Successed ? $"Success: {Exponent}" : "Error";
	}

	public static class ReducedResult
	{
		public static ReducedResult<T> Success<T>(ulong exponent, T check) where T: IEquatable<T> =>
			new ReducedResult<T>(exponent, check, true);
		public static ReducedResult<T> Error<T>() where T : IEquatable<T> =>
			new ReducedResult<T>(ulong.MaxValue, default(T), false);
	}
}
