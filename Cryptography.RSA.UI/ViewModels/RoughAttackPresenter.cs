﻿using Cryptography.Host.Api.ViewModels;
using Cryptography.Math.Abstract;
using Cryptography.RSA.Core.Abstract;
using Cryptography.RSA.Core.Attacks;
using Cryptography.RSA.UI.Abstract;
using Cryptography.Utills.Extenssions;

namespace Cryptography.RSA.UI.ViewModels
{
	public class RoughAttackPresenter : ViewModelBase, IAttackPresenter
	{
		public string Key { get; }
		public string Title { get; }

		public RoughAttackPresenter(IFactorizeService service)
		{
			service.AssignTo(out _service, nameof(service));

			Title = "Атака перебором";
			Key = "RoughAttack";
		}
		public IRSAProviderAttack CreateProvider() =>
			new RoughAttack(_service);

		private readonly IFactorizeService _service;
	}
}
