﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Cryptography.Host.Api.ViewModels;
using Cryptography.RSA.Core.Abstract;
using Cryptography.RSA.UI.Abstract;
using Cryptography.Security.Abstract;
using Cryptography.Utills.Extenssions;
using Cryptography.Utills.Helpers;

namespace Cryptography.RSA.UI.Documents
{
	public class RSAProviderAttackDocument : DocumentBase
	{
		#region Properties
		public string Passphrase
		{
			get { return _passphrase; }
			set { this.UpdateProperty(value, ref _passphrase); }
		}
		public ulong PublicKey
		{
			get { return _publicKey; }
			set { this.UpdateProperty(value, ref _publicKey); }
		}
		public ulong SecretKey
		{
			get { return _secretKey; }
			set { this.UpdateProperty(value, ref _secretKey); }
		}
		public ulong Module
		{
			get { return _module; }
			set { this.UpdateProperty(value, ref _module); }
		}
		public IAttackResult AttackResult
		{
			get { return _result; }
			set { this.UpdateProperty(value, ref _result); }
		}
		public IAttackPresenter SelectedPresenter
		{
			get { return _selected; }
			set { this.UpdateProperty(value, ref _selected); }
		}
		public ICollection<IAttackPresenter> Presenters { get; }
		#endregion

		#region Commands
		public ICommand GenerateKeyCommand { get; private set; }
		public ICommand AttackCommand { get; private set; }
		#endregion

		public RSAProviderAttackDocument(
			IRSAWeakAlgorithmProvider provider,
			ICollection<IAttackPresenter> presenters)
		{
			provider.AssignTo(out _provider, nameof(provider));
			presenters.NotNull(nameof(presenters));

			Title = "Атаки на криптосистему RSA";
			Presenters = presenters;
		}

		#region Overrides
		protected override void CreateCommands()
		{
			base.CreateCommands();
			GenerateKeyCommand = CommandFactory.CreateCommand(
				this.GenerateKeyExecute,
				this.GenerateKeyCanExecute);
			AttackCommand = CommandFactory.CreateCommand(
				this.AttackExecute,
				this.AttackCanExecute);
		}
		protected override string ValidateProperty(string propertyName)
		{
			if (String.Equals(propertyName, "Passphrase", StringComparison.Ordinal))
			{
				return String.IsNullOrWhiteSpace(Passphrase)
					? "Укажите фразу для ключа"
					: String.Empty;
			}
			if (String.Equals(propertyName, "PublicKey", StringComparison.Ordinal))
			{
				return PublicKey == 0UL
					? "Открытый ключ не указан"
					: String.Empty;
			}
			if (String.Equals(propertyName, "SecretKey", StringComparison.Ordinal))
			{
				return SecretKey == 0UL
					? "Секретный ключ не указан"
					: String.Empty;
			}
			if (String.Equals(propertyName, "Module", StringComparison.Ordinal))
			{
				return Module == 0UL
					? "Модуль не указан"
					: String.Empty;
			}

			return base.ValidateProperty(propertyName);
		}
		#endregion

		#region Command Methods
		#region Execute
		private async void GenerateKeyExecute()
		{
			await this.ExecuteOperationAsync(this.GenerateKeyAsync);
		}
		private async void AttackExecute()
		{
			await this.ExecuteOperationAsync(this.AttackExecuteAsync);
		}
		#endregion

		#region CanExecute
		private bool GenerateKeyCanExecute() =>
			SelectedPresenter != null && !String.IsNullOrWhiteSpace(Passphrase);
		private bool AttackCanExecute() => IsValid;
		#endregion
		#endregion

		#region Helper methods
		private async Task AttackExecuteAsync(CancellationToken token)
		{
			var provider = SelectedPresenter.CreateProvider();

			AttackResult = await Task.Run(
				() => provider.Attack(_rsaEncoder, token));
		}
		private async Task GenerateKeyAsync(CancellationToken token)
		{
			_rsaEncoder = await Task.Run(
					() => this.CreateEncoder())
				.WithCancellation(token);

			token.ThrowIfCancellationRequested();

			var encoder = _rsaEncoder as IEncoder;
			var rsaDecoder = _provider.CreateDecoder(encoder.EncodeKey) as IRSAEncoder;

			Module = _rsaEncoder.Module;
			PublicKey = _rsaEncoder.Exponent;
			SecretKey = rsaDecoder.Exponent;
		}
		private IRSAEncoder CreateEncoder()
		{
			var encoder = ( SelectedPresenter.Key == "VinnerAttack" )
				? _provider.CreateWeakEncoder(Passphrase)
				: _provider.CreateEncoder(Passphrase);

			return encoder as IRSAEncoder;
		}
		#endregion

		private string _passphrase;
		private ulong _publicKey;
		private ulong _secretKey;
		private ulong _module;
		private IRSAEncoder _rsaEncoder;
		private IAttackResult _result;
		private IAttackPresenter _selected;
		private readonly IRSAWeakAlgorithmProvider _provider;
	}
}
