﻿using Cryptography.RSA.Core.Abstract;

namespace Cryptography.RSA.UI.Abstract
{
	public interface IAttackPresenter
	{
		string Title { get; }
		string Key { get; }

		IRSAProviderAttack CreateProvider();
	}
}
